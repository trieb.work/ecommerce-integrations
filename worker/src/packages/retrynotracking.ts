/* eslint-disable prefer-destructuring */
import { ZohoClientInstance } from '@trieb.work/zoho-inventory-ts';
import { WinstonLoggerType } from '@trieb.work/apm-logging';
import EasyPost from '@easypost/api';
import generateTrackingPortalURL, { Carrier, CountryCode } from '@trieb.work/carrier-tracking-portals';
import eciPrismaClient from '../lib/prismaClient';

const retryPackageWithoutTrackingNumber = async ({ appConfigId, zohoPackageId, logger } :{ appConfigId :number, zohoPackageId :string, logger :WinstonLoggerType}) => {
    const prisma = eciPrismaClient();

    const appConfig = await prisma.appConfig.findFirst({ where: { id: appConfigId }, include: { saleor: true, zoho: true, easypost: true } });
    const zohoSettings = appConfig?.zoho;

    if (zohoPackageId) {
        logger.info(`Received a Zoho Package with ID ${zohoPackageId}`);
    } else {
        logger.error('Received a not valid job! No zohoPackageId given');
        return false;
    }

    if (!zohoSettings?.clientId || !zohoSettings?.clientSecret || !zohoSettings?.orgId) {
        logger.error('Mandatory zoho settings missing!');
        return false;
    }
    const zohoClient = new ZohoClientInstance({
        zohoClientId: zohoSettings?.clientId,
        zohoClientSecret: zohoSettings?.clientSecret,
        zohoOrgId: zohoSettings.orgId,
    });
    await zohoClient.authenticate();

    const easypostClient = new EasyPost(appConfig?.easypost?.apiToken);

    const zohoPackage = await zohoClient.getPackage({ package_id: zohoPackageId });

    if (zohoPackage.tracking_number) {
        logger.info(`We received a tracking number for package ${zohoPackage.package_id}. Adding easypost tracker now`);
        const carrier = zohoPackage.carrier.includes('DPD') ? 'DPD' : '';
        const tracker = new easypostClient.Tracker({
            tracking_code: zohoPackage.tracking_number,
            carrier,
        });
        await tracker.save();

        // add the tracking Portal URL back to the salesorder
        try {
            const country = zohoPackage.shipping_address!.country;
            const countryCode = {
                Deutschland: 'de' as CountryCode,
                Österreich: 'at' as CountryCode,
                Schweiz: 'ch' as CountryCode,
                Spanien: 'es' as CountryCode,
                Frankreich: 'fr' as CountryCode,
                'Vereinigtes Königreich': 'uk' as CountryCode,
                Belgien: 'be' as CountryCode,
                Niederlande: 'nl' as CountryCode,
            };
            const url = generateTrackingPortalURL(carrier as Carrier, countryCode[country], zohoPackage.tracking_number, zohoPackage?.shipping_address?.zip || '');

            if (url && !zohoPackage?.shipment_order?.notes) {
                await zohoClient.addNote(zohoPackage.shipment_id, url);
            }
        } catch (error) {
            logger.error(error);
        }

        return true;
    }
    logger.error(`We still could not get a tracking number from Zoho! Can not do anything: ${JSON.stringify(zohoPackage)}`);

    return false;
};
export default retryPackageWithoutTrackingNumber;
