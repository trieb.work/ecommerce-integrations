/* eslint-disable no-continue */
/* eslint-disable no-await-in-loop */
import { WinstonLoggerType } from '@trieb.work/apm-logging';
import { ZohoClientInstance } from '@trieb.work/zoho-inventory-ts';
import Timeout from 'await-timeout';
import eciPrismaClient from '../lib/prismaClient';

/* eslint-disable no-restricted-syntax */
const transactionFeeSync = async (appConfigId :number, transactions :TransactionFeeItem[], logger :WinstonLoggerType) => {
    const prisma = eciPrismaClient();
    const appConfig = await prisma.appConfig.findFirst({ where: { id: appConfigId }, include: { zoho: true } });
    const zohoConf = appConfig?.zoho;
    let zohoClient :ZohoClientInstance;
    if (zohoConf && zohoConf.clientId && zohoConf.clientSecret && zohoConf.orgId) {
        zohoClient = new ZohoClientInstance({
            zohoClientId: zohoConf.clientId,
            zohoClientSecret: zohoConf.clientSecret,
            zohoOrgId: zohoConf.orgId,
        });
    } else {
        logger.error(`Could not get Zoho Config for appConfigId ${appConfig}`);
        return false;
    }

    await zohoClient.authenticate();

    logger.info(`Starting to work on ${transactions.length} transactions`);
    for (const transaction of transactions) {
        const transactionId = transaction['Transaction ID'];
        const paymentsFromZoho = await zohoClient.getCustomerPayments({ reference_number: transactionId });
        if (paymentsFromZoho.length <= 0) {
            logger.error(`Got no valid payment from Zoho for gateway_transaction_id ${transactionId}! Proceeding..`);
            continue;
        }
        const payment = paymentsFromZoho[0];

        const totalCosts = parseFloat(transaction['Braintree Fee Amount']) * -1 + parseFloat(transaction['Interchange Amount']) * -1;

        if (payment.reference_number === transactionId) {
            logger.info(`Adding bank_charges of ${totalCosts} to Customer Payment ID ${payment.payment_id}`);
            const updateData = {
                bank_charges: totalCosts,
            };
            await zohoClient.updateCustomerPayment(payment.payment_id as string, updateData);
        } else {
            logger.info(`Can't find a payment in Zoho for gateway_transaction_id ${transactionId}`);
        }
        await Timeout.set(1000);
    }
    return true;
};
export default transactionFeeSync;
