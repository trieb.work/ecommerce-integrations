type TransactionFeeItem = {
    'Transaction ID': string,
    'Disbursement Date': '2020-12-17',
    'Merchant Name': 'Pfeffer & Frost',
    'Merchant Account Token': string,
    'Transaction Currency': 'EUR',
    'Transaction Amount': '24.80',
    'Settlement Currency': 'EUR',
    'Settlement Amount': '24.80',
    'Braintree Fee Amount': '-0.77',
    'Interchange Amount': '-0.07',
    'Card Brand': 'Visa',
    'Card Type Group': 'Consumer Credit',
    'Region Relation': 'Domestic',
    'Interchange Rate Description': 'DE NON-SEC CR-DE NON-SEC CR'
};
