/* eslint-disable import/first */
import { logAndMeasure } from '@trieb.work/apm-logging';

import process from 'process';
import dotenv from 'dotenv';

dotenv.config({ path: '../.env' });

const elasticConfig = {
    serviceName: 'eci-worker',
    elasticLoggingServer: '',
};

import Bull from 'bull';
import eciPrismaClient from './lib/prismaClient';
import webhookSync from './lib/webhooksync';
import mailchimpVoucherSync from './mailchimp/voucherSync';
import transactionFeeSync from './braintree/transactionfeesync';
import syncZohoSettings from './zoho/setupEci';
import retryPackageWithoutTrackingNumber from './packages/retrynotracking';

const ENV = process.env.APM_ENV as string;

if (!ENV) {
    console.error('No environment given! Set with env variable "APM_ENV". Exiting.');
    process.exit(1);
}
if (!process.env.DATABASE_URL) throw new Error('No DATABASE_URL set. Can not connect to Postgres');

const prisma = eciPrismaClient();

const main = async () => {
    const commonConf = await prisma.commonConfig.findFirst({ where: { id: 1 }, include: { redis: true, elasticsearch: true } });
    if (!commonConf) throw new Error('Common config object could not be loaded! Failing here -check Prisma DB Client');

    console.log(`Connecting to Redis host ${commonConf.redis?.host}, Port ${commonConf.redis?.port}`);

    const redisOpts = {
        host: commonConf?.redis?.host,
        port: commonConf?.redis?.port,
        password: commonConf?.redis?.password,
    };

    const elasticConf = commonConf.elasticsearch;
    elasticConfig.elasticLoggingServer = elasticConf?.loggingServer as string;

    if (!elasticConf) {
        console.error('Elastic Options missing!');
    }

    // subscribe to webhookSyncWorker Job
    const webhookSyncWorker = new Bull('webhookSyncJob', {
        redis: redisOpts,
        prefix: `${ENV}_`,
    });

    // subscribe to the mailchimp Sync Vouchers Job
    const mailchimpVoucherSyncQueue = new Bull('mailchimpSyncJob', {
        redis: redisOpts,
        prefix: `${ENV}_`,
    });

    // subscribe to the generic Queue for standard Jobs
    const genericQueue = new Bull('genericQueue', {
        redis: redisOpts,
        prefix: `${ENV}_`,
    });

    const packagesQueue = new Bull('packagesQueue', {
        redis: redisOpts,
        prefix: `${ENV}_`,
    });

    packagesQueue.process('retryPackageWithoutTrackingNumber', async (job) => {
        const appConfigId = job.data?.appConfigId;
        const logger = logAndMeasure({ appConfigId }, elasticConfig);
        logger.info(`Receiving a new packages retryPackageWithoutTrackingNumber job for AppId ${appConfigId}`);
        return retryPackageWithoutTrackingNumber({ appConfigId, zohoPackageId: job.data?.zohoPackageId, logger });
    });

    mailchimpVoucherSyncQueue.process('*', async (job) => {
        const appConfigId = job.data?.appConfigId;
        const logger = logAndMeasure({ appConfigId }, elasticConfig);
        logger.info(`received mailchimp voucher sync job id ${job.id} for AppId ${appConfigId}`);
        return mailchimpVoucherSync(appConfigId, logger);
    });

    mailchimpVoucherSyncQueue.on('error', (error) => {
        // An error occured.
        console.error(error);
    });

    genericQueue.process('transactionFeesJson', async (job) => {
        const appConfigId = job.data?.appConfigId;
        const logger = logAndMeasure({ appConfigId }, elasticConfig);
        logger.info(`Received transactionFees processing job for AppConfigId ${appConfigId}`);
        return transactionFeeSync(appConfigId, job.data.parsedCsv, logger);
    });

    genericQueue.process('zohoSettingsSync', async (job) => {
        const logger = logAndMeasure({ appConfigId: job.data.appConfigId }, elasticConfig);
        logger.info('received a Zoho Settings Sync Job');
        return syncZohoSettings(job.data.appConfigId, logger);
    });

    webhookSyncWorker.process(async (job) => {
        const logger = logAndMeasure({ appConfigId: job.data.appConfigId }, elasticConfig);
        logger.info(`Received saleor webhook sync job for AppId ${job.data.appConfigId}`);
        return webhookSync(job.data.appConfigId, logger);
    });

    console.log('Worker started and subscribed to Redis Queue. Environment:', ENV);

    webhookSyncWorker.on('error', (error) => {
        // An error occured.
        console.error(error);
    });
};
main();
