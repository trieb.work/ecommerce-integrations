/* eslint-disable @typescript-eslint/no-shadow */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable no-await-in-loop */
/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable no-restricted-syntax */
import { ZohoClientInstance, SalesOrderReturn, Address, InvoiceOptional, LineItem, LineItemOptional } from '@trieb.work/zoho-inventory-ts';
import csvtojson from 'csvtojson';
import { find } from 'lodash';
import { calculateShippingTaxRate } from '@trieb.work/helpers-ts';
import { mapLimit } from 'async';

const SHIPPING_CHARGE = 5;
const SHIPPING_METHOD = 'DPD Germany';
const READY_TO_FULFILL = true;

type Ident = 'Dose (5 Lebkuchen)' | 'Lebkuchen in Folie (3)';

type importJson = {
    Nummer :string,
    Vorname: string,
    Nachname: string,
    'Adresse 1: Straße 1' :string,
    'Adresse 1: Ort' :string,
    'Adresse 1: Straße 2 (optional)' :string,
    'Firmenname (optional)' :string,
    'Adresse 1: Land' :string,
    'Adresse 1: Postleitzahl' :string,
    Titel :string,
    'Dose (5 Lebkuchen)' :string,
    'Lebkuchen in Folie (3)' :string,
}[];

// some fields we need
let zohoBillingAddressId :string = '';
let zohoShippingAddressId :string = '';

// TODO: YOU ALWAYS NEED TO CHANGE THIS !!!!!!!
const contactInfo = {
    first_name: 'Marcus',
    last_name: 'Schauerte',
    email: 'mail@heiko-simonek.de',
    company_name: 'MP Management Pool GmbH',
};
// you can set directly a Zoho Contact Id if the contact already exists
const contactId = '98644000001064630';
const billingAddress = {
    attention: '',
    address: 'Mohnweg 9',
    street2: '',
    city: 'Fürth',
    zip: '90768',
    country: 'Deutschland',
};
// Notes you want to put on every Salesorder
const notes = '';
const orderPrefix = {
    prefix: 'RP-128-',
};

const standardItems = [
    { ident: 'Dose (5 Lebkuchen)', itemSKU: 'pf-dose-5-cj-gemischt', price: 17, item_id: '' },
    { ident: 'Lebkuchen in Folie (3)', itemSKU: 'pf-leb-3-gemischt', price: 5, item_id: '' },
    { ident: 'Grußkarte in Kuvert', itemSKU: 'karte-acontech', price: 0, item_id: '' },
    { ident: 'VR-Brille', itemSKU: 'vrbrille-acontech', price: 0, item_id: '' },
];

// ============== Here the script starts =================
export default async ({ csv, json, createInvoiceDraft, zohoClient }:{ createInvoiceDraft :boolean, csv :string, json :importJson, zohoClient :ZohoClientInstance}) => {
    const rawCsvString = csv;

    // Activate this setting via query parameter. We will create an invoice after all Salesorder
    // are finished processing

    //=  =========We parse and group the addresses
    const lines = await parseCSV(rawCsvString);

    //= ======== We check for the Contact and add the billingAdress if necessary
    const contactData = await contact(contactInfo, zohoClient, contactId);

    //=    ========== We collect the needed data of the products for this order
    const lineItems = await getItems(standardItems, zohoClient);

    const shippingChargeTaxPercentage = calculateShippingTaxRate(lineItems, 'tax_percentage');
    const shippingChargeTaxId = lineItems.find((item) => item.tax_percentage === shippingChargeTaxPercentage)?.tax_id || '';

    const salesOrderIds :string[] = [];

    let summarizedShippingCharges = 0.00;
    let totalLength = lines.length;
    //= ========= Create a salesOrder for every address. We do it one by one now .. parallel does not seem to work.
    mapLimit(lines, 2, async (person) => {
        // get the contact details
        const street2 = person['Firmenname (optional)'] ? `${person['Firmenname (optional)'].trim()} ${person['Adresse 1: Straße 2 (optional)']}`
            : person['Adresse 1: Straße 2 (optional)'];
        const nummer = parseInt(person.Nummer, 10);
        const firstName = person.Titel ? `${person.Titel.trim()} ${person.Vorname.trim()}` : person.Vorname.trim();
        const shippingAddress = {
            attention: `${firstName} ${person.Nachname.trim()}`,
            address: person['Adresse 1: Straße 1'].trim(),
            street2,
            city: person['Adresse 1: Ort'].trim(),
            zip: person['Adresse 1: Postleitzahl'].trim(),
            country: person['Adresse 1: Land'].trim(),
        };

        const orderLine :LineItem[] = [];

        standardItems.map((item) => {
            if (person[item.ident as Ident]) {
                const amount = parseInt(person[item.ident as Ident], 10);
                const fullitem = lineItems.find((x) => x.item_id === item.item_id) as LineItem;
                fullitem.quantity = amount;
                if (amount > 0) orderLine.push(fullitem);
            }
            return true;
        });

        // check, if shippingAdress is already in the (everytime new) adresses array of the contact
        const contact_details = await zohoClient.getContactWithFullAdresses(contactData.contact_id);
        const shippingAdressExists = find(contact_details.addresses, shippingAddress) as Address;

        if (shippingAdressExists) {
            zohoShippingAddressId = shippingAdressExists.address_id;
        } else {
            // shippingAddress missing --> add it to the Contact
            const res = await zohoClient.addAddresstoContact(contactData.contact_id, shippingAddress, 2);
            zohoShippingAddressId = res;
        }

        // call the function which does create the salesOrder and confirm it
        const { salesorder_id, shipping_charge, status } = await createSalesOrderForPerson(contactData.contact_id, nummer, orderLine, shippingChargeTaxId, zohoClient, notes);
        totalLength -= 1;
        console.log(`Created Salesorder ${salesorder_id} number ${nummer} - Verbleibend: ${totalLength}`);
        // we confirm several salesorders at once
        if (status !== 'confirmed') {
            await zohoClient.salesorderConfirm(salesorder_id, 4);
        }
        summarizedShippingCharges += shipping_charge;
        salesOrderIds.push(salesorder_id);
        return true;
    }, (err) => {
        if (err) throw err;
    });

    if (createInvoiceDraft) {
        console.info('Starting invoice creation process.');
        // get all the needed data like line items used to create one final invoice for all Salesorders we created.
        const { line_items, contact } = await zohoClient.getInvoiceDataFromMultipleSalesOrders(salesOrderIds, contactData.contact_id, false);
        const contactPerson = contact.contact_persons?.filter((y) => y.is_primary_contact)?.map((x) => x.contact_person_id);
        const invoice :InvoiceOptional = {
            line_items,
            customer_id: contact.contact_id,
            billing_address_id: zohoBillingAddressId,
            contact_persons: contactPerson,
            shipping_charge: summarizedShippingCharges,
            shipping_charge_tax_id: shippingChargeTaxId,
        };
        const invoiceCreateResult = await zohoClient.createInvoice(invoice);
        if (invoiceCreateResult) console.log('invoice create succesfully');
    }

    return true;
};

/**
 * Function which checks if contact is available. If not, it creates it with the Billing Address.
 * @param searchBody which holds contact info
 * @returns contact_id either a new one or a retrieved one
 */
async function contact(searchBody: {
    first_name: string,
    last_name: string,
    email: string,
    company_name: string
}, zohoClient :ZohoClientInstance, zohoContactId? :string) {
    // console.log(searchBody);
    let contact_id = zohoContactId || await zohoClient.getContact(searchBody);
    // console.log("Result getContact: ", contact_id);
    if (!contact_id) { // contact does not exist, so let's create it directly.
        // create a new Contact on '/contacts' with the billingAdress of our Customer!!
        console.log('Does not exist, we create a new contact');
        // Check for "attention" not being longer than 35 signs
        if (billingAddress.attention.length > 34 || searchBody.company_name.length > 34) {
            throw new Error('++++billingAdress > 35 signs++++');
        }
        const customer_sub_type = searchBody.company_name ? 'business' : 'individual';
        const new_user = {
            contact_name: searchBody.company_name,
            billing_address: billingAddress,
            contact_persons: [{
                first_name: contactInfo.first_name,
                last_name: contactInfo.last_name,
                email: contactInfo.email,
            }],
            company_name: searchBody.company_name,
            customer_sub_type, // either 'business' or 'individual'
        };
        // console.log("New User: ", new_user);
        const result = await zohoClient.createContact(new_user);
        contact_id = result.contact_id;
        zohoBillingAddressId = result.billing_address_id;
        console.log('New contact got created. Contact ID: ', contact_id);
    } else { // Contact exists
        // get details and check fo a valid billing Adress
        console.log('Contact exists with id: ', contact_id);
        try {
            const contact_details = await zohoClient.getContactWithFullAdresses(contact_id);
            const findValidBillingAddress = find(contact_details.addresses, billingAddress) as Address;
            if (findValidBillingAddress) {
                console.log('This contact has a suitable billing address with id', findValidBillingAddress.address_id);
                zohoBillingAddressId = findValidBillingAddress.address_id;
            } else {
                // no valid billing_adress, we add it now
                console.log('no valid billing_adress, we add it now');
                const res = await zohoClient.addAddresstoContact(contact_id, billingAddress);
                zohoBillingAddressId = res;
            }
        } catch (error) {
            if (error?.response?.status === 429) console.error('We send too many requests! Got blocked');
            throw new Error(error);
        }
    }
    return {
        contact_id,
        zohoBillingAddressId,
    };
}

/**
 * Function which creates a new salesOrder and confirms it for a given person!
 * @param customerId mit den ganzen Kontaktinfos wie contact_id usw.
 * @param nummer extrahierte Zeilennummer zum Nummerieren der Bestellungen (salesorder_number)
 *
 */
async function createSalesOrderForPerson(customerId :string,
    nummer :number, lineItems :LineItem[], shipping_charge_tax_id :string, zohoClient :ZohoClientInstance, orderNotes? :string) :Promise<SalesOrderReturn> {
    const salesOrderNumber = `${orderPrefix.prefix}${nummer}`;
    try {
        const ZohoSalesOrder = await zohoClient.createSalesorder({
            customer_id: customerId,
            salesorder_number: salesOrderNumber,
            is_inclusive_tax: false,
            notes: orderNotes,
            line_items: lineItems, // Hier sind die Produkte drin!
            shipping_charge_tax_id,
            shipping_charge: SHIPPING_CHARGE,
            delivery_method: SHIPPING_METHOD,
            billing_address_id: zohoBillingAddressId,
            shipping_address_id: zohoShippingAddressId,
            custom_fields: [{
                label: 'Ready to fulfill',
                placeholder: 'cf_ready_to_fulfill',
                value: READY_TO_FULFILL,
            }],
        });
        return ZohoSalesOrder;
    } catch (error) {
        // some error handling
        if (error?.response?.data?.code === 36004) {
            console.log('We already created this salesorder. Proceeding');
            const salesorder = await zohoClient.getSalesorder(salesOrderNumber);
            return salesorder;
            // salesorder = result.metadata;
            // if this salesorder is already fully paid we don't need to create a payment
            // if (salesorder.total_invoiced_amount === salesorder.total) {
            //     console.log('This Salesorder is already fully paid in Zoho. Skipping creation of payment');
            //     salesorder_id = null;
            // }
        } if (error?.response?.data?.code === 36414) {
            console.error('You have exhausted your available sales orders for this month in Zoho. Kindly upgrade your account.');
            throw new Error('Can not procees');
        }
        console.log(error);
        // console.error(error);
        throw new Error(`Error creating Salesorder::: ${error} ${error?.response}`);
    }
}

/**
 * Funktion zum Sammeln der line_items - Daten für die Bestellungen
 * @param item array mit den SKUs der hinzuzufügenden Produkte
 * @returns array mit dem fertigen line_items Objekt für die API
 */
async function getItems(items: { ident: string; itemSKU: string; price: number; item_id: string; }[], zohoClient :ZohoClientInstance) {
    const returnItem :LineItemOptional[] = [];
    for (const item of items) {
        const { zohoItemId, zohoTaxRate, zohoTaxRateId, name } = await zohoClient.getItembySKU({ product_sku: item.itemSKU });
        item.item_id = zohoItemId;
        console.log('Result getItembySKU: ', zohoItemId, zohoTaxRate, name);
        returnItem.push({
            item_id: zohoItemId,
            name,
            rate: item.price,
            tax_percentage: zohoTaxRate,
            tax_id: zohoTaxRateId,
        });
    }
    return returnItem;
}

async function parseCSV(csv_file :string) {
    const lines = await csvtojson({ delimiter: ';', trim: true }).fromString(csv_file) as importJson;
    // console.log('Lines: ', lines);
    return lines;
}

// const groupByAddr = (items: importJson) => items.reduce(
//     (result, item) => ({
//         ...result,
//         [item.Vorname + item.Nachname + item['Adresse 1: Straße 1'] + item['Adresse 1: Ort'] + item['Adresse 1: Postleitzahl'] + item['Adresse 1: Land']]: [
//             ...(result[item.Vorname + item.Nachname + item['Adresse 1: Straße 1'] + item['Adresse 1: Ort'] + item['Adresse 1: Postleitzahl'] + item['Adresse 1: Land']] || []),
//             item,
//         ],
//     }),
//     {},
// );
