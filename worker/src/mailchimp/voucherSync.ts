import { mapLimit } from 'async';
import CustomMailChimp from '@trieb.work/mailchimp-ts';

import SaleorClient from '@trieb.work/apollo-client';
import { WinstonLoggerType } from '@trieb.work/apm-logging';
import { getShopInfo, GetVouchers } from '../lib/gql/voucherSync.gql';
import { GetVouchersQuery, GetVouchersQueryVariables, ShopInfoQuery, ShopInfoQueryVariables } from '../gqlTypes/globalTypes';
import eciPrismaClient from '../lib/prismaClient';

/**
 * This API route is used to setup and sync a mailchimp store with Saleor / Zoho. Some
 * values like vouchers can't get synchronised with webhooks, so this API route needs to get
 * triggered from time to time.
 */
const mailchimpVoucherSync = async (appConfigId :number, logger :WinstonLoggerType) => {
    const prisma = eciPrismaClient();

    const appConfig = await prisma.appConfig.findFirst({ where: { id: appConfigId }, include: { saleor: true, mailchimp: true } });
    const mailchimpConf = appConfig?.mailchimp;

    if (!mailchimpConf?.apiToken || !mailchimpConf.storeUrl) return `Job failed! No Mailchimp API token or store URL found for AppConfig ID ${appConfigId}`;
    const mailchimp = new CustomMailChimp({
        apiKey: mailchimpConf!.apiToken,
        listId: mailchimpConf.listId as string,
        storeDomain: mailchimpConf.storeUrl as string,
        storeId: mailchimpConf?.storeId || undefined,
    });

    const saleorDomain = appConfig?.saleor?.domain;
    const secretKey = appConfig?.saleor!.authToken;
    if (!secretKey) {
        logger.error('No AppToken found in DB! can not sync');
        throw new Error('AppToken missing');
    }
    const saleorConnectUrl = saleorDomain ? `https://${saleorDomain}/graphql/` : '';
    const saleorGraphQlClient = SaleorClient(saleorConnectUrl, secretKey);

    const storeData = await saleorGraphQlClient.query<ShopInfoQuery, ShopInfoQueryVariables>({
        query: getShopInfo,
    });
    if (!storeData.data.shop) throw new Error('Did not receive valid result from Saleor to create/update store in MailChimp');
    const Shop = storeData.data.shop;
    const companyAddress = Shop?.companyAddress || undefined;
    const StoreID = await mailchimp.SetupStore({
        name: Shop.name,
        companyAddress,
    });
    logger.info(`The Mailchimp Store ID is ${StoreID}`);
    if (!mailchimpConf.storeId) await prisma.appConfig.update({ where: { id: appConfigId }, data: { mailchimp: { update: { storeId: { set: StoreID } } } } });

    const VoucherData = await saleorGraphQlClient.query<GetVouchersQuery, GetVouchersQueryVariables>({
        query: GetVouchers,
        variables: {
            first: 100,
        },
    });
    if (!VoucherData.data?.vouchers?.edges) throw new Error('Did not receive valid voucher data from saleor.');
    logger.info(`Trying to sync now ${VoucherData.data.vouchers.edges.length} vouchers`);
    const vouchers = VoucherData.data.vouchers.edges.map((x) => x.node);
    try {
        await mapLimit(vouchers, 4, async (voucher) => {
            // right now we just use the default channel. Channel is not selectable
            const discountValue = voucher.channelListings?.[0].discountValue;
            const type = voucher.type === 'SHIPPING' ? 'fixed' : voucher.discountValueType.toLowerCase();
            let target = 'total';
            if (voucher.type === 'SPECIFIC_PRODUCT') {
                target = 'per_item';
            } else if (voucher.type === 'SHIPPING') {
                target = 'shipping';
            }
            if (!discountValue) {
                logger.error(`This voucher has no discount value! Can't create it in Mailchimp ${voucher.id} ${voucher.channelListings?.[0].channel.name}`);
                return true;
            }
            await mailchimp.voucherCreateAndUpdate({
                id: voucher.id,
                code: voucher.code,
                start: voucher.startDate,
                end: voucher.endDate,
                amount: type === 'percentage' ? discountValue / 100 : discountValue,
                type,
                description: '',
                target,
                count: voucher.used,
                url: mailchimpConf.storeUrl,
            });
            return true;
        });
    } catch (error) {
        logger.error(`Error trying to sync vouchers with mailchimp ${error}`);
    }

    logger.info('Mailchimp sync finished');
    return true;
};
export default mailchimpVoucherSync;
