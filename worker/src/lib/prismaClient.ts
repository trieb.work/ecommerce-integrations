import { PrismaClient } from '@prisma/client';

let prisma: PrismaClient;

const eciPrismaClient = () => {
    // Prisma - start the Postgres connection, or reuse the existing one
    // Ensure the prisma instance is re-used during hot-reloading
    // Otherwise, a new client will be created on every reload
    prisma = prisma || new PrismaClient();

    return prisma;
};
export default eciPrismaClient;
