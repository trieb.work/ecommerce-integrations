/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/no-use-before-define */
import { ZohoClientInstance } from '@trieb.work/zoho-inventory-ts';
import { LexofficeInvoiceObject, LexOfficeVoucherItem, LexofficeInstance } from '@trieb.work/lexoffice-ts';
import dayjs from 'dayjs';
// import differenceBy from 'lodash/differenceBy';
import * as Sentry from '@sentry/node';
import { round } from 'reliable-round';
import countries from 'i18n-iso-countries';
import { findLastIndex } from 'lodash';
import { apmFlush, WinstonLoggerType } from '@trieb.work/apm-logging';
import eciPrismaClient from './prismaClient';

/**
 * Mark this invoice as successfully transferred to the Accounting Software using custom field.
 * @param invoiceId
 */
const markInvoiceAsTransferred = async (invoiceId :string, zohoClient :ZohoClientInstance) => {
    await zohoClient.updateInvoice(invoiceId, { custom_fields: [{ api_name: 'cf_accounting_transferred', value: true }] });
};

export default async (appConfigId :number, logger :WinstonLoggerType) => {
    const prisma = eciPrismaClient();
    const currentTentantConfig = await prisma.appConfig.findFirst({ where: { id: appConfigId }, include: { zoho: true, lexoffice: true } });

    if (!currentTentantConfig) {
        logger.error(`Could not get a tenant confing for AppConfigId ${appConfigId}`);
        return false;
    }
    const zohoClient = new ZohoClientInstance({
        zohoClientId: currentTentantConfig.zoho?.clientId || '',
        zohoClientSecret: currentTentantConfig.zoho?.clientSecret || '',
        zohoOrgId: currentTentantConfig.zoho?.orgId || '',
    });
    await zohoClient.authenticate();

    if (!currentTentantConfig.lexoffice?.apiToken) {
        logger.error('No Lexoffice Token found in tenant config! Can not proceed');
        throw new Error('Lexoffice API Token missing. Can not proceed');
    }
    const lexofficeClient = new LexofficeInstance(currentTentantConfig.lexoffice?.apiToken);

    const olderThan = dayjs().subtract(2, 'day').format('YYYY-MM-DD');
    logger.info(`New incoming Invoice Sync Request. Getting a list of invoices older than ${olderThan} to sync from Zoho..`);
    // we created a custom view in Zoho that is used to filter all invoices that are not yet transfered.
    const rawZohoInvoicesList = zohoClient.getInvoices({ date_before: olderThan, status: 'paid', customview_id: '98644000001340007' });
    const rawLexofficeVouchers = lexofficeClient.voucherlist('salesinvoice', 'open');
    const [LexOfficeVouchers, ZohoInvoices] = await Promise.all([rawLexofficeVouchers, rawZohoInvoicesList]).catch((e) => {
        logger.error(e);
        throw e;
    });

    logger.info(`We received ${ZohoInvoices.length} invoices from Zoho, that are not yet synced with Lexoffice`);
    // format the Zoho Invoices. We write the zahlungs ID as an Remark to Lexoffice
    const transformedZohoInvoices = ZohoInvoices.map((invoice) => {
        const returner = {
            type: 'salesinvoice' as 'salesinvoice',
            voucherDate: invoice.date,
            voucherNumber: invoice.invoice_number as string,
            dueDate: invoice.due_date,
            totalGrossAmount: invoice.total,
            remark: `${invoice.reference_number} ${invoice?.custom_field_hash?.cf_zahlungs_id ? invoice.custom_field_hash.cf_zahlungs_id : ''} ${invoice?.custom_field_hash?.cf_paypal_id || ''} ${invoice?.salesorder_number || ''}`,
            zohoId: invoice.invoice_id as string,
        };
        return returner;
    });
    // calculate the difference between Lexoffice existing invoices and Zoho open invoices. We do no longer need this!
    // const toBeCreatedinLexoffice = differenceBy(transformedZohoInvoices, LexOfficeVouchers, 'voucherNumber');
    if (transformedZohoInvoices.length <= 0) logger.info('No Invoices to process - accounting is clean');

    for (const invoice of transformedZohoInvoices) {
        try {
            await createInvoicesLogic(invoice, zohoClient, lexofficeClient, logger);
        } catch (error) {
            logger.error(error?.response?.data || error);
        }
    }

    logger.info('Invoice uploading process done without errors');

    return true;
};

/**
 * Returns gross value rounded to two decimal points
 * @param net
 * @param taxrate
 */
const grossFromNet = (net:number, taxrate :number) => round(net * (1 + (taxrate / 100)), 2);

const netFormGross = (gross :number, taxrate :number) => round(gross * (1 - (taxrate / 100)), 2);

const taxAmount = (gross :number, taxrate :number) => round(gross / (1 + (taxrate / 100)) * (taxrate / 100), 2);

const createInvoicesLogic = async (invoice: {
    type: 'salesinvoice';
    voucherDate: string;
    voucherNumber: string;
    dueDate: '2020-11-08';
    totalGrossAmount: number;
    remark: string;
    zohoId: string;
}, zohoClient :ZohoClientInstance, lexofficeClient :LexofficeInstance, logger :WinstonLoggerType) => {
    logger.info(`Searching for invoice ${invoice.voucherNumber} in Lexoffice..`);
    const lexOfficeVoucherCheck = await lexofficeClient.getVoucherByVoucherNumber(invoice.voucherNumber);
    let skipCreation = false;
    let skipFileUpload = false;
    let currentLexofficeVoucherID = '';
    if (lexOfficeVoucherCheck) {
        if (lexOfficeVoucherCheck.totalGrossAmount === invoice.totalGrossAmount) {
            logger.info(`${invoice.voucherNumber} is already created in Lexoffice. Skipping creation.`);
            skipCreation = true;
            currentLexofficeVoucherID = lexOfficeVoucherCheck.id;
            if (lexOfficeVoucherCheck.files.length > 0) {
                logger.info(`Voucher has File attached as well. Skipping file upload, marking as transferred ${invoice.voucherNumber}`);
                skipFileUpload = true;
                await markInvoiceAsTransferred(invoice.zohoId, zohoClient);
                return true;
            }
        } else {
            const error = new Error(`Invoice ${invoice.voucherNumber} exist in Lexoffice, but with a different amount! This needs be checked manually`);
            console.error(error);
            Sentry.captureException(error);
            return null;
        }
    }

    logger.info(`Invoice ${invoice.voucherNumber} - Pulling full invoice data from Zoho..`);
    const fullZohoInvoice = await zohoClient.getInvoiceById(invoice.zohoId);

    if ((fullZohoInvoice.adjustment) !== 0.00) {
        skipCreation = true;
        skipFileUpload = true;
    }

    // create the line items in the appropriate Format
    let voucherItems :LexOfficeVoucherItem [] = fullZohoInvoice.line_items.map((item) => {
        const gross = grossFromNet(item.item_total, item.tax_percentage);
        if (gross <= 0) throw new Error(`Gross Value <= 0 ! Invoice: ${invoice.zohoId}, line item: ${JSON.stringify(item)}`);
        const lexofficeItem :LexOfficeVoucherItem = {
            amount: gross,
            taxRatePercent: item.tax_percentage,
            taxAmount: round(gross - item.item_total, 2),
            categoryId: '8f8664a8-fd86-11e1-a21f-0800200c9a66',
        };
        return lexofficeItem;
    });
    if (fullZohoInvoice.shipping_charge_inclusive_of_tax > 0) {
        voucherItems.push({
            amount: fullZohoInvoice.shipping_charge_inclusive_of_tax,
            taxAmount: fullZohoInvoice.shipping_charge_tax,
            taxRatePercent: fullZohoInvoice.shipping_charge_tax_percentage || 0,
            categoryId: '8f8664a8-fd86-11e1-a21f-0800200c9a66',
        });
    }

    // Add together all line items gross to calculate a gross discount in the next step
    const itemTotal = round(voucherItems.reduce((acc, current) => acc + current.amount, 0), 2);

    if (fullZohoInvoice.total === (0 || 0.00)) {
        console.log('Zero-Invoice. Skipping creation in Lexoffice');
        skipCreation = true;
        skipFileUpload = true;
        return true;
    }
    let BruttoDiscount = round(itemTotal - fullZohoInvoice.total, 4);

    voucherItems = voucherItems.map((item) => {
        // the applied Discount reduces the item amount to <= 0
        if ((item.amount - BruttoDiscount <= 0)) {
            BruttoDiscount -= item.amount;
            item.amount = 0;
            item.taxAmount = 0;
        // the discount is less than the orderline amount.
        } else {
            item.amount = round(item.amount - BruttoDiscount, 2);
            item.taxAmount = taxAmount(item.amount, item.taxRatePercent);
            BruttoDiscount = 0;
        }
        return item;
    }).filter((filterItem) => filterItem.amount !== 0);

    const taxTotal = round(voucherItems.reduce((acc, current) => acc + current.taxAmount, 0), 2);

    const contact = fullZohoInvoice?.contact_persons_details.find((x) => x.email);
    if (!contact || !contact.email) {
        console.error(`The invoice ${fullZohoInvoice.invoice_id} has no contact person or Email Address attached! 
            Please add it or we can't create this invoice in Lexoffice`);
        Sentry.captureException(
            new Error(`The invoice ${fullZohoInvoice.invoice_id} has no contact person or Email Address attached! 
            Please add it or we can't create this invoice in Lexoffice`),
        );
        return null;
    }
    const countryCode = countries.getAlpha2Code(fullZohoInvoice.billing_address.country, 'de');

    if (!skipCreation) {
        try {
            const lexofficeCustomerId = await lexofficeClient.createContactIfnotExisting({
                email: contact.email.toLowerCase(),
                firstName: contact.first_name as string,
                lastName: contact.last_name as string,
                street: fullZohoInvoice.billing_address.address,
                countryCode,
            });

            const createObject :LexofficeInvoiceObject = {
                ...invoice,
                voucherStatus: 'paid',
                totalTaxAmount: taxTotal,
                taxType: 'gross',
                useCollectiveContact: false,
                contactId: lexofficeCustomerId,
                voucherItems,
                version: 0,
            };
            console.info(`Creating now ${createObject.voucherNumber} with total gross amount ${createObject.totalGrossAmount} and contactId ${lexofficeCustomerId} in Lexoffice`);
            const lexofficeInvoiceID = await lexofficeClient.createVoucher(createObject);
            currentLexofficeVoucherID = lexofficeInvoiceID;
        } catch (error) {
            console.error('Error creating voucher or contact in Lexoffice');
            console.error(error.response?.data);
            console.error(error.response?.config);
            // Sentry.captureException(JSON.stringify(error.response));
            return true;
        }
    }
    if (!skipFileUpload) {
        try {
            logger.info(`Trying to upload file now..${invoice.voucherNumber}`);
            const { rawBuffer, filename } = await zohoClient.getDocumentBase64StringOrBuffer('invoices', fullZohoInvoice.invoice_id);
            const result = (rawBuffer && filename) ? await lexofficeClient.addFiletoVoucher(rawBuffer, filename, currentLexofficeVoucherID) : findLastIndex;

            // last step: mark the invoice as successfully transferred in Zoho
            if (result) await markInvoiceAsTransferred(fullZohoInvoice.invoice_id, zohoClient);
            return true;
        } catch (error) {
            if (error?.response?.data?.IssueList?.[0]?.i18nKey === 'action_forbidden_business_transactions_banktransaction') {
                logger.info(`${invoice.voucherNumber} can't be edited any longer! We can't add a file - marking it as transfered`);
                await markInvoiceAsTransferred(fullZohoInvoice.invoice_id, zohoClient);
            }
            logger.error('Error uploading file!', error?.response?.data || error);
            return true;
        }
    }
    return true;
};
