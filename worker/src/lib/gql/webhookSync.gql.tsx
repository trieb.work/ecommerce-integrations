import { gql } from '@trieb.work/apollo-client';

export const getWebhookById = gql`
    query getWebhookById($id: ID!) {
        webhook(id: $id) {
            name
            targetUrl
            isActive
        }
    }
`;

export const createWebhookMutation = gql`
    mutation createWebhook($name: String, $events:[WebhookEventTypeEnum!], $targetUrl: String, $secretKey: String ) {
        webhookCreate(
            input: {
                name: $name,
                events: $events,
                targetUrl: $targetUrl,
                secretKey: $secretKey
            }
        ) {
            webhookErrors {
            field
            message
            }
            webhook {
            id
            app {
                id
            }
            }
        }
    }
`;
