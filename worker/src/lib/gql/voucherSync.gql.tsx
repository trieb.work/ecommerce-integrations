import { gql } from '@trieb.work/apollo-client';

export const getShopInfo = gql`
  query shopInfo {
        shop {
            name
            companyAddress {
              companyName
              streetAddress1
              streetAddress2
              postalCode
              country {
                  code
                  country
              }
              city
              postalCode
            }
        }
    }
`;

export const GetVouchers = gql`
    query getVouchers($first: Int) {
    vouchers(first: $first) {
        edges {
        node {
            id
            code
            used
            type
            startDate
            endDate
            usageLimit
            discountValueType
            applyOncePerOrder
            applyOncePerCustomer
            channelListings {
                id
                discountValue
                channel {
                    name
                }
            }
            minSpent {
            currency
            amount
            }
        }
        }
    }
    }
`;
