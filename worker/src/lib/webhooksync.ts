import SaleorClient, { ApolloClient, NormalizedCacheObject } from '@trieb.work/apollo-client';
import { apmFlush, WinstonLoggerType } from '@trieb.work/apm-logging';
import eciPrismaClient from './prismaClient';
import { CreateWebhookMutation, CreateWebhookMutationVariables, GetWebhookByIdQuery, GetWebhookByIdQueryVariables, WebhookEventTypeEnum } from '../gqlTypes/globalTypes';
import { createWebhookMutation, getWebhookById } from './gql/webhookSync.gql';

const webhookCreateAndRegisterInDB = async (saleorGraphQlClient :ApolloClient<NormalizedCacheObject>,
    targetUrl :string, secretKey :string, appConfigId :number, logger :WinstonLoggerType) => {
    const prisma = eciPrismaClient();
    const result = await saleorGraphQlClient.mutate<CreateWebhookMutation, CreateWebhookMutationVariables>({
        mutation: createWebhookMutation,
        variables: {
            name: 'eci-auto-generated',
            targetUrl,
            events: [
                WebhookEventTypeEnum.AnyEvents,
            ],
            secretKey,
        },
    });
    if (result?.data?.webhookCreate?.webhookErrors && result.data.webhookCreate.webhookErrors.length > 0) {
        logger.error('Error creating Webhook in Saleor', result.data?.webhookCreate?.webhookErrors);
    }
    const createdWebhook = result.data?.webhookCreate?.webhook;
    logger.info('Successfully created webhook in saleor', createdWebhook?.id);
    await prisma.saleorConf.update({ where: { appConfigId }, data: { webhookID: createdWebhook?.id } });
};

const webhookSync = async (appConfigId :number, logger :WinstonLoggerType) => {
    const prisma = eciPrismaClient();

    const appConfig = await prisma.appConfig.findFirst({ where: { id: appConfigId }, include: { saleor: true } });

    const saleorDomain = appConfig?.saleor?.domain;
    const webhookId = appConfig?.saleor?.webhookID;
    const secretKey = appConfig?.saleor?.webhookToken || '';
    const saleorConnectUrl = saleorDomain ? `https://${saleorDomain}/graphql/` : '';
    const saleorAppToken = appConfig?.saleor?.authToken || '';
    const saleorGraphQlClient = SaleorClient(saleorConnectUrl, saleorAppToken);
    const targetUrl = `${appConfig?.baseUrl}/api/interconnection/saleor-incoming`;
    // TODO: Do we have a webhook Id in the config? Try to update this webhook. No Id, create webhook and save Id in config.
    if (!webhookId) {
        logger.info('We do not have a webhook yet. Creating one now...');
        await webhookCreateAndRegisterInDB(saleorGraphQlClient, targetUrl, secretKey, appConfigId, logger);
    } else {
        logger.info('We have a webhook Id already in our DB');
        const checkResult = await saleorGraphQlClient.query<GetWebhookByIdQuery, GetWebhookByIdQueryVariables>({
            query: getWebhookById,
            variables: {
                id: webhookId,
            },
        });

        if (!checkResult.data?.webhook) {
            logger.info('Webhhook ID we have in our DB does no longer exist. Creating a new one.');
            await webhookCreateAndRegisterInDB(saleorGraphQlClient, targetUrl, secretKey, appConfigId, logger);
        }
    }
    await apmFlush();
    return true;
};
export default webhookSync;
