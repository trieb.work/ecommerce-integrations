/* eslint-disable max-len */

import { ZohoClientInstance } from '@trieb.work/zoho-inventory-ts';
import { WinstonLoggerType } from '@trieb.work/apm-logging';
import eciPrismaClient from '../lib/prismaClient';

const getSalesOrderWebhook = (secretKey :string, endpointUrl :string) => ({
    webhook_name: 'eci-sales-order-webhook',
    entity: 'salesorder',
    method: 'POST',
    url: endpointUrl,
    additional_parameters: [{ param_name: 'token', param_value: secretKey }],
    entity_parameters: [] as [],
    headers: [] as [],
    user_defined_format_value: '${JSONString}',
    user_defined_format_name: '',
    description: 'DO NOT EDIT! Auto generated webhook for the ECI system',
});

const getInvoiceCustomFunction = (secretKey :string, endpointUrl :string) => ({
    function_name: 'eci_invoice_custom_function',
    description: 'DO NOT EDIT! This is the automated Invoice Custom Function',
    entity: 'invoice',
    script: `\ndata = Map();\ndata.put("JSONString",{"invoice":invoice});\ndata.put("token","${secretKey}");\nresponse = invokeurl\n[\n\turl :"${endpointUrl}"\n\ttype :POST\n\tparameters:data\n];\n`,
    include_orgvariables_params: false });

/* eslint-disable no-template-curly-in-string */
// const setupEci = async () => {
//     const workflowRuleInvoice = {
//         workflow_name: 'eci-invoice-workflow-auto-generated',
//         description: 'DO NOT EDIT! This is the auto-generated workflow for the ECI system',
//         entity: 'invoice',
//         rule_type: 'add_edit',
//         field_update: ['status'],
//         field_update_comparator: 'or',
//         apply_rule_always: true,
//         day_difference: 0,
//         rule: { columns: [{ index: 1, field: 'status', value: 'paid', comparator: 'equal', group: 'invoice' }], criteria_string: '(1)' },
//         instant_actions: [{ action_type: 'customfunction', action_id: '116240000000519169' }],
//         time_based_actions: [],
//     };

//     const workflowRuleSalesOrder = {
//         workflow_name: 'eci-sales-order-auto-generated',
//         description: 'DO NOT EDIT! This is the automated Webhook Workflow to sync all Zoho Inventory Sales with the ECI-System / Saleor',
//         entity: 'salesorder',
//         rule_type: 'add_edit',
//         field_update: [],
//         field_update_comparator: 'any',
//         apply_rule_always: true,
//         day_difference: 0,
//         rule: {
//             columns: [{ index: 1, field: 'status', value: 'draft', comparator: 'not_equal', group: 'salesorder' },
//                 { index: 2, field: 'so_shipping_status', value: 'fulfilled', comparator: 'not_equal', group: 'salesorder' }],
//             criteria_string: '(1 AND 2)' },
//         instant_actions: [{ action_type: 'webhook', action_id: '98644000000184127' }],
//         time_based_actions: [] };
// };
const syncZohoSettings = async (appConfigId :number, logger :WinstonLoggerType) => {
    const prisma = eciPrismaClient();

    const appConfig = await prisma.appConfig.findFirst({ where: { id: appConfigId }, include: { zoho: true } });
    const zohoSettings = appConfig?.zoho;
    if (!zohoSettings?.clientId || !zohoSettings?.clientSecret || !zohoSettings?.orgId) {
        logger.error('Mandatory zoho settings missing!');
        return false;
    }

    const zohoClient = new ZohoClientInstance({
        zohoClientId: zohoSettings?.clientId,
        zohoClientSecret: zohoSettings?.clientSecret,
        zohoOrgId: zohoSettings.orgId,
    });
    await zohoClient.authenticate();

    const endpointUrl = `${appConfig?.baseUrl}/api/interconnection/zoho-incoming/`;
    const salesOrderWebhookData = getSalesOrderWebhook(zohoSettings.webhookToken, endpointUrl);
    const invoiceCustomFunction = getInvoiceCustomFunction(zohoSettings.webhookToken, endpointUrl);

    if (zohoSettings.webhookID) {
        logger.info(`We already have the webhook Id ${zohoSettings.webhookID} in the Database. Trying to update..`);

        await zohoClient.updateWebhook(zohoSettings.webhookID, salesOrderWebhookData);
        logger.info('Update in Zoho succesfull');
    } else {
        logger.info('No WebhookId in the database. Creating a new one..');
        const createdWebhook = await zohoClient.createWebhook(salesOrderWebhookData);
        await prisma.appConfig.update({ where: { id: appConfigId }, data: { zoho: { update: { webhookID: createdWebhook.webhook_id } } } });
    }

    // if (zohoSettings.customFunctionID) {
    //     logger.info(`We already have the webhook Id ${zohoSettings.customFunctionID} in the Database. Trying to update..`);

    //     await zohoClient.updateCustomFunction(zohoSettings.customFunctionID, invoiceCustomFunction);
    // } else {
    //     logger.info('No customFunctionID in the database. Creating a new one..');
    //     try {
    //         const createdCustomFunction = await zohoClient.createCustomfunction(invoiceCustomFunction);
    //         await prisma.appConfig.update({ where: { id: appConfigId }, data: { zoho: { update: { customFunctionID: createdCustomFunction.customfunction_id } } } });
    //     } catch (error) {
    //         console.error(error);
    //         logger.error(error);
    //         return false;
    //     }
    // }
};
export default syncZohoSettings;
