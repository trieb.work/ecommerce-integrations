import { useRouter } from 'next/router';
import Uppy from '@uppy/core';
import { DragDrop } from '@uppy/react';
import XHRUpload from '@uppy/xhr-upload';
import { useState } from 'react';

const braintreeFileUpload = () => {
    const router = useRouter();
    const zohoOrg = router.query?.zohoOrg;
    const uppy = Uppy({
        meta: { type: 'transactionlist' },
        restrictions: {
            maxNumberOfFiles: 3,
            maxFileSize: 1048576 * 4,
            allowedFileTypes: ['.csv'],
        },
        autoProceed: true,
    });
    const [success, setSuccess] = useState(false);
    const [error, setError] = useState('');

    uppy.use(XHRUpload, {
        endpoint: `/api/braintree/uploadfees/${zohoOrg}`,
        formData: false,
    });

    uppy.on('complete', () => {
        if (uppy.getState().error) {
            setError(uppy.getState().error);
        } else {
            setSuccess(true);
        }
    });

    return (
        <div>
            {
                !zohoOrg && <span>This is not a valid link! Can not get Zoho Org</span>
            }
            {
                zohoOrg && !success && !error && (
                    <div style={{ maxWidth: '250px' }}>
                        <DragDrop
                            uppy={uppy}
                            locale={{
                                strings: {
                                    // Text to show on the droppable area.
                                    // `%{browse}` is replaced with a link that opens the system file selection dialog.
                                    dropHereOr: `Upload one or more transaction_fee_reports for ZohoOrgId ${zohoOrg}`,
                                    // Used as the label for the link that opens the system file selection dialog.
                                    browse: 'transaction_fee_report.csv',
                                },
                            }}
                        />
                    </div>
                )
            }
            {
                success && <span>File upload successful!</span>
            }
            {
                error && <span>{error}</span>
            }
        </div>
    );
};
export default braintreeFileUpload;
