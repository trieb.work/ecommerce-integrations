import AppConfiguration from 'components/saleor/AppConfig';
import { GetServerSideProps } from 'next';
import { ApolloClient, ApolloProvider, HttpLink, InMemoryCache } from '@trieb.work/apollo-client';

const deployedUrl = process.env.NEXT_PUBLIC_VERCEL_URL;
const basePath = deployedUrl ? `https://${deployedUrl}` : 'http://localhost:3000';
const eciGraphQl = `${basePath}/api/graphql/`;

const SaleorConfig = ({ saleorAuthToken, saleorDomain } : { saleorAuthToken :string, saleorDomain :string }) => {
    const apolloClient = new ApolloClient({
        link: new HttpLink({
            uri: eciGraphQl,
            headers: {
                authorization: `Bearer ${saleorAuthToken}`,
                'saleor-domain': saleorDomain,
            },
        }),
        cache: new InMemoryCache(),
    });
    return (
        <ApolloProvider client={apolloClient}>
            <div>
                <AppConfiguration />
            </div>
        </ApolloProvider>
    );
};

export const getServerSideProps :GetServerSideProps = async (ctx) => {
    const saleorDomain = ctx.req?.headers?.['saleor-domain'] as string;
    const saleorAuthToken = ctx.req?.headers?.['saleor-token'] as string;

    console.log(`Saleor Headers: ${JSON.stringify(ctx.req.headers)}`);

    return { props: { saleorAuthToken, saleorDomain } };
};

export default SaleorConfig;
