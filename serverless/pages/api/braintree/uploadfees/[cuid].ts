import setupRequestBraintreeUpload from 'lib/setupRequest/setupRequestBraintreeUpload';
import { withSentry } from 'lib/withSentryHOF';
import { NextApiRequest, NextApiResponse } from 'next';
import neatCsv from 'neat-csv';
import Bull from 'bull';

/**
 * Handle Incoming file uploads for transaction list CSVs
 */
export default withSentry(async (req :NextApiRequest, res :NextApiResponse) => {
    if (req.method.toLowerCase() !== 'post') return res.status(500).json({ error: 'Wrong HTTP method' });
    if (req.headers?.['content-type'] !== 'text/csv') return res.status(500).json({ error: 'Wrong file type' });

    const { logger, valid, redisOpts, currentTentantConfig } = await setupRequestBraintreeUpload(req);
    if (!valid) return res.status(404).json({ error: 'this user is not authorized' });

    logger.info('Incoming braintree transaction file upload');

    const genericQueue = new Bull('genericQueue', {
        redis: redisOpts,
        prefix: `${process.env.APM_ENV}_`,
    });

    const parsedCsv = await neatCsv(req.body);
    await genericQueue.add('transactionFeesJson', { appConfigId: currentTentantConfig.id, parsedCsv }, {
        attempts: 5,
        backoff: {
            type: 'exponential',
            delay: 100000,
        },
    });
    if (parsedCsv) return res.status(200).send('success');
    return res.status(500).end();
});
