/* eslint-disable import/no-extraneous-dependencies */
import 'reflect-metadata';
import { ApolloServer } from 'apollo-server-micro';
import { AppConfigResolver } from 'lib/ApolloServer/resolvers/AppConfig';
import { buildSchema } from 'type-graphql';
import { NextApiRequest, NextApiResponse } from 'next';
import { customAuthChecker } from 'modules/saleor/auth';
import Cors from 'cors';
import initMiddleware from 'lib/init-middleware';
import { AppConfigRelationsResolver, UpdateAppConfigResolver, TrackAndTraceConfRelationsResolver } from '@generated/type-graphql';
import { PrismaClient } from '@prisma/client';
import { SaleorDataResolver } from 'lib/ApolloServer/resolvers/SaleorData';

const prisma = new PrismaClient({
});

const cors = initMiddleware(
    Cors({
        methods: ['GET', 'POST', 'OPTIONS'],
    }),
);

let apolloServerHandler: (req: NextApiRequest, res: NextApiResponse) => Promise<void>;

const getApolloServerHandler = async () => {
    if (!apolloServerHandler) {
        const schema = await buildSchema({
            resolvers: [AppConfigResolver, AppConfigRelationsResolver, UpdateAppConfigResolver, SaleorDataResolver, TrackAndTraceConfRelationsResolver],
            validate: false,
            authChecker: customAuthChecker,
        });
        apolloServerHandler = new ApolloServer({ schema,
            playground: true,
            tracing: true,
            introspection: true,
            // we add the complete req Object to the context
            context: ({ req }) => ({ req, prisma }) }).createHandler({
            path: '/api/graphql/',
        });
    }
    return apolloServerHandler;
};

export default async (req: NextApiRequest, res: NextApiResponse) => {
    await cors(req, res);
    const apolloServerReturn = await getApolloServerHandler();
    return apolloServerReturn(req, res);
};

export const config = { api: { bodyParser: false } };
