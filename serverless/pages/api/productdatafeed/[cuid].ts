/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable import/no-unresolved */

import {
    withSentry,
} from 'lib/withSentryHOF';
import md5 from 'md5';
import { NextApiRequest, NextApiResponse } from 'next';

import setupRequestDataFeed from 'lib/setupRequest/setupRequestDatafeed';
import { apmFlush } from '@trieb.work/apm-logging';
import { generateProductDataFeed } from 'lib/generateProductDataFeed';

// ToDo: add language values - make language of the feed configurable

export default withSentry(async (req :NextApiRequest, res :NextApiResponse) => {
    const { logger, saleorGraphQLClient, valid, storefrontProductUrl, channel, feedVariant } = await setupRequestDataFeed(req);

    if (!valid) {
        logger.info('Sending 400 - no valid request');
        return res.status(400).end();
    }
    if (!channel) {
        logger.info('No channel given! Can\'t create feed');
        return res.status(400).end();
    }

    logger.info('Creating new product data feed');
    const returnValue = await generateProductDataFeed(saleorGraphQLClient, channel, storefrontProductUrl, feedVariant);
    const hash = md5(returnValue);

    res.setHeader('Content-Type', 'text/csv');
    res.setHeader('Content-Disposition', `attachment; filename=productdatafeed-${hash}.csv`);
    res.setHeader('Cache-Control', 's-maxage=1, stale-while-revalidate');
    await apmFlush();
    return res.send(returnValue);
});
