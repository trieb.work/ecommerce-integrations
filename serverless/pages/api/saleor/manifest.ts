import { withSentry } from 'lib/withSentryHOF';
import { saleorManifest } from 'modules/saleor/appmanifest';
import { NextApiRequest, NextApiResponse } from 'next';

export default withSentry(async (req :NextApiRequest, res :NextApiResponse) => {
    if (req.method !== 'GET') return res.status(500).send('Wrong HTTP method');

    return res.status(200).json(saleorManifest());
});
