/* eslint-disable no-underscore-dangle */
import { withSentry } from 'lib/withSentryHOF';
import { NextApiRequest, NextApiResponse } from 'next';
import setupRequestSaleor from 'lib/setupRequest/setupRequestSaleor';
import Bull from 'bull';
import { captureException } from '@sentry/node';

export default withSentry(async (req :NextApiRequest, res :NextApiResponse) => {
    const { method } = req;
    const { saleorAppToken, valid, saleorDomain, getTenantConfig, updateTenantConfig, createTenantConfig, logger, redisOpts } = await setupRequestSaleor({ req });
    if (method !== 'POST') return res.status(500).send('Wrong Method');

    logger.info('Incoming Register from saleor');

    if (!saleorAppToken || !saleorDomain) return res.status(500).send('Mandatory parameters missing');

    if (!valid) return res.status(401).send('Auth Token not valid');

    const result = await getTenantConfig();

    const webhookSyncWorker = new Bull('webhookSyncJob', {
        redis: redisOpts,
        prefix: `${process.env.APM_ENV}_`,
    });

    if (!result) {
        logger.info('New Client - creating Config');
        const createdSettings = await createTenantConfig();

        // Sync Webhook Job
        try {
            const syncJobUpdateResult = await webhookSyncWorker.add({
                appConfigId: createdSettings.id,
            }, { attempts: 5, backoff: { type: 'exponential', delay: 10000 } });
            logger.info(`Created a new WebhookSyncJob with Id ${syncJobUpdateResult.id}`);
        } catch (error) {
            captureException(error);
        }
    } else {
        logger.info('Client was already registered - updating');
        try {
            await updateTenantConfig(result.id);
            // Sync Webhook Job
            try {
                const syncJobUpdateResult = await webhookSyncWorker.add({
                    appConfigId: result.id,
                }, { attempts: 5, backoff: { type: 'exponential', delay: 10000 } });
                logger.info(`Created a new WebhookSyncJob with Id ${syncJobUpdateResult.id}`);
            } catch (error) {
                logger.error(error);
                captureException(error);
            }
        } catch (error) {
            logger.error(error);
            return res.status(500).end();
        }
    }

    return res.status(200).end();
});
