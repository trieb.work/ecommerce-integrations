import { PackageTrackerInstance, Tracker } from '@trieb.work/internal-package-trackers';
import { withSentry } from 'lib/withSentryHOF';
import { NextApiRequest, NextApiResponse } from 'next';
import setupRequestEasypost from 'lib/setupRequest/setupRequestEasypost';
import { apmFlush } from '@trieb.work/apm-logging';
import getContactData from 'modules/easypost/getContactData';
import { lowerCase } from 'lodash';

// These messages are used from carriers when the parcel arrives at the delivery centre
// there is no way to get this information directly from easypost.
const parcelDeliveryCentre = ['At parcel delivery centre.'];

/**
 * Incoming Webhooks for shipment updates, for example from easypost trackers.
 */
export default withSentry(async (req :NextApiRequest, res :NextApiResponse) => {
    const { logger, valid, zohoClient, packageTrackerClient, trackAndTraceConf } = await setupRequestEasypost(req);

    const easypostBody :Tracker = req.body;

    const easyTrackingEvent = easypostBody?.result?.status;
    const trackingTimeline = easypostBody.result?.tracking_details;

    /**
     * Checks, if this customer has in_transit E-Mails enabled
     */
    const inTransitEmailEnabled = trackAndTraceConf.trackAndTraceEmails.find((x) => x.inTransitEmailEnabled);

    // check if the last message of the parcel Delivery Timeline is "at parcel delivery centre";
    const atParcelDeliveryCentre = !!parcelDeliveryCentre.includes(trackingTimeline[trackingTimeline?.length - 1]?.message);
    logger.info(`Incoming easypost event - CUID ${req?.query?.cuid}, Event ID ${easypostBody.id}`);

    if (!valid) {
        logger.error('The incoming call is not valid!');
        await apmFlush();
        return res.status(500).end();
    }
    const trackingNumber = easypostBody.result.tracking_code;

    // we send the in_transit email when the parcel is in_transit or pre_transit
    // if (['in_transit', 'pre_transit'].includes(easyTrackingEvent) && !atParcelDeliveryCentre && inTransitEmailEnabled) {
    //     logger.info(`The package ${trackingNumber} is ${easyTrackingEvent}. Checking if we need to send out in_transit Email..`);

    //     const fullPackageData = await zohoClient.getPackageByTrackingNumber(trackingNumber);

    //     if (!fullPackageData) {
    //         logger.error(`No package for tracking number ${trackingNumber} found! Returning 200 ok.`);
    //         return res.status(200).end();
    //     }

    //     // check in the package comments, if we already send out the "in transit" or outfordelivery email. In rare cases,
    //     // the webhooks don't arrive in time and we send out in transit after outfordelivery;
    //     const inTransitEmailComment = fullPackageData?.comments.find((x) => x.description.includes('In Transit Email sent'))
    //         || fullPackageData?.comments.find((x) => x.description.includes('Out for Delivery Email sent'));

    //     if (inTransitEmailComment) {
    //         logger.info('in_transit email already sent! Finishing process');
    //         return res.status(200).end();
    //     }
    //     if (!fullPackageData.email) {
    //         logger.info('Can\'t send out tracking notification. This package has no contact person with email attached.');
    //         return res.status(200).end();
    //     }
    //     if (fullPackageData.status === 'delivered') {
    //         logger.info('This package is already delivered. We don\'t send the out_for_delivery mail');
    //         return res.status(200).end();
    //     }

    //     const { shipmentNotificationsDisabled,
    //         customer,
    //         userLanguage, contactPersonEmail,
    //         trackingPortalUrl, emailVariables, contactPerson } = await getContactData(zohoClient, fullPackageData);
    //     if (shipmentNotificationsDisabled) {
    //         logger.info(`The user ${customer.contact_id} has shipment notifications disabled. Ending process`);
    //         return res.status(200).end();
    //     }
    //     if (!contactPersonEmail) {
    //         logger.info(`Customer ${customer.contact_id} and contact person ${contactPerson} has no e-mail attached`);
    //         return res.status(200).end();
    //     }
    //     if (!trackingPortalUrl) {
    //         logger.error(`Shipment Order Notes with portal URL not found! Package Id: ${fullPackageData.package_id}`);
    //         await apmFlush();
    //         return res.status(200).end();
    //     }

    //     const trackAndTraceEmailData = trackAndTraceConf.trackAndTraceEmails?.find((x) => lowerCase(x.language) === userLanguage);
    //     const trackAndTraceEmailTemplateName = trackAndTraceEmailData?.inTransitEmailTemplateName;
    //     if (!trackAndTraceEmailTemplateName) {
    //         logger.error(`No E-Mail template name found for in_transit_email and language ${userLanguage}. Can't send out E-Mail`);
    //         return res.status(200).end();
    //     }
    //     logger.info(`Sending out now "in_transit" email to Zoho customer ${customer.contact_id} with E-Mail template name ${trackAndTraceEmailTemplateName} \
    //      and this variables: ${JSON.stringify(emailVariables)}`);
    //     const emailResponse = await packageTrackerClient.sendMail({
    //         toEmail: contactPersonEmail,
    //         subject: trackAndTraceEmailData.inTransitEmailSubject,
    //         template: trackAndTraceEmailData.inTransitEmailTemplateName,
    //         variables: emailVariables,
    //     });
    //     if (emailResponse) {
    //         await zohoClient.addComment('packages', fullPackageData.package_id, `In Transit Email sent to ${contactPersonEmail}`);
    //     } else {
    //         logger.error('Did not receive an answer from Mailgun when trying to send in_transit_email');
    //     }
    // }

    // if (atParcelDeliveryCentre || easyTrackingEvent === 'out_for_delivery') {
    //     logger.info(`The package ${trackingNumber} is ${atParcelDeliveryCentre ? 'at the parcel delivery centre'
    //         : 'out for delivery!'}`);
    //     const fullPackageData = await zohoClient.getPackageByTrackingNumber(trackingNumber);

    //     if (!fullPackageData) {
    //         logger.error(`No package for tracking number ${trackingNumber} found! Returning 200 ok.`);
    //         return res.status(200).end();
    //     }
    //     // check in the package comments, if we already send out the "out for delivery" email;
    //     const outForDeliveryEmailComment = fullPackageData?.comments.find((x) => x.description.includes('Out for Delivery Email sent'));

    //     if (outForDeliveryEmailComment) logger.info('We already sent out the out_for_delivery_email for this user.');
    //     if (!fullPackageData.email) logger.info('Can\'t send out tracking notification. This package has no contact person with email attached.');
    //     if (fullPackageData.status === 'delivered') {
    //         logger.info('This package is already delivered. We don\'t send the out_for_delivery mail');
    //         return res.status(200).end();
    //     }

    //     // Currently we send just tracking updates for STORE and SO orders. Could be changed in the future.
    //     if (fullPackageData?.salesorder_number?.match(/STORE-|SO-/) && fullPackageData.email && !outForDeliveryEmailComment) {
    //         // We need to get the full customer of the package in order to get the language and other data
    //         const { shipmentNotificationsDisabled, customer, userLanguage, contactPersonEmail, trackingPortalUrl } = await getContactData(zohoClient, fullPackageData);

    //         if (shipmentNotificationsDisabled) {
    //             logger.info(`The user ${customer.contact_id} has shipment notifications disabled. Ending process`);
    //             return res.status(200).end();
    //         }
    //         const deliveryTime = atParcelDeliveryCentre ? PackageTrackerInstance.getNextBusinessDay() : undefined;
    //         logger.info(`We send out a Out_for_Delivery Notification to ${contactPersonEmail} ${deliveryTime
    //             ? `- delayed to ${deliveryTime.rfc2822}` : ' right now'}`);

    //         if (!trackingPortalUrl) {
    //             logger.error(`Shipment Order Notes with portal URL not found! Package Id: ${fullPackageData.package_id}`);
    //             await apmFlush();
    //             return res.send('success');
    //         }
    //         // await callZoho.sendEmailWithTemplate('packages', fullPackageData.package_id, 'Out_for_delivery', fullPackageData.email, userLanguage);
    //         const { body, subject } = await zohoClient.getEmailTemplateData('shipmentorders', fullPackageData.shipment_id, 'Out_for_delivery', userLanguage);
    //         const bodyWithTracking = body.replace('%TRACKING_PORTAL_URL%', trackingPortalUrl);
    //         const emailResponse = await packageTrackerClient.sendMail({
    //             toEmail: contactPersonEmail,
    //             subject,
    //             html: bodyWithTracking,
    //             replyTo: 'servus@pfefferundfrost.de',
    //             deliveryTime: deliveryTime?.unixSeconds });
    //         if (emailResponse) {
    //             await zohoClient.addComment('packages', fullPackageData.package_id, `Out for Delivery Email sent to ${contactPersonEmail}`);
    //         } else {
    //             logger.error('Did not receive an answer from Mailgun!');
    //         }
    //     }
    // }

    await apmFlush();
    res.send('success');
});
