/* eslint-disable prefer-destructuring */
import { withSentry } from 'lib/withSentryHOF';
import { NextApiRequest, NextApiResponse } from 'next';
import { apmFlush } from '@trieb.work/apm-logging'
import paypal from 'paypal-rest-sdk';

const dev = process.env.APM_ENV === 'dev';

const payPalClientId = process.env.PAYPAL_CLIENT_ID;
const payPalClientSecret = process.env.PAYPAL_CLIENT_SECRET;
paypal.configure({
    mode: dev ? 'sandbox' : 'live',
    client_id: 'EBWKjlELKMYqRNQ6sYvFo64FtaRLRR5BdHEESmha49TM',
    client_secret: 'EO422dn3gQLgDbuwqTjzrFgFtaRLRR5BdHEESmha49TM',
});

export default withSentry(async (req :NextApiRequest, res :NextApiResponse) => {
    console.info('Incoming PayPal Webhook');
    if (dev) console.log('working in dev mode with Sandbox Account');
    if (!payPalClientId) throw new Error('Paypal Client Id missing');
    if (!payPalClientSecret) throw new Error('Paypal Client Secret missing');
    const body = req.body;

    const webhookAsync = async () => new Promise((resolve, reject) => {
        paypal.notification.webhookEvent.get(body.id, (error, response) => {
            if (error) {
                reject(error);
            } else {
                resolve(response);
            }
        });
    });

    const webhookData = await webhookAsync();
    console.log('data', webhookData);

    await apmFlush();
    return res.send('success');
});
