import { apmFlush } from '@trieb.work/apm-logging';
import eciPrismaClient from 'lib/setupRequest/prismaSetup';
import { withSentry } from 'lib/withSentryHOF';
import { NextApiRequest, NextApiResponse } from 'next';

export default withSentry(async (_req :NextApiRequest, res :NextApiResponse) => {
    const prisma = eciPrismaClient();
    /**
     * Just pull the common configg to check the DB availability
     */
    const commonConf = await prisma.commonConfig.findFirst({ where: { id: 1 }, include: { elasticsearch: true } });

    if (commonConf) {
        res.status(200).json({
            Status: 'Green',
        });
    } else {
        res.status(400).json({
            Status: 'Red',
        });
    }

    await apmFlush();
});
