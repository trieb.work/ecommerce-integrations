import { withSentry } from 'lib/withSentryHOF';
import { NextApiRequest, NextApiResponse } from 'next';
import { ZohoClientInstance } from '@trieb.work/zoho-inventory-ts';
import { validateDate } from '@trieb.work/helpers-ts';
import eciPrismaClient from 'lib/setupRequest/prismaSetup';

/**
 * Little API route to get bundles and packages by a specific user in a specific time frame. Used for statistics and billing purposes
 * Call with Querystring from, to and createdByName
 */
export default withSentry(async (req :NextApiRequest, res :NextApiResponse) => {
    const cuid = req?.query?.cuid as string;

    const prismaClient = eciPrismaClient();
    const appConfig = await prismaClient.appConfig.findFirst({ where: { zoho: { cuid }}, include: { zoho: true }})
    const zohoSettings = appConfig.zoho;
    const zohoClient = new ZohoClientInstance({
        zohoClientId: zohoSettings?.clientId,
        zohoClientSecret: zohoSettings?.clientSecret,
        zohoOrgId: zohoSettings.orgId,
    });
    await zohoClient.authenticate();
    const from = req.query?.from as string;
    if (!from) return res.status(400).send('From value missing! Add with queryparameter "from"');
    if (!validateDate(from, 'YYYY-MM-DD')) res.status(400).send('From value is not valid!');
    const to = req.query?.to as string;
    if (!to) return res.status(400).send('From value missing! Add with queryparameter "to"');
    if (!validateDate(to, 'YYYY-MM-DD')) res.status(400).send('To value is not valid!');
    const createdByName = req.query?.createdByName as string;

    const result1 = zohoClient.getBundles(from, to, createdByName);
    const result2 = zohoClient.getPackagesTotal({ from, to });
    const [totalBundles, totalPackages] = await Promise.all([result1, result2]);

    return res.json({
        from,
        to,
        totalBundles,
        totalPackages,
    });
});
