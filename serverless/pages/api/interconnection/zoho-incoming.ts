/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable no-console */
/* eslint-disable no-continue */
/* eslint-disable no-await-in-loop */
/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable no-restricted-syntax */
/* eslint-disable import/no-unresolved */
import { withSentry } from 'lib/withSentryHOF';
import * as Sentry from '@sentry/node';
// import {
//     stockLevelMutation,
//     addPrivateMetaMutation,
//     productVariant,
// } from 'lib/gql/saleor.gql';
import { NextApiRequest, NextApiResponse } from 'next';
import { sendPaidInvoice } from 'modules/zoho/paidInvoice';
// import { getSaleorWarehouses } from 'modules/zoho/common';
import { apmFlush } from '@trieb.work/apm-logging';
import { packagesLogic } from 'modules/zoho/package';
import captureOrderSaleor from 'modules/zoho/captureOrderSaleor';
import winston from 'winston';
import { CustomField, Invoice, SalesOrder, ZohoClientInstance } from '@trieb.work/zoho-inventory-ts';
import { ApolloClient, NormalizedCacheObject } from '@trieb.work/apollo-client';
import setupRequestZoho from 'lib/setupRequest/setupRequestZoho';
import CustomMailChimp from '@trieb.work/mailchimp-ts';
import Bull from 'bull';
// import { AddPrivateMetaMutation, AddPrivateMetaMutationVariables } from 'gqlTypes/globalTypes';

let globalLogger :winston.Logger;
let client :ApolloClient<NormalizedCacheObject>;

export default withSentry(async (req :NextApiRequest, res :NextApiResponse) => {
    const { logger, saleorGraphQLClient, valid, fullTenantConfig, redisOpts, zohoClient, packageTrackerClient } = await setupRequestZoho(req);
    client = saleorGraphQLClient;
    globalLogger = logger;

    // authenticate the call first
    if (!valid) {
        logger.info('Incoming Zoho Webhook call not valid! Returning 500');
        return res.status(500).send('Unauthorized or not valid');
    }

    await zohoClient.authenticate();

    const mailchimpSettings = fullTenantConfig?.mailchimp;
    const mailchimpActive = mailchimpSettings?.active;

    const mailchimp = mailchimpActive ? new CustomMailChimp({
        listId: mailchimpSettings.listId,
        apiKey: mailchimpSettings.apiToken,
        storeDomain: mailchimpSettings.storeUrl,
        logger,
    }) : null;
    const body = JSON.parse(req.body.JSONString);

    const invoice :Invoice = body?.invoice;

    // start the process for salesorders
    if (body.salesorder) {
        const { salesorder } : { salesorder :SalesOrder } = body;
        globalLogger.info('Incoming Webhook from Zoho from type "Salesorder"');
        // We don't do anything on drafts
        if (salesorder.order_status === 'draft') {
            globalLogger.info('Received status "draft" - answering with 200 ok');
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify({
                Status: 'Success',
            }));
        }

        // update the stock level of the products in the corresponding salesorder.
        // TODO: find a way to not run this on every request. Maybe redis caching -
        // this will always produce API-calls back to Zoho. Needed at most once every five minutes
        // per product.
        // try {
        //     await updateStockandMeta(body.salesorder, zohoClient);
        // } catch (error) {
        //     globalLogger.error(error);
        //     Sentry.setTag('function', 'updateStockandMeta');
        //     Sentry.captureException(error);
        // }

        if (salesorder.shipped_status === 'shipped') {
            globalLogger.info('This Salesorder is fully shipped. Setting custom field "ready to fulfill" to false..');
            try {
                await setSalesorderFulfillStatus(salesorder.salesorder_id, salesorder.custom_fields, false, zohoClient);
            } catch (error) {
                globalLogger.error(error);
                Sentry.captureException(error);
            }
        }

        // we have packages in the Webhook - do stuff with them
        // if (body.salesorder?.packages) {
        //     const { packages } = salesorder;

        //     // start the packages Logic. Fulfill and print, etc..
        //     for (const pkg of packages) {
        //         await packagesLogic(pkg, body.salesorder, globalLogger, client, mailchimp, zohoClient, packageTrackerClient);
        //         const trackingNumber = pkg.tracking_number || pkg?.shipment_order?.tracking_number;
        //         if (!trackingNumber) {
        //             const packagesQueue = new Bull('packagesQueue', {
        //                 redis: redisOpts,
        //                 prefix: `${process.env.APM_ENV}_`,
        //             });
        //             // check if we already have an active or finished job for this package
        //             const jobs = await packagesQueue.getJobs(['active', 'delayed', 'completed']);
        //             const filteredForExistingJobs = jobs.find((job) => job.data.zohoPackageId === pkg.package_id);
        //             if (!filteredForExistingJobs) {
        //                 logger.info('Package without tracking number found. We add this job to the task-queue and let the worker check in 5 minutes again.');
        //                 // add this package to the retryQueue to make sure, that after three minutes there is still no tracking number
        //                 await packagesQueue.add('retryPackageWithoutTrackingNumber', { appConfigId: fullTenantConfig.id, zohoPackageId: pkg.package_id }, {
        //                     delay: 300 * 1000,
        //                 });
        //             }
        //         }
        //     }
        // }
    }

    if (invoice?.status === 'paid') {
        // eslint-disable-next-line max-len
        globalLogger.info(`Incoming Webhook from Zoho from type "Paid Invoice": Invoice Number: ${invoice.invoice_number}. Email status (invoice.is_emailed): ${invoice?.is_emailed}`);
        // Send out the invoice via email only when STORE order and only when this invoice is not yet emailed
        if (invoice.is_emailed) logger.info('This Invoice is already emailed. Do not need to do this again');
        const result1 = (invoice.reference_number?.includes('STORE-') && !invoice.is_emailed) ? sendPaidInvoice(invoice, zohoClient, logger) : true;
        const result2 = captureOrderSaleor(body.invoice, globalLogger, client);
        await Promise.all([result1, result2]);
    }

    globalLogger.info('Finishing process');
    await apmFlush();

    return res.status(200).json({ message: 'success' });
});

// TODO: move this functionality to the worker. Add a job named with SKU, so that AT MOST ONE job is running per item. Add it with a delay of three minutes,
// so that burst calls get aggregated. Maybe wait for the bull debounce functionality.
/**
 * Update the stock level per order line item. Fetches needed data from Saleor and Zoho. Updates the metdata in Saleor (adding Zoho item_id)
 * @param {object} body
 */
// async function updateStockandMeta(salesorder :SalesOrder, zohoClient :ZohoClientInstance) {
//     const SaleorWarehouses = await getSaleorWarehouses(client);

//     // for every lineitem of a salesorder we pull more data from Zoho and update Saleor
//     for (const item of salesorder?.line_items) {
//         globalLogger.info('Pulling more precise article data from Zoho now..');
//         const zohoProductItemId = item.product_id || item.item_id;
//         if (!zohoProductItemId) throw new Error(`Product ID not found in item! Please check the item: ${JSON.stringify(item)}`);
//         const result = await zohoClient.getItem({
//             product_id: zohoProductItemId,
//         });

//         const saleorIdResult = await client.query({
//             query: productVariant,
//             variables: {
//                 sku: result.sku,
//             },
//         });
//         const saleor_id = saleorIdResult.data?.productVariant?.id;
//         if (!saleor_id) {
//             // eslint-disable-next-line max-len
//             globalLogger.info(`No Valid data from Saleor. This product does most likely not exist in Saleor! SKU: ${result.sku}, Saleor Result: ${JSON.stringify(saleorIdResult)}`);
//             return null;
//         }
//         const saleorStock = saleorIdResult.data.productVariant.stocks;
//         globalLogger.info(`Updating Stock level for ${result.sku} Saleor ID ${saleor_id}`);

//         for (const warehouse of result.warehouses) {
//             // we filter the saleor warehouses for the current Warehouse from Zoho
//             const temp = SaleorWarehouses.find((x) => x.node.name === warehouse.warehouse_name);
//             if (!temp) {
//                 globalLogger.info(`We couldn't find a matching Warehouse in Saleor for Zoho Warehouse ${warehouse.warehouse_name}`);
//                 continue;
//             }
//             const saleorWarehouseId = temp.node.id;
//             const saleorQuantityAllocated = saleorStock.find((x) => x.warehouse.id === saleorWarehouseId)?.quantityAllocated || 0;
//             const stock = parseInt(warehouse.warehouse_actual_available_for_sale_stock, 10);

//             // the actual stock is the stock + the current allocated stock in Saleor .. this is a little workaround
//             let actualSaleorStock = stock + parseInt(saleorQuantityAllocated, 10);
//             // Stock can't be negative for Saleor
//             if (actualSaleorStock < 0) actualSaleorStock = 0;

//             const data = await client.mutate({
//                 mutation: stockLevelMutation,
//                 variables: {
//                     variantId: saleor_id,
//                     warehouseId: saleorWarehouseId,
//                     quantity: actualSaleorStock,
//                 },
//             });
//             if (data.data.productVariantStocksUpdate.bulkStockErrors.length > 0) {
//                 throw new Error(`Error updating stocks in Saleor ${JSON.stringify(data.data.productVariantStocksUpdate.bulkStockErrors)}`);
//             }

//             /**
//              * Set the ZohoID also at the Saleor Product Metadata.
//              */
//             const MetadataResult = await client.mutate<AddPrivateMetaMutation, AddPrivateMetaMutationVariables>({
//                 mutation: addPrivateMetaMutation,
//                 variables: {
//                     id: saleor_id,
//                     input: [{ key: 'ZohoInventory.item_id', value: result.item_id }],
//                 },
//             });
//             if (!MetadataResult.data || MetadataResult.data.updatePrivateMetadata.metadataErrors.length > 0) {
//                 throw new Error(`Error updating metadata in Saleor. 
//                     Received no answer or this error:${JSON.stringify(MetadataResult.data.updatePrivateMetadata.metadataErrors)}`);
//             }

//             globalLogger.info('Successfully updated the stock level in Saleor');
//         }
//     }
//     return true;
// }

/**
 * Set the custom field ready_to_fulfill to true or false in a salesorder
 * @param salesorderId
 * @param customFields
 * @param status
 */
const setSalesorderFulfillStatus = async (salesorderId :string, customFields :CustomField[], status:boolean, zohoClient :ZohoClientInstance) => {
    const customField = customFields.find((x) => x.placeholder === 'cf_ready_to_fulfill');
    if (customField.value === status) {
        globalLogger.info(`Ready to fulfill is already ${status}. Skipping.`);
        return true;
    }
    const customFieldId = customField.customfield_id;
    if (!customFieldId) throw new Error('The custom field "cf_ready_to_fulfill" could not be found at this salesorder! Create it in Zoho first as mandatory field');
    const updateData = {
        custom_fields: [
            {
                customfield_id: customFieldId,
                value: status,
            },
        ],
    };
    try {
        await zohoClient.updateSalesorder(salesorderId, updateData, 3);
    } catch (error) {
        globalLogger.error(error);
        Sentry.captureException(error.error || error);
    }
    return true;
};
