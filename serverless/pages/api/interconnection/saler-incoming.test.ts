import axios from 'axios';
import { ORDER_CREATED_NO_PAYMENT_SAMPLE_WEBHOOK } from '../../../test-files/saleor-sample-webhooks';

it('Works to send a local ORDER_CREATED webhook without a payment', async () => {
    const sendWebhookResponse = await axios({
        method: 'POST',
        url: 'http://localhost:3000/api/interconnection/saleor-incoming',
        data: ORDER_CREATED_NO_PAYMENT_SAMPLE_WEBHOOK,
        headers: {
            'Saleor-Domain': 'testing--saleor.triebwork.com',
            'Saleor-Event': 'ORDER_CREATED',
        },
    });

    expect(sendWebhookResponse.status).toBe(200);
});
