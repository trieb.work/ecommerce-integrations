/* eslint-disable no-case-declarations */
/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-console */
import assert from 'assert';
import dayjs from 'dayjs';
import {
    withSentry,
} from 'lib/withSentryHOF';
import { find } from 'lodash';
import isUrl from 'is-valid-http-url';
import countries from 'i18n-iso-countries';
import { apmFlush } from '@trieb.work/apm-logging';
import * as Sentry from '@sentry/node';
import { round } from 'reliable-round';
import user_transformer from 'lib/saleor2zoho/user_transformer';

import {
    getProduct,
} from 'lib/gql/saleor.gql';
import {
    GetProductQueryQuery, GetProductQueryQueryVariables,
} from 'gqlTypes/globalTypes';
import Braintree from 'lib/Braintree';
import { NextApiRequest, NextApiResponse } from 'next';
import winston from 'winston';
import {
    Address,
    Contact,
    ContactPerson,
    ContactWithFullAddresses,
    CustomerPayment, SalesOrder, ZohoClientInstance,
} from '@trieb.work/zoho-inventory-ts';
import { calculateShippingTaxRate } from '@trieb.work/helpers-ts';
import setupRequestSaleor from 'lib/setupRequest/setupRequestSaleor';
import CustomMailChimp, { MailchimpOrder, ECommerceCustomer } from '@trieb.work/mailchimp-ts';
import { SaleorWebhookAddress, SaleorWebhookOrder, WebhookPayment } from 'types/saleor';
// import getPaymentGatewayMetadata from 'modules/saleor/getPaymentGatewayMetadata';

export default withSentry(async (req: NextApiRequest, res: NextApiResponse) => {
    const { logger,
        saleorGraphQLClient,
        valid, saleorEvent, mailchimpActive,
        fullTenantConfig, unauthenticatedSaleorGraphQLClient, zohoClient } = await setupRequestSaleor({ req });

    // const braintreeSettings = fullTenantConfig?.braintree;
    // const braintree = braintreeSettings?.active ? new Braintree({
    //     merchantId: braintreeSettings.merchantId,
    //     publicKey: braintreeSettings.publicKey,
    //     privateKey: braintreeSettings.privateKey,
    // }) : null;

    const mailchimpSettings = fullTenantConfig?.mailchimp;

    const mailchimp = mailchimpActive ? new CustomMailChimp({
        listId: mailchimpSettings.listId,
        apiKey: mailchimpSettings.apiToken,
        storeDomain: mailchimpSettings.storeUrl,
    }) : null;

    // Customers that use Zoho Books can have a more advanced booking
    // including Banking fees automatically transfered
    // const paypalTransactionsAccountId = fullTenantConfig?.zoho?.payPalAccountId;
    // const creditCardPaymentTransactionAccountId = fullTenantConfig?.zoho?.creditCardAccountId;

    // Saleor sends the name of the current event in the header
    if (!saleorEvent) {
        logger.error('No Saleor Event given!');
        return res.json({ Status: 'Success' });
    }

    if (!valid) {
        logger.error('Incoming request not valid! Returning 500');
        return res.status(500).end();
    }

    let { body } = req;
    assert(typeof (body) === 'object');

    if (saleorEvent === 'notify_user') {
        return res.json({
            Status: 'Success',
        });
    }
    if (!body[0]) {
        logger.info('No valid body found. Got this:', body);
        return res.json({
            Status: 'Success',
        });
    }

    body = body[0];

    // drafts in Saleor don't get created in Zoho
    if (body?.status === 'draft' || saleorEvent === 'fulfillment_created') {
        return res.json({
            Status: 'Success',
        });
    }

    logger.info(`incoming webhook with event ${saleorEvent}`);

    // we save the payments for an individual call later
    const { payments }: { payments: WebhookPayment[] } = body;
    delete body.payments;

    let user_id: string;
    let email: string;
    let shipping_address: SaleorWebhookAddress;
    let billing_address: SaleorWebhookAddress;
    let zohoShippingAddressId: string;
    let zohoBillingAddressId: string;
    let contact_person_id: string;

    let salesorder_id: string;
    let salesorder: SalesOrder;

    switch (saleorEvent) {
        case 'customer_created':

            // const customerTransformResponse = await addressPreTransform({ inputBody: body, languageCode: 'de', logger, zohoClient });
            // body = customerTransformResponse.body;
            // user_id = customerTransformResponse.user_id;
            // if (user_id) {
            //     logger.info(`The user account already exist. Skipping creation for ${body.contact_persons[0].email}`);
            // } else {
            //     logger.info('Creating customer account', body.contact_persons[0].email);
            //     await zohoClient.createContact(body);
            // }

            break;
        case 'order_created' || 'order_confirmed':

            const orderObject :SaleorWebhookOrder = body;
            // The language of a certain user. Right now, we just set German or English
            const userLang = orderObject?.metadata?.userLang === 'de' ? 'de' : 'en';
            if (!userLang) logger.error('We don\'t know the language of the user! Set it via Order Metadata "userLang"');

            const versandaktion = !!orderObject?.metadata?.versandaktion;
            if (versandaktion) logger.info('This order is a Rohrpost Service Order');

            // the SalesOrder Object that we use to finally send the create Event to Zoho
            const createSalesorderObject: SalesOrder = {};

            shipping_address = orderObject.shipping_address;
            billing_address = orderObject.billing_address;

            /**
             * we generate a human-readable and localized country name in English or German.
             */
            const localizedShippingAddressCountryName = countries.getName(shipping_address.country, userLang);

            /**
             * we generate a human-readable and localized country name in English or German.
             */
            const localizedBillingAddressCountryName = countries.getName(billing_address.country, userLang);

            // const transformResponse = await addressPreTransform({
            //     inputBody: body,
            //     languageCode: userLang,
            //     userId: rohrpostMainContactId,
            //     versandService: versandaktion,
            //     logger,
            //     zohoClient,
            // });

            // user_id = transformResponse.user_id;
            createSalesorderObject.customer_id = user_id;
            createSalesorderObject.contact_persons = body.contact_persons;
            // zohoBillingAddressId = transformResponse.zohoBillingAddressId;
            // zohoShippingAddressId = transformResponse.zohoShippingAddressId;
            // contact_person_id = transformResponse.contact_person_id;
            // const contactPerson = transformResponse.contact_person;
            email = body.user_email.toLowerCase();

            // logger.info(`Calling Saleor to get full order data for order id ${body.id}`);
            // // getting more Order Data from Saleor
            // const orderDetails = await saleorGraphQLClient.query<GetOrderQuery, GetOrderQueryVariables>({
            //     query: getOrderQuery,
            //     variables: {
            //         id: body.id,
            //     },
            // });

            // const salesorderAlreadyCreated = orderDetails.data?.order?.privateMetadata?.find((x) => x.key === 'ZohoInventory.salesorder_id')?.value;

            const firstName = orderObject.billing_address.first_name;
            const lastName = orderObject.billing_address.last_name;
            const userEmail = (orderObject?.user_email || orderObject?.email).toLowerCase();

            const createdTimeString = orderObject.created;
            if (!firstName) logger.error('We could not get first and last name. Mailchimp will not know this data!');
            createSalesorderObject.customer_id = user_id;
            const array = [];
            array.push(contact_person_id);
            // body.contact_persons = array;
            createSalesorderObject.contact_persons = array;

            /**
             * The base64 encoded ID that is actually the token
             */
            // const saleorOrderId = body.id;
            /**
             * The human readable number
             */
            const saleorOrderNumber = body.number;
            const { zohoPrefixOrderNumber, orderNumber } = salesOrderID(saleorOrderNumber);
            createSalesorderObject.salesorder_number = zohoPrefixOrderNumber;
            createSalesorderObject.reference_number = body.id.slice(0, 30);

            const webhookOrderLines = orderObject.lines;

            for (const line in webhookOrderLines) {
                if (Object.prototype.hasOwnProperty.call(webhookOrderLines, line)) {
                    // get the Zoho Item ID and Tax Rate by search for SKU.
                    // We take the Tax Rate from Zoho - this is our "truth"
                    // const { zohoItemId, zohoTaxRate } = await zohoClient.getItembySKU({
                    //     product_sku: body.lines[line].product_sku,
                    // });
                    // body.lines[line].tax_rate = zohoTaxRate;
                    // body.lines[line].item_id = zohoItemId;
                    body.lines[line].rate = body.lines[line].unit_price_gross_amount;
                    delete body.lines[line].type;
                    delete body.lines[line].id;
                    delete body.lines[line].allocations;
                    delete body.lines[line].product_name;
                } else {
                    logger.error(`Loop has unexpected properties ${webhookOrderLines}`);
                }
            }
            createSalesorderObject.line_items = body.lines;
            // body.line_items = body.lines;

            // Saleor is giving us gross prices.
            // body.is_inclusive_tax = true;
            createSalesorderObject.is_inclusive_tax = true;

            // add the delivery method using the name of the shipping zone in saleor
            // body.delivery_method = shippingMethod(body.shipping_method.name);
            createSalesorderObject.delivery_method = shippingMethod(body.shipping_method.name);

            // shipping costs now
            // body.shipping_charge_inclusive_of_tax = body.shipping_method.price_amount;
            // createSalesorderObject.shipping_charge_inclsive_of_tax = orderDetails.data.order.shippingPrice.gross.amount;

            // set the brutto price for shipping
            // body.shipping_charge = body.shipping_method.price_amount;
            createSalesorderObject.shipping_charge = parseFloat(orderObject.shipping_price_gross_amount);

            // const taxRate = calculateShippingTaxRate(body.lines, 'tax_rate');
            // const { taxes, customFieldReadyToFulfill } = await zohoClient.salesOrderEditpage();
            // const tax = taxes.filter((x) => x.tax_percentage === taxRate);
            // if (tax.length < 1) throw new Error(`The appropriate tax rate of ${taxRate}% is missing in Zoho! Please create it first`);

            // createSalesorderObject.shipping_charge_tax_id = tax[0].tax_id;
            // delete body.shipping_method;
            // delete body.lines;

            /**
             * Create a entity level discount, if we have one. Have to disable for now
             */
            // if (body.discount_amount) {
            //     const orderVoucher = orderDetails.data.order?.voucher;

            //     // If we have an entity level percentage discount, we set it also in Zoho.
            //     if (orderVoucher.discountValueType === 'PERCENTAGE' && orderVoucher.type === 'ENTIRE_ORDER') {
            //         const discountPercentage = orderVoucher.discountValue;
            //         createSalesorderObject.discount = `${discountPercentage}%`;
            //         createSalesorderObject.is_discount_before_tax = true;
            //     }
            //     if (orderVoucher.discountValueType === 'FIXED' && orderVoucher.type === 'ENTIRE_ORDER') {
            //         createSalesorderObject.discount = orderVoucher.discountValue;
            //         createSalesorderObject.is_discount_before_tax = false;
            //     }
            //     if (orderVoucher.discountValueType === 'PERCENTAGE' && orderVoucher.type === 'SPECIFIC_PRODUCT') {
            //         const subTotal = orderDetails.data.order.subtotal.gross.amount;
            //         const discountedSubtotal = orderDetails.data.order.total.gross.amount - orderDetails.data.order.shippingPrice.gross.amount;

            //         // eslint-disable-next-line no-mixed-operators
            //         const bruttoPercentage = (subTotal - discountedSubtotal) * 100 / subTotal;
            //         const roundedBruttoPercentage = round(bruttoPercentage, 2);
            //         createSalesorderObject.discount = `${roundedBruttoPercentage}%`;
            //         createSalesorderObject.is_discount_before_tax = true;
            //     }
            //     createSalesorderObject.discount_type = 'entity_level';
            // }

            // we only mark an order as vorkasse payment, if the right gateway is used and the gross amount is bigger than 0 (could be fully paid by giftcard)
            // if we have a versendeaktion, we do not want to mark the order as Vorkasse Order
            const isVorkassePayment = payments?.[0]?.gateway.includes('triebwork.payments.rechnung') && body.total_gross_amount > 0 && !versandaktion;

            // We set the custom field "ready to fulfill" here. This packages are marked as ready to be send out.
            // createSalesorderObject.custom_fields = [{
            //     customfield_id: customFieldReadyToFulfill,
            //     value: !isVorkassePayment,
            // }];

            // if (orderDetails.data.order?.voucher) {
            //     createSalesorderObject?.custom_fields?.push({
            //         api_name: 'cf_voucher_code',
            //         value: orderDetails.data.order.voucher.code,
            //     });
            // }

            // if (salesorderAlreadyCreated) {
            //     logger.info('We already have a salesorder_id in the private_meta of this order. Skipping creation.');
            //     salesorder_id = salesorderAlreadyCreated;
            // }
            if (!fullTenantConfig?.zoho?.active) logger.info('Zoho is not active. Skipping creation');
            // Create and confirm salesorder when Zoho is active
            // if (fullTenantConfig?.zoho?.active) {
            //     try {
            //         // we need to save billing and shipping address
            //         // but delete it now here, as Zoho just works with adress IDs ..
            //         createSalesorderObject.billing_address_id = zohoBillingAddressId;
            //         // body.shipping_address_id = zohoShippingAddressId;
            //         createSalesorderObject.shipping_address_id = zohoShippingAddressId;

            //         // delete body.shipping_address;
            //         // delete body.billing_address;
            //         // const ZohoSalesorder = await zohoClient.createSalesorder(createSalesorderObject, body.total_gross_amount);
            //         // salesorder_id = ZohoSalesorder.salesorder_id;
            //         // salesorder = ZohoSalesorder;
            //         // confirm the order, when finished. Otherwise it will stay as draft
            //         // await zohoClient.salesorderConfirm(salesorder_id);

            //         // add the salesorder ID as order metadata
            //         // await addPrivateMetaToSaleorEntity(orderDetails.data.order.id, 'ZohoInventory.salesorder_id', salesorder_id, saleorGraphQLClient);
            //     } catch (error) {
            //         if (error?.response?.data?.code === 36004) {
            //             logger.info('We already created this salesorder. Proceeding');
            //             const result = await zohoClient.getSalesorder(body.salesorder_number);
            //             salesorder_id = result.salesorder_id;
            //             salesorder = result;
            //         } else if (error?.response?.data?.code === 36414) {
            //             logger.error('You have exhausted your available sales orders for this month in Zoho. Kindly upgrade your account.');
            //             return res.status(500).json({
            //                 Status: 'Zoho Limit reached',
            //             });
            //         } else {
            //             throw new Error(`Error creating Salesorder ${error} ${JSON.stringify(error?.response?.data)} we sent this body: \
            //             ${JSON.stringify(createSalesorderObject)}`);
            //         }
            //     }
            // }

            // Vorkasse Payment gets send out via email. Zoho tracks, if this salesorder got send emailed before.
            // if (isVorkassePayment && payments?.[0]?.charge_status !== 'fully-charged' && !salesorder?.is_emailed) {
            //     logger.info('We got a payment with Rechnungs Gateway. Send out Zahlungsdaten');
            //     // send out salesorder with vorkasse Email Template and create and invoice draft
            //     try {
            //         await zohoClient.sendEmailWithTemplate('salesorders', salesorder_id, 'Vorkasse', email, userLang);
            //         await zohoClient.invoicefromSalesorder(salesorder_id);
            //     } catch (error) {
            //         logger.error(error);
            //         Sentry.captureException(error);
            //     }
            // }

            /**
             * When Mailchimp is enabled, this functionality adds the store, products and orders automatically.
             */
            if (mailchimpActive && mailchimp) {
                const mailchimpOrderCheck = await mailchimp.GetOrder(orderNumber);
                if (mailchimpOrderCheck) {
                    logger.info('This mailchimp order already exists. Skipping any creation...');
                    return res.status(200).end();
                }

                const Lines = orderObject.lines.map((item) => {
                    const returnValue = {
                        id: `${orderObject.token}${item.product_variant_id}`,
                        product_id: item.product_variant_id,
                        product_variant_id: item.product_variant_id,
                        quantity: item.quantity,
                        price: parseFloat(item.unit_price_gross_amount),
                    };

                    return returnValue;
                });

                // when we have a versendeaktion, we can't use the main user Id as mailchimp customer id
                // instead, we use the contact_person_id
                const mailchimpEcommerceUserId = versandaktion ? contact_person_id : user_id;

                const mailchimpCustomer = await mailchimp.GetCustomer(mailchimpEcommerceUserId);

                /**
                 * Set the opt-in status according to metadata in the order! Otherwise they are always just transactional.
                 */
                const newsletterAccepted = body?.metadata?.newsletterAccepted === 'true';

                const orderToken = orderObject.token;
                const domain = process.env.MAIN_DOMAIN;
                let mailchimpOrderUrl = 'https://pfefferundfrost.de';
                if (orderToken && domain) {
                    const generatedUrl = generateMailchimpOrderUrl(domain, userLang, orderToken);
                    if (generatedUrl) {
                        mailchimpOrderUrl = generatedUrl;
                    } else {
                        Sentry.captureException(new Error(`Generated a mailchimp order url, that is not valid! ${domain} ${userLang} ${orderToken}`));
                    }
                } else {
                    logger.error(`Can't generate a valid order URL. Something is missing. Main Domain: ${domain}, orderToken: ${orderToken}`);
                }

                const customerObject: ECommerceCustomer = {
                    id: mailchimpEcommerceUserId,
                };
                if (!mailchimpCustomer) {
                    customerObject.id = userEmail;
                    customerObject.email_address = userEmail;
                    customerObject.first_name = firstName;
                    customerObject.last_name = lastName;
                    customerObject.opt_in_status = newsletterAccepted;
                    customerObject.company = body.company_name;
                } else {
                    logger.info('This user already exist in Mailchimp. We do not need to create it.');
                }

                // Mailchimp date Formating. Adding Timezone correctly
                const createdTime = dayjs(createdTimeString).format();
                // const orderFromSaleor = orderDetails.data.order;
                const discountTotal = (parseFloat(orderObject.undiscounted_total_gross_amount) - parseFloat(orderObject.total_gross_amount)) || 0;

                const mailchimpOrder: MailchimpOrder = {
                    id: orderNumber,
                    customer: customerObject,
                    currency_code: 'EUR',
                    order_total: parseFloat(orderObject.total_gross_amount),
                    shipping_total: parseFloat(orderObject.shipping_price_gross_amount),
                    tax_total: 0,
                    processed_at_foreign: createdTime,
                    lines: Lines,
                    shipping_address: {
                        name: `${shipping_address.first_name} ${shipping_address.last_name}`,
                        address1: shipping_address.street_address_1,
                        address2: shipping_address.street_address_2,
                        city: shipping_address.city,
                        postal_code: shipping_address.postal_code,
                        country_code: shipping_address.country,
                        country: localizedShippingAddressCountryName,
                        company: shipping_address.company_name,
                    },
                    billing_address: {
                        name: `${billing_address.first_name} ${billing_address.last_name}`,
                        address1: billing_address.street_address_1,
                        address2: billing_address.street_address_2,
                        city: billing_address.city,
                        postal_code: billing_address.postal_code,
                        country_code: billing_address.country,
                        country: localizedBillingAddressCountryName,
                        company: billing_address.company_name,
                    },
                    discount_total: discountTotal,
                    promos: [],
                    landing_site: mailchimpSettings.storeUrl,
                    financial_status: 'pending',
                    fulfillment_status: 'pending',
                    order_url: mailchimpOrderUrl,

                };
                // if (body.discount_name) {
                //     mailchimpOrder.promos.push({ code: body.discount_name, amount_discounted: createSalesorderObject.discount_total, type: 'fixed' });
                // }
                logger.info(`Trying to create the order in Mailchimp now.. ${mailchimpOrder.id}.`);
                try {
                    const CreateResult = await mailchimp.CreateOrder(mailchimpOrder);
                    if (CreateResult) logger.info(`Order sync with Mailchimp successful. ID: ${CreateResult.id}`);
                    const chimpId = mailchimp.getMailchimpIdFromEmail(email);
                    logger.info(`setting the user language now in Mailchimp for user ${chimpId}, email ${email.toLowerCase()}`);
                    const updateObject = { language: userLang, merge_fields: {} };
                    if (firstName && lastName) {
                        updateObject.merge_fields = {
                            FNAME: firstName,
                            LNAME: lastName,
                        };
                    } else {
                        delete updateObject.merge_fields;
                    }
                    await mailchimp.UpdateUser(chimpId, updateObject);
                } catch (error) {
                    logger.error(`Error creating order in mailchimp ${error} - trying to create this order: ${JSON.stringify(mailchimpOrder)}`);
                }
            }

            break;

        case 'checkout_update':
            logger.info(body);
            break;
        case 'order_fully_paid':
            await zohoClient.authenticate();
            const ids = salesOrderID(body.number);

            // When Zoho is not active, we just return 200
            // if (!fullTenantConfig?.zoho?.active) {
            //     logger.info('Zoho Integration is not active! Returning 200 Ok');
            //     return res.status(200).end();
            // }

            // const result = await zohoClient.getSalesorder(ids.zohoPrefixOrderNumber);
            // The order creation is a different API call from Saleor, that might come even after the order fully paid call.
            // we do not want to create the order, when it is not there yet, as this call might interfere with the order creation
            // call from Saleor
            // if (!result) {
            //     logger.info(`We received a payment for the non-existing salesorder ${ids.zohoPrefixOrderNumber}. Returning 500 and letting Saleor Retry...`);
            //     return res.status(500).end('Salesorder does not (yet) exist');
            // }

            // getting more Order Data from Saleor
            // const getOrder = await saleorGraphQLClient.query<GetOrderFullyPaidQuery, GetOrderFullyPaidQueryVariables>({
            //     query: getOrderFullyPaid,
            //     variables: {
            //         id: body.id,
            //     },
            // });
            // const orderFullyPaidDetails = getOrder.data.order;

            if (mailchimpActive) {
                // Make sure that the order is paid in Mailchimp as well
                logger.info('Setting the order as paid in Mailchimp');
                try {
                    const mailchimpOrder = {
                        financial_status: 'paid',
                    };
                    await mailchimp.UpdateOrder(ids.orderNumber, mailchimpOrder);
                } catch (error) {
                    logger.error('Error updating order in mailchimp:', error);
                    return res.status(500).end();
                }
            }
            // salesorder_id = result.salesorder_id;
            // create the payment
            // if (payments !== null) {
            //     let invoice_id :string;
            //     let invoice_metadata = { status: '' };

            //     // check if we already created an invoice before.
            //     if (result.invoiced_status !== 'invoiced') {
            //         logger.info('We have to create an Invoice in Zoho now..');
            //         try {
            //             invoice_id = await zohoClient.invoicefromSalesorder(salesorder_id);
            //         } catch (error) {
            //             logger.error('Error creating invoice', error);
            //             logger.error(error?.data);
            //             Sentry.captureException(`${error} ${error?.data}`);
            //         }
            //         invoice_metadata.status = 'unpaid';
            //     } else {
            //         logger.info('Invoice does already exist. Getting Invoice ID now ..');
            //         const invoiceResult = await zohoClient.getInvoicefromSalesorder(salesorder_id);
            //         invoice_id = invoiceResult.invoice_id;
            //         invoice_metadata = invoiceResult.metadata;
            //     }

            //     assert(typeof invoice_id === 'string');

            //     const payment = payments[0];
            //     const zohoPayment :CustomerPayment = {};

            //     // create the payment automatically only when we really charged the customer.
            //     if (invoice_metadata.status !== 'paid' && payment.charge_status === 'fully-charged') {
            //         // we take the customer id from the corresponding salesorder
            //         const { customer_id } = result;

            //         zohoPayment.payment_mode = payment.gateway === 'mirumee.payments.braintree' ? 'onlinepayment' : 'banktransfer';
            //         zohoPayment.customer_id = customer_id;
            //         zohoPayment.amount = payment.captured_amount;
            //         zohoPayment.date = dayjs(payment.created).format('YYYY-MM-DD');
            //         delete payment.created;
            //         zohoPayment.invoices = [{
            //             invoice_id,
            //             amount_applied: payment.captured_amount,
            //         }];
            //         logger.info(`We set the "Zahlungs-ID" custom field and create an Payment in Zoho now and add it to invoice ${invoice_id}`);
            //         try {
            //             const {
            //                 invoiceUpdateObject,
            //                 zohoPaymentAccountId,
            //                 zohoPaymentBankCharges,
            //                 zohoPaymentCustomFields } = await getPaymentGatewayMetadata(
            //                 payment.id, logger,
            //                 braintree,
            //                 saleorGraphQLClient,
            //                 paypalTransactionsAccountId, creditCardPaymentTransactionAccountId, braintreeSettings?.active,
            //             );
            //             zohoPayment.account_id = zohoPaymentAccountId;
            //             zohoPayment.bank_charges = zohoPaymentBankCharges;
            //             zohoPayment.custom_fields = zohoPaymentCustomFields;

            //             // Adding the voucher code if available to the invoice as custom field
            //             if (orderFullyPaidDetails?.voucher?.code) {
            //                 invoiceUpdateObject.custom_fields.push({
            //                     api_name: 'cf_voucher_code',
            //                     value: orderFullyPaidDetails?.voucher?.code,
            //                 });
            //             }
            //             await zohoClient.updateInvoice(invoice_id, invoiceUpdateObject);
            //         } catch (error) {
            //             logger.error(`Error getting gateway metadata to update Invoice and Payment data: ${error}`);
            //         }

            //         try {
            //             await zohoClient.createPayment(zohoPayment);
            //         } catch (error) {
            //             if (error?.response?.data?.code === 24016) {
            //                 logger.info('Payment can not be added. Is most likely already created.');
            //             } else {
            //                 const exception = error?.response?.data || error;
            //                 logger.error(exception);
            //                 Sentry.captureException(exception);
            //             }
            //         }
            //     }
            // }
            break;

        case ('product_updated' || 'product_created'):
            if (!mailchimpActive) {
                logger.info('Mailchimp not activated. No Product syncing needed. Skipping');
                return res.status(200).end();
            }

            const productDetails = await unauthenticatedSaleorGraphQLClient.query<GetProductQueryQuery, GetProductQueryQueryVariables>({
                query: getProduct,
                variables: {
                    id: body.id,
                    channel: 'storefront',
                },
            });
            logger.info(`Finished getting all product details from Saleor for ${body.name}`);
            if (!productDetails.data?.product) {
                logger.error(`no product returned from Saleor! ${JSON.stringify(productDetails.data)} ${body.id} - Can't proceed with this product. This may be intended`);
                await apmFlush();
                return res.status(200).json({ status: 'success!' });
            }
            const { product } = productDetails.data;
            if (product!.variants!.length < 1) throw new Error(`Getting no variants back from saleor! Skipping ${JSON.stringify(product)}`);
            const returnParallel = product?.variants?.map(async (variant) => {
                // const { zohoItemId } = await getItembySKU({ product_sku: variant?.sku });
                const productUrl = mailchimpSettings?.productUrlPattern?.replace(/{slug}/, product.slug);
                const variantImage = variant!.images!.length > 0 ? variant?.images?.[0]?.url : product.images?.[0]?.url;
                const title = variant.name ? `${product.name} - ${variant.name}` : product.name;
                const price = variant?.pricing?.price?.gross.amount;
                const sku = variant?.sku;

                // for product recommendations, Mailchimp needs the inventory qunatity.
                const inventory_quantity = variant.quantityAvailable;

                const variants = [{
                    id: variant.id,
                    image_url: variantImage,
                    title,
                    price,
                    sku,
                    inventory_quantity,
                }];
                const brand = product.attributes.find((attr) => attr.attribute.name === 'brand')?.values[0]?.name;
                if (!brand) logger.warn(`Did not receive any brand values in attributes: ${JSON.stringify(product.attributes)}`);
                const mailchimpProduct = {
                    id: variant.id,
                    title,
                    vendor: brand || '',
                    variants,
                    image_url: variantImage,
                    url: productUrl,
                    published_at_foreign: product.updatedAt,
                };
                const shopResult = await mailchimp.GetProduct(variant.id);
                if (shopResult) {
                    logger.info(`Updating Product ${title} in Mailchimp..`);
                    await mailchimp.UpdateProduct(variant.id, mailchimpProduct);
                } else {
                    logger.info(`Creating product ${title} in Mailchimp..`);
                    await mailchimp.CreateProduct(mailchimpProduct).catch((e) => console.error(e));
                }
                return true;
            });
            try {
                await Promise.all([returnParallel]);
            } catch (error) {
                logger.error('Error getting product variant in Zoho');
                Sentry.captureException(error);
            }

            break;
        default:
            break;
    }

    await apmFlush();

    return res.status(200).json({ status: 'success!' });
});

function shippingMethod(name: string) {
    if (name.includes('DPD')) return 'DPD Germany';
    if (name.includes('DHL')) return 'DHL';
    console.error('No shipping method in Zoho found that matches the shipping method in Saleor :( we got', name);
    return '';
}

/**
 * This function formats a saleor order number valid Zoho Inventory ID like
 *  PREFIX-ORDERNUMBER
 * @param {string} saleorID
 */
const salesOrderID = (saleorID: string) => ({ zohoPrefixOrderNumber: `STORE-${saleorID}`, orderNumber: saleorID.toString() });

/**
 * Creates an order URL for customers to get back to their order
 */
const generateMailchimpOrderUrl = (domain: String, language: String, token: string) => {
    const url = `https://${domain}/${language}/checkout/confirmation?orderToken=${token}`;

    return isUrl(url) ? url : false;
};

/**
 * Transforms the body and customer objects to match Zoho and gives you shipping and billing address IDs form Zoho.
 * Creates a user, if it does not exist yet.
 * @param inputBody
 * @param languageCode the language code that will be set when user is created. This will change the language of all E-Mails, that the user receives
 * @param userId Optional: if you set the Zoho User Id already here, we do not need to create the user, but instead use an existing one and just add addresses for example.
 * @param versandService Optional: if set to true, the returning contact person will be from the shipping address
 */
const addressPreTransform = async ({ inputBody, languageCode, userId, versandService, logger, zohoClient }
: { inputBody, languageCode: string, userId?: string, versandService?: boolean, logger: winston.Logger, zohoClient: ZohoClientInstance }) => {
    const body = user_transformer(inputBody, versandService, logger);
    let contact_person_id: string = '';
    let zohoBillingAddressId: string = '';
    let zohoShippingAddressId: string = '';
    // search, if the user already exist. When a user for example adds a company name on second order, we will create a new user.
    const searchbody = { ...body.contact_persons[0], company_name: body.company_name };
    logger.info('Looking up user in Zoho ...');
    let user_id: string = userId || await zohoClient.getContact(searchbody);

    // we transformed the user body, so that we just have a contact_person_arry. Here
    // we reconstruct the data for easy access.
    const contactPersonData: ContactPerson = {
        first_name: body.contact_persons[0].first_name,
        last_name: body.contact_persons[0].last_name,
        email: body.contact_persons[0].email,
    };
    if (user_id) {
        logger.info(`Customer ${body.contact_persons[0].first_name} ${body.contact_persons[0].last_name} with user ID ${user_id} already exist in Zoho.`);
        const contact_details = await zohoClient.getContactWithFullAdresses(user_id);
        // contact_person_id = contact_details.contact_persons[0].contact_person_id;
        contact_person_id = await contactPersonId(contact_details,
            { firstName: contactPersonData.first_name, lastName: contactPersonData.last_name, email: contactPersonData.email }, logger, zohoClient);

        body.contact_persons[0].contact_person_id = contact_person_id;
        const { billingAddressId, shippingAddressId } = await billingAndShippingAddressId(user_id,
            contact_details.addresses,
            body.shipping_address, body.billing_address, logger, zohoClient);
        zohoBillingAddressId = billingAddressId;
        zohoShippingAddressId = shippingAddressId;
    }
    if (!user_id) {
        logger.info(`The customer ${JSON.stringify(body.contact_persons[0])}, company: ${body.company_name} does not exist in Zoho. We need to create it now`);
        const customer_sub_type = body.company_name ? 'business' : 'individual';
        const new_user = {
            contact_name: body.company_name || body.contact_name,
            contact_persons: body.contact_persons,
            language_code: languageCode,
            shipping_address: body.shipping_address,
            billing_address: body.billing_address,
            company_name: body.company_name,
            customer_sub_type,
        };
        const result = await zohoClient.createContact(new_user).catch((e) => {
            logger.error(e);
        });
        if (!result) throw new Error('Contact creation was not succesfull');
        user_id = result.contact_id;
        contact_person_id = result.contact_person_id;
        zohoBillingAddressId = result.billing_address_id;
        zohoShippingAddressId = result.shipping_address_id;
    }

    if (!contact_person_id) {
        throw new Error(`Out of some reason, we did not get any contact person Id. Can't proceed. User Id: ${user_id}`);
    }
    contactPersonData.contact_person_id = contact_person_id;
    return { body, user_id, contact_person_id, contact_person: contactPersonData, zohoBillingAddressId, zohoShippingAddressId };
};

/**
 * Returns a valid shipping and billing address Id. Creates a suitable address, if no address could be found.
 */
const billingAndShippingAddressId = async (zohoUserId: string,
    contactDetailAddresses: Address[],
    shippingAddress: Address, billingAddress: Address, logger: winston.Logger, zohoClient: ZohoClientInstance) => {
    let shippingAddressId: string;
    let billingAddressId: string;
    const findValidBillingAddress: Address = find(contactDetailAddresses, billingAddress);
    if (findValidBillingAddress) {
        logger.info(`This contact has a suitable billing address with id ${findValidBillingAddress.address_id}`);
        billingAddressId = findValidBillingAddress.address_id;
    } else {
        logger.info('We have to create this billing address');
        const res = await zohoClient.addAddresstoContact(zohoUserId, billingAddress);
        billingAddressId = res;
    }
    const findValidShippingAddress: Address = find(contactDetailAddresses, shippingAddress);
    if (findValidShippingAddress) {
        logger.info(`This contact has a suitable shipping address with id ${findValidShippingAddress.address_id}`);
        shippingAddressId = findValidShippingAddress.address_id;
    } else {
        logger.info('We have to create this shipping address');
        const res = await zohoClient.addAddresstoContact(zohoUserId, shippingAddress);
        shippingAddressId = res;
    }
    return { shippingAddressId, billingAddressId };
};

/**
 * Looks for a suitable contact person of a contact. Creates the contact person, if not existing yet.
 * Returns the contact person Id
 * @param contactDetails the contact Details from a Zoho "getContact" API Call
 * @param {} {}
 */
const contactPersonId = async (contactDetails: Contact | ContactWithFullAddresses,
    { firstName, lastName, email }: { firstName: string, lastName: string, email: string }, logger: winston.Logger, zohoClient: ZohoClientInstance) => {
    // a contact person with same email address can't exist twice
    const existingContactPerson = find(contactDetails.contact_persons, { first_name: firstName, last_name: lastName, email })
        || find(contactDetails.contact_persons, { email });
    if (existingContactPerson) {
        return existingContactPerson.contact_person_id;
    }
    logger.info(`Contact Person ${firstName} ${lastName} has to be created and added to the Zoho contact ${contactDetails.contact_id}`);
    const newContactPersonId = await zohoClient.createContactPerson(contactDetails.contact_id, { first_name: firstName, last_name: lastName, email });
    return newContactPersonId;
};
