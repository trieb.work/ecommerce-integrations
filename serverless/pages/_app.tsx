/* eslint-disable react/jsx-props-no-spreading */
import type { AppProps } from 'next/app';
import CssBaseline from '@material-ui/core/CssBaseline';
import { StylesProvider } from '@material-ui/styles';
import { ThemeProvider as StyledComponentProvider } from 'styled-components';
import { ThemeProvider as MaterialUiProvider } from '@material-ui/core/styles';
import { ExtensionMessageEvent, ExtensionMessageType, ThemeChangeMessage, useExtensionMessage } from '@saleor/macaw-ui/extensions';
import light from 'themes/light';
import dark from 'themes/dark';
import { useState } from 'react';
import Head from 'next/head';

function MyApp({ Component, pageProps }: AppProps) {
    const [isDark, setDark] = useState(false);
    const handleThemeChange = (
        event: ExtensionMessageEvent<ThemeChangeMessage>,
    ) => {
        if (event.data.type === ExtensionMessageType.THEME) {
            setDark(event.data.theme === 'dark');
        }
    };
    useExtensionMessage(handleThemeChange);

    return (
        <>

            <Head>
                <title>eci-settings</title>
                <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
            </Head>
            <StyledComponentProvider theme={isDark ? dark : light}>
                <MaterialUiProvider theme={isDark ? dark : light}>
                    <StylesProvider injectFirst>
                        <CssBaseline />
                        <Component {...pageProps} />
                    </StylesProvider>
                </MaterialUiProvider>
            </StyledComponentProvider>
        </>
    );
}

export default MyApp;
