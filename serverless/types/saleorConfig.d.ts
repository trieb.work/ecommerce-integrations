export type ConfigJson = {
    saleor: {
        domain: string,
        appId: string,
    },
    zoho: {
        active: boolean,
        orgId: string,
        clientId: string,
        clientSecret: string,
    },
};
