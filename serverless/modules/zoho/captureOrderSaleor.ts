import { Invoice } from '@trieb.work/zoho-inventory-ts';
import { GetOrderQuery, GetOrderQueryVariables, OrderCaptureMutation, OrderCaptureMutationVariables } from 'gqlTypes/globalTypes';
import { getOrderQuery, orderCaptureMutation } from 'lib/gql/saleor.gql';
import { getGqlOrderId } from '@trieb.work/helpers-ts';
import winston from 'winston';
import { ApolloClient, NormalizedCacheObject } from '@trieb.work/apollo-client';

const captureOrderSaleor = async (invoice :Invoice, logger :winston.Logger, client :ApolloClient<NormalizedCacheObject>) => {
    const referenceNumber = invoice?.reference_number;
    if (!referenceNumber?.match(/STORE-\d+/)) {
        logger.info(`Invoice reference number is not existing or not matching our pattern. We don't capture this payment in Saleor ${referenceNumber}`);
        return true;
    }
    const orderNumber = referenceNumber.split('-')[1];
    const saleorOrderId = getGqlOrderId(orderNumber);
    const saleorOrder = await client.query<GetOrderQuery, GetOrderQueryVariables>({
        query: getOrderQuery,
        variables: {
            id: saleorOrderId,
        },
    });
    if (!saleorOrder.data?.order?.id) return true;
    if (saleorOrder.data.order.paymentStatus !== 'NOT_CHARGED') {
        logger.info(`Order ${saleorOrderId} has status ${saleorOrder.data.order.paymentStatus} in Saleor - won't capture payment in Saleor`);
        return true;
    }
    const { order } = saleorOrder.data;
    if (invoice.total !== order.total.gross.amount) {
        logger.info('Can not automatically capture payment. Invoice total and order total diverge. This is most likely an error');
        return true;
    }
    await client.mutate<OrderCaptureMutation, OrderCaptureMutationVariables>({
        mutation: orderCaptureMutation,
        variables: {
            id: saleorOrderId,
            amount: invoice.total,
        },

    });
    return true;
};
export default captureOrderSaleor;
