import { Invoice, ZohoClientInstance } from '@trieb.work/zoho-inventory-ts';
import winston from 'winston';

/**
 * Setting an order as paid in Mailchimp. Generating Invoice Download Link and send out invoice
 * @param invoice The Zoho Invoice Object
 */
export const sendPaidInvoice = async (invoice :Invoice, zohoClient :ZohoClientInstance, logger :winston.Logger) => {
    const email = invoice?.email;
    if (!email) {
        logger.error('We got an invoice without contact person assotiated. We can\'t send this invoice out automatically!', { 'zoho-invoice-id': invoice.invoice_id });
        return true;
    }
    if (invoice.total <= 0) {
        logger.info('This is a zero invoice. We most likely dont want to send it out', { 'zoho-invoice-id': invoice.invoice_id });
        return true;
    }
    if (invoice?.is_emailed) {
        logger.info(`Invoice ${invoice.invoice_number} is already emailed. Skpping`);
        return true;
    }
    const user = await zohoClient.getContactById(invoice.customer_id);
    const userLang = user.language_code;
    const emailResponse = await zohoClient.sendEmailWithTemplate('invoices', invoice.invoice_id, 'Paid_Invoice', email, userLang);
    if (!emailResponse) {
        logger.error('Invoice emailing not succesful!');
    } else {
        logger.info('Sending Invoice via Email successful');
    }

    return true;
};
