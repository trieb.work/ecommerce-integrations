/* eslint-disable no-continue */
/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
/* eslint-disable @typescript-eslint/no-loop-func */
import { Package, SalesOrder, ZohoClientInstance } from '@trieb.work/zoho-inventory-ts';
import * as Sentry from '@sentry/node';

import {
    fulfillmentMutation,
    getOrderQuery,
    updateTrackingMutation,
} from 'lib/gql/saleor.gql';
import { getSaleorWarehouses, returnMailchimpOrderId } from 'modules/zoho/common';
import assert from 'assert';
import {
    CreateFulfillmentMutation,
    CreateFulfillmentMutationVariables,
    GetOrderQuery, GetOrderQueryVariables,
    OrderFulfillLineInput, SetTrackingNumberMutation,
    SetTrackingNumberMutationVariables } from 'gqlTypes/globalTypes';
import winston from 'winston';
import { ApolloClient, NormalizedCacheObject } from '@trieb.work/apollo-client';
import CustomMailChimp from '@trieb.work/mailchimp-ts';
import generateTrackingPortalURL, { Carrier, CountryCode } from '@trieb.work/carrier-tracking-portals';
import { PackageTrackerInstance } from '@trieb.work/internal-package-trackers';
import { lowerCase } from 'lodash';
// import { printCarrierLabel, printCustomsDocuments, printPackageSlip } from './printingHelpers';

const mailchimpActive = process.env.ENABLE_MAILCHIMP === 'true';

/**
 * Create a fulfillment with tracking number in Saleor.
 * @param param0
 */
const fulfillOrderInSaleor = async ({ SaleorOrder, packagedata, reference_number }
:{ packagedata :Package, reference_number :string, SaleorOrder :GetOrderQuery['order']},
logger :winston.Logger, client :ApolloClient<NormalizedCacheObject>) => {
    if (SaleorOrder.fulfillments.find((full) => full.trackingNumber.includes(packagedata.tracking_number))) {
        logger.info('Tracking number already found in this order. Skipping fulfillment create. Tracking number:', packagedata.tracking_number);
        return null;
    }

    const SaleorWarehouses = await getSaleorWarehouses(client);

    // go through all line items of the Zoho Package and change the data to map to Saleor Format
    // In Zoho it can happen, that you have two orderlines with the same product - in saleor this
    // can't happen
    const lines :OrderFulfillLineInput[] = [];
    for (const lineItem of packagedata.line_items) {
        // get the orderlineID with the SKU
        const orderLineId = SaleorOrder.lines.find((x) => x.productSku === lineItem.sku)?.id;
        if (!orderLineId) {
            logger.info(`Couldn't find this SKU in any orderline in Saleor ${lineItem.sku}. \
            This orderline could be added manually in Zoho. Order Number: ${packagedata.salesorder_number}`);
            continue;
        }

        const temp = SaleorWarehouses.find((x) => x.node.name === lineItem.warehouse_name);
        if (!temp) {
            logger.info('We couldn\'t find a matching Warehouse in Saleor for Zoho Warehouse', lineItem.warehouse_name);
            continue;
        }
        const stocks = [{
            warehouse: temp.node.id,
            quantity: lineItem.quantity,
        }];

        lines.push({
            orderLineId,
            stocks,
        });
    }
    assert(typeof reference_number === 'string');
    const variables = {
        orderId: reference_number,
        input: { lines },
    };
    const result = await client.mutate<CreateFulfillmentMutation, CreateFulfillmentMutationVariables>({
        mutation: fulfillmentMutation,
        variables,
    });
    if (result.data.orderFulfill.orderErrors.length > 0) {
        logger.info('Error-Result from Saleor', JSON.stringify(result.data.orderFulfill, null, 2), JSON.stringify(variables, null, 2));
        throw new Error(`Error creating fulfillment in Saleor '+ ${JSON.stringify(result.data.orderFulfill.orderErrors)} ${JSON.stringify(variables, null, 2)}`);
    } else {
        logger.info('Created Fulfillment in Saleor successful');
    }

    const fullfillId = result.data.orderFulfill.fulfillments[0].id;
    assert(typeof fullfillId === 'string');
    logger.info('Adding tracking number to fulfillment', fullfillId);

    // add the tracking data after creating the fulfillment
    const trackingResult = await client.mutate<SetTrackingNumberMutation, SetTrackingNumberMutationVariables>({
        mutation: updateTrackingMutation,
        variables: {
            id: fullfillId,
            trackingNumber: packagedata.tracking_number,
        },
    });
    if (trackingResult.data.orderFulfillmentUpdateTracking.orderErrors.length < 0) throw new Error('Adding tracking number not successful!');
    logger.info('Adding Fullfillment to Saleor Order successfully');

    return true;
};

/**
 * Logic that works with an Zoho packages array.
 * @param rawPackages
 * @param salesorder the full salesorder used to get some more needed metadata.
 */
export const packagesLogic = async (pkg :Package,
    salesorder :SalesOrder, logger:winston.Logger, client :ApolloClient<NormalizedCacheObject>, mailchimp :CustomMailChimp,
    zohoClient :ZohoClientInstance, packageTrackerClient :PackageTrackerInstance) => {
    const zohoSalesOrderNumber = salesorder.salesorder_number;
    const MailchimpOrderId = returnMailchimpOrderId(zohoSalesOrderNumber, logger);

    // if (pkg.status === 'not_shipped') {
    //     logger.info('We have newly created packages in Zoho, that are not shipped yet. Checking, if we need to print stuff...');
    //     const packagedata = await zohoClient.getPackage({
    //         package_id: pkg.package_id,
    //     });
    //         // check in the custom field "label_printed"
    //     const labelPrintedAlready = packagedata?.custom_field_hash.cf_label_printed_unformatted;
    //     if (labelPrintedAlready) {
    //         logger.info('Label got already printed');
    //         return true;
    //     }

    //     const printResult = await printPackageSlip(pkg, zohoClient, logger).catch((e) => {
    //         logger.error(e);
    //         Sentry.captureException(e);
    //     });
    //         // set the custom field "label_printed" to true. Shipped packages can't be updated right now, so this is disabled.
    //     if (printResult) {
    //         // const customFieldId = packagedata.custom_fields.find((field) => field.label === 'label_printed')?.customfield_id;
    //         // await setCheckboxValue('packages', pkg.package_id, customFieldId, true);
    //     }
    // }

    if (pkg.status === ('shipped' || 'delivered')) {
        logger.info('Received a salesorder with newly shipped package(s)');

        // eslint-disable-next-line @typescript-eslint/naming-convention
        const { reference_number } = salesorder;
        // pull more data about this package from zoho to know all the line items and comments in this package
        const packagedata = await zohoClient.getPackage({
            package_id: pkg.package_id,
        });

        logger.info(`Working now on package with Zoho ID ${pkg.package_id}`);

        const trackingNumber = packagedata.tracking_number || packagedata?.shipment_order?.tracking_number;
        if (!trackingNumber) {
            return true;
        }

        let { carrier } = packagedata;

        // eslint-disable-next-line no-nested-ternary
        const easypostCarrier = lowerCase(carrier).includes('dpd') ? 'DPD' : lowerCase(carrier).includes('dhl') ? 'DHLParcel' : 'DPD';

        // add the easypost tracker
        try {
            logger.info('Adding an easypost tracker now.', { 'zoho-package-id': pkg.package_id, 'easypost-tracking-code': packagedata.tracking_number });
            await packageTrackerClient.addTracker(packagedata.tracking_number, easypostCarrier);
        } catch (error) {
            logger.error(error);
            Sentry.captureException(error);
        }

        let trackingPortalUrl = '';
        // add the tracking Portal URL back to the salesorder
        try {
            // little cleanup for DPD or DPD Germany etc.
            if (carrier.includes('DPD')) carrier = 'DPD';
            const { country } = packagedata.shipping_address;
            const countryCode = {
                Deutschland: 'de' as CountryCode,
                Österreich: 'at' as CountryCode,
                Schweiz: 'ch' as CountryCode,
                Spanien: 'es' as CountryCode,
                Frankreich: 'fr' as CountryCode,
                'Vereinigtes Königreich': 'uk' as CountryCode,
                Belgien: 'be' as CountryCode,
                Niederlande: 'nl' as CountryCode,
            };
            const url = generateTrackingPortalURL(carrier as Carrier, countryCode[country], packagedata.tracking_number, packagedata.shipping_address.zip);

            if (url && !packagedata?.shipment_order?.notes) {
                await zohoClient.addNote(packagedata.shipment_id, url);
                trackingPortalUrl = url;
            }
        } catch (error) {
            logger.error(error);
            Sentry.captureException(error);
        }

        /**
         * Mailchimp is activated - fulfill the order there
         */
        if (mailchimpActive && packagedata?.tracking_number && packagedata?.status === 'shipped') {
            const contact = await zohoClient.getContactById(salesorder.customer_id);
            const { email } = contact;
            if (email) {
                try {
                    // const carrier = packagedata.carrier || salesorder.delivery_method;
                    await mailchimp.fulfillOrderInMailChimp(email, MailchimpOrderId,
                        { tracking_number: packagedata.tracking_number, carrier, tracking_portal_url: trackingPortalUrl });
                    logger.info('Marked this order as fulfilled in MailChimp');
                } catch (error) {
                    if (error.statusCode === 404) {
                        logger.info(`This order does not exist in Mailchimp. Can't fulfill. Order Number: ${MailchimpOrderId}`);
                    } else {
                        logger.error('Error fulfilling order in Mailchimp!', error);
                        Sentry.captureException(new Error(error));
                    }
                }
            } else {
                logger.info(`This Salesorder has no contact person email included. Can not update Mailchimp. Customer_ID: ${salesorder.customer_id}`);
            }
        }

        if (!reference_number) return true;

        // logger.info(`Pulling order with id: ${reference_number} from saleor`);

        // pull the order from saleor to get the line_item_ids for this package
        // const FullOrderData = await client.query<GetOrderQuery, GetOrderQueryVariables>({
        //     query: getOrderQuery,
        //     variables: {
        //         id: reference_number,
        //     },
        // });
        // const SaleorOrder = FullOrderData?.data?.order;

        // if (SaleorOrder && SaleorOrder.status !== 'FULFILLED') {
        //     const result = await fulfillOrderInSaleor({ SaleorOrder, packagedata, reference_number }, logger, client);
        //     if (result) logger.info('Fulfilling the order in Saleor was successfull!');
        // } else {
        //     logger.info('Order already completely fulfilled in Saleor or not found. Skipping');
        // }
    }

    return true;
};
