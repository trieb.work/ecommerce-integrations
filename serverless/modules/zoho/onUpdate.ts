import Bull from 'bull';

/**
 * Adds or updates a task runner async job for this Tenant.
 * The mailchimp job is used to sync items like general Store Settings and Vouchers from Saleor with Mailchimp.
 * @param redisOpts
 * @param appConfigId
 */
const onUpdateZoho = async (redisOpts, appConfigId :number) => {
    const mailchimpSyncQueue = new Bull('genericQueue', {
        redis: redisOpts,
        prefix: `${process.env.APM_ENV}_`,
    });

    await mailchimpSyncQueue.add('zohoSettingsSync', { appConfigId }, {
        attempts: 5,
        backoff: {
            type: 'exponential',
            delay: 10000,
        },
    }).catch((e) => console.error(e));

    return true;
};
export default onUpdateZoho;
