import * as printNode from 'lib/PrintNode';
import * as Sentry from '@sentry/node';
import { Package, SalesOrder, ZohoClientInstance } from '@trieb.work/zoho-inventory-ts';
import winston from 'winston';

const packageSlipPrinterDescription = process.env.PRINTNODE_PACKAGESLIP_PRINTER_DESCRIPTION;
const packageSlipPrinterId = parseInt(process.env.PRINTNODE_PACKAGESLIP_PRINTER_ID, 10);

const shippingLabelPrinterName = process.env.PRINTNODE_SHIPPING_LABEL_PRINTER || undefined;
const shippingLabelPrinterId = parseInt(process.env.PRINTNODE_SHIPPING_LABEL_PRINTER_ID, 10);
const shippingLabelPrinterPaper = process.env.PRINTNODE_SHIPPING_LABEL_PRINTER_PAPER;

/**
 * Calls Zoho for package slips as base64 string and calls printnode to print it out.
 * @param packages array of objects of not_shipped packages
 */
export const printPackageSlip = async (input :Package, zohoClient :ZohoClientInstance, logger :winston.Logger) => {
    logger.info(`Downloading Package Slip for ${input.package_id}`);
    const { base64Buffer: packageSlipString, filename } = await zohoClient.getDocumentBase64StringOrBuffer('packages', input.package_id);
    const printerId = packageSlipPrinterId || await printNode.getPrinterByName(packageSlipPrinterDescription);
    if (!printerId) {
        return null;
    }
    logger.info(`trying to add package slip to printer queue with ID ${printerId}`);
    try {
        const result = await printNode.printBase64(packageSlipString, filename, printerId, 1, { IdempotencyString: filename });
        logger.info(`Print Job with ID ${result} successful`);
    } catch (error) {
        if (error?.response?.status === 409) {
            logger.info('Idempotency from printnode says we already printed that document :) all good');
            return true;
        }
        Sentry.captureException(error);
    }

    return true;
};

/**
 * For packages to Switzerland etc. - handle the creation and printing of additional documents here. Needs a full
 * Salesorder with all invoices and attached CN23 documents
 * @param salesorder
 */
export const printCustomsDocuments = async (salesorder :SalesOrder, zohoClient :ZohoClientInstance, logger :winston.Logger) => {
    const invoice = salesorder?.invoices[0];
    const printDocuments = [];
    if (!invoice) {
        console.error('No invoices found! Cant print them');
    } else {
        const { base64Buffer, filename } = await zohoClient.getDocumentBase64StringOrBuffer('invoices', invoice.invoice_id);
        // we want to print the invoice three times
        printDocuments.push({ base64Buffer, filename, qty: 3 });
    }
    const cn23 = salesorder?.documents?.find((x) => x.file_name.includes('cn23_'))?.document_id;
    if (cn23) {
        logger.info('Found a cn23 customs statement to print');
        const { base64Buffer, filename } = await zohoClient.getDocumentFromSalesorderBase64String(salesorder.salesorder_id, cn23);
        printDocuments.push({ base64Buffer, filename });
    }
    const printers = await printNode.getPrinters();
    const packageSlipPrinter = printers.find((x) => x.name.includes(packageSlipPrinterDescription));
    if (!packageSlipPrinter) {
        logger.info('We got printers, but can\'t find a suitable one with our packageSlip printer filter pattern. Skipping');
        return null;
    }
    try {
        const result = printDocuments.map((document) => {
            printNode.printBase64(document.base64Buffer, document.filename, packageSlipPrinter.id, document.qty, { IdempotencyString: document.filename });
            return true;
        });
        await Promise.all(result);
    } catch (error) {
        console.error('Printing error', error);
    }
    return true;
};

/**
 * Prints out the carrier label of a package on a label printer
 * @param shipmentORderId
 */
export const printCarrierLabel = async (shipmentOrderId :string, zohoClient :ZohoClientInstance, logger :winston.Logger) => {
    const { base64Buffer, filename } = await zohoClient.getDocumentBase64StringOrBuffer('shipmentorders', shipmentOrderId, '/image');
    if (!base64Buffer) return true;
    const printerId = shippingLabelPrinterId || await printNode.getPrinterByName(shippingLabelPrinterName);

    if (!printerId) {
        logger.info('We got printers, but can\'t find a suitable one with our shipping label printer filter pattern. Skipping');
        return null;
    }

    try {
        const printResult = await printNode.printBase64(base64Buffer, filename, printerId, 1, { IdempotencyString: filename, rotate: 0, paper: shippingLabelPrinterPaper });
        logger.info(`Print Job for carrier label with id ${printResult} successfull!`);
    } catch (error) {
        if (error?.response?.status === 409) {
            logger.info('Idempotency from printnode says we already printed that document :) all good');
            return true;
        }
        console.error('Error printing the shipping label!', JSON.stringify(error));
        Sentry.captureException(error);
        return false;
    }
    return true;
};
