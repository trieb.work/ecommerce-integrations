import { Package, ZohoClientInstance } from '@trieb.work/zoho-inventory-ts';
import dayjs from 'dayjs';

/**
 * Pull and generate all needed customer data together to send out notification emails in the correct language.
 * @param zohoClient
 * @param fullPackageData
 */
export default async function getContactData(zohoClient :ZohoClientInstance, fullPackageData :Package) {
    const trackingPortal = fullPackageData?.shipment_order?.notes;
    const trackingPortalUrl = trackingPortal?.includes('https://') ? trackingPortal : '';
    /**
     * These E-Mail variables are send to Mailgun templates to personalize E-Mail Templates
     */
    const emailVariables = {
        FIRSTNAME: '',
        LASTNAME: '',
        TRACKINGNUMBER: fullPackageData.tracking_number,
        TRACKINGPROVIDER: fullPackageData.carrier,
        CURRENTYEAR: dayjs().format('YYYY'),
        TRACKINGPORTALURL: trackingPortalUrl,
    };

    // Pulling full customer data
    const customer = await zohoClient.getContactById(fullPackageData.customer_id);

    const userLanguage = customer.language_code || 'de';
    const shipmentNotificationsDisabled = customer?.custom_fields.find((x) => x.placeholder === 'cf_shipment_notifications_disa')?.value;

    // we look in the array of contact persons and search for the one assotiated with this package
    const contactPerson = customer.contact_persons.find((x) => x.contact_person_id === fullPackageData.contact_persons[0]);
    const contactPersonEmail = contactPerson?.email;
    emailVariables.FIRSTNAME = contactPerson?.first_name;
    emailVariables.LASTNAME = contactPerson?.last_name;

    return { emailVariables, contactPersonEmail, userLanguage, shipmentNotificationsDisabled, customer, trackingPortalUrl, contactPerson };
}
