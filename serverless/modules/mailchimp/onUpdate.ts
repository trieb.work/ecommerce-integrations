import Bull from 'bull';

/**
 * Adds or updates a task runner async job for this Tenant.
 * The mailchimp job is used to sync items like general Store Settings and Vouchers from Saleor with Mailchimp.
 * @param redisOpts
 * @param appConfigId
 */
const onUpdateMailchimp = async (redisOpts, appConfigId :number) => {
    const mailchimpSyncQueue = new Bull('mailchimpSyncJob', {
        redis: redisOpts,
        prefix: `${process.env.APM_ENV}_`,
    });
    // we want this sync job to run every hour
    const jobRepeatOptions = {
        repeat: {
            every: 3600000,
        },
    };

    await mailchimpSyncQueue.removeRepeatable(`Job_for_appConfigId_${appConfigId}`, jobRepeatOptions.repeat);

    await mailchimpSyncQueue.add(`Job_for_appConfigId_${appConfigId}`, { appConfigId }, jobRepeatOptions).catch((e) => console.error(e));
    await mailchimpSyncQueue.add(`AddHoc_Job_for_appConfigId_${appConfigId}`, { appConfigId }, {
        removeOnComplete: true,
        attempts: 5,
        timeout: 2000,
    }).catch((e) => console.error(e));

    return true;
};
export default onUpdateMailchimp;
