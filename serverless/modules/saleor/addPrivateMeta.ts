import { ApolloClient, NormalizedCacheObject } from '@trieb.work/apollo-client';
import { AddPrivateMetaMutation, AddPrivateMetaMutationVariables } from 'gqlTypes/globalTypes';
import { addPrivateMetaMutation } from 'lib/gql/saleor.gql';

/**
 * Adds private metadata to an entity
 * @param id
 * @param key the metadata key like "ZohoInventory.salesorder_id"
 * @param value
 * @param saleorGraphQLClient
 */
const addPrivateMetaToSaleorEntity = async (id :string, key :string, value :string, saleorGraphQLClient :ApolloClient<NormalizedCacheObject>) => {
    const MetadataResult = await saleorGraphQLClient.mutate<AddPrivateMetaMutation, AddPrivateMetaMutationVariables>({
        mutation: addPrivateMetaMutation,
        variables: {
            id,
            input: [{ key, value }],
        },
    });
    if (!MetadataResult.data || MetadataResult.data.updatePrivateMetadata.metadataErrors.length > 0) {
        throw new Error(`Error updating metadata in Saleor.\
                Received no answer or this error:${JSON.stringify(MetadataResult.data.updatePrivateMetadata.metadataErrors)}`);
    }
};
export default addPrivateMetaToSaleorEntity;
