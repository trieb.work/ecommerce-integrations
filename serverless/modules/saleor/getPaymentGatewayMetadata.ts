import { captureException } from '@sentry/node';
import { ApolloClient, NormalizedCacheObject } from '@trieb.work/apollo-client';
import { GetPaymentQuery, GetPaymentQueryVariables } from 'gqlTypes/globalTypes';
import Braintree from 'lib/Braintree';
import { paymentTransactionData } from 'lib/gql/saleor.gql';
import winston from 'winston';

/**
 * gets the needed metadata from braintree / payment gateway for this transaction
 * @param logger
 * @param braintree
 * @param paypalTransactionsAccountId
 * @param creditCardPaymentTransactionAccountId
 * @param braintreeSettings
 */
const getPaymentGatewayMetadata = async (saleorPaymentId :string, logger :winston.Logger, braintree :Braintree, saleorGraphQLClient :ApolloClient<NormalizedCacheObject>,
    paypalTransactionsAccountId, creditCardPaymentTransactionAccountId, braintreeActive :boolean) => {
    const transactionDetails = await saleorGraphQLClient.query<GetPaymentQuery, GetPaymentQueryVariables>({
        query: paymentTransactionData,
        variables: {
            id: saleorPaymentId,
        },
    });

    // this token is the transaction ID of the payment gateway. We add it to the invoices custom field
    const { token } = transactionDetails?.data?.payment?.transactions?.[0];

    let zohoPaymentCustomFields :{ api_name :string, value :string }[];
    let zohoPaymentAccountId;
    let zohoPaymentBankCharges;

    if (token) {
        const invoiceUpdateObject = {
            custom_fields: [{
                api_name: 'cf_zahlungs_id',
                value: token,
            }],
        };
            // we set the payment ID also as custom_field for the payment
        const paymentCustomField = [
            {
                api_name: 'cf_gateway_transaction_id',
                value: token,
            },
        ];
        zohoPaymentCustomFields = paymentCustomField;

        if (braintreeActive) {
            try {
                // get the full transaction data from Braintree. If we have a paypal transaction, we
                // set the PayPal ID as well.
                const fullBraintreeData = await braintree.getTransaction(token);
                const payPalTransactionId = fullBraintreeData?.paypalAccount?.authorizationId;
                const payPalTransactionFee = payPalTransactionId
                    ? parseFloat(fullBraintreeData?.paypalAccount?.transactionFeeAmount.replace(/,/g, '.')) : undefined;

                // We have a paypal transaction with fees and a corresponding Bank Account in Zoho Books.
                // great! we can set the bank account and fees when creating the payment
                if (payPalTransactionFee && paypalTransactionsAccountId) {
                    logger.info(`We got a PayPal Payment with Fees of ${payPalTransactionFee}. \
                    We create the payment on Zoho Books Bank Account with ID ${paypalTransactionsAccountId}`);
                    zohoPaymentAccountId = paypalTransactionsAccountId;
                    zohoPaymentBankCharges = payPalTransactionFee;
                }
                if (payPalTransactionId) {
                    invoiceUpdateObject.custom_fields.push({
                        api_name: 'cf_paypal_id',
                        value: payPalTransactionId,
                    });
                }

                // if this payment is a credit card payment and not paypal, we set the corresponding bank account id
                if (creditCardPaymentTransactionAccountId && !payPalTransactionId && fullBraintreeData?.id) {
                    logger.info('This is a credit card payment. Using the appropriate banking account');
                    zohoPaymentAccountId = creditCardPaymentTransactionAccountId;
                }
            } catch (error) {
                logger.error(`Error getting Braintree data ${error}`);
                captureException(new Error(error));
            }
        }

        return { invoiceUpdateObject, zohoPaymentAccountId, zohoPaymentBankCharges, zohoPaymentCustomFields };
    }
};

export default getPaymentGatewayMetadata;
