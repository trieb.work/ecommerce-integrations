import { ApolloClient, NormalizedCacheObject } from '@trieb.work/apollo-client';
import { GetShopDomainQuery, ShopInfoQuery, VerifyAppTokenMutation, VerifyAppTokenMutationVariables, VerifyTokenMutation, VerifyTokenMutationVariables } from 'gqlTypes/globalTypes';
import { getShopDomain, verifyAppTokenMutation, verifyTokenQuery } from 'lib/gql/saleor.gql';
import setupRequestSaleor from 'lib/setupRequest/setupRequestSaleor';
import { NextApiRequest } from 'next';
import { AuthChecker } from 'type-graphql';

/**
 * Run the verifyTokenQuery against Saleor
 * @param token
 */
export const verifyUserToken = async (token :string, client:ApolloClient<NormalizedCacheObject>) => {
    const verifyResult = await client.mutate<VerifyTokenMutation, VerifyTokenMutationVariables>({
        mutation: verifyTokenQuery,
        variables: {
            token,
        },
    });
    const valid = verifyResult.data?.tokenVerify?.isValid || false;

    return valid;
};

/**
 * Run the verifyAppTokenQuery against Saleor
 * @param token
 */
export const verifyAppToken = async (token :string, client:ApolloClient<NormalizedCacheObject>) => {
    const verifyResult = await client.mutate<VerifyAppTokenMutation, VerifyAppTokenMutationVariables>({
        mutation: verifyAppTokenMutation,
        variables: {
            token,
        },
    });
    const valid = verifyResult.data?.appTokenVerify?.valid || false;

    return valid;
};

export const getLoginSession = async (token :string, client:ApolloClient<NormalizedCacheObject>) => {
    const validity = await verifyUserToken(token, client);
    if (!validity) return null;
    const userSessionResult = await client.query<GetShopDomainQuery>({
        query: getShopDomain,
    });
    return userSessionResult.data?.shop?.domain;
};
type AuthType = {
    req: NextApiRequest,
};
export const customAuthChecker: AuthChecker<AuthType> = async ({ context }) => {
    const { valid } = await setupRequestSaleor({ req: context.req });
    // here we can read the user from context
    // and check his permission in the db against the `roles` argument
    // that comes from the `@Authorized` decorator, eg. ["ADMIN", "MODERATOR"]
    return valid;
};
