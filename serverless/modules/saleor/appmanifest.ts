/* eslint-disable max-len */
const baseUrl = process.env.ECI_BASE_URL;
if (!baseUrl) throw new Error('Mandatory ENV Variable ECI_BASE_URL not set! Can not proceed');

export const saleorManifest = () => {
    const manifest = {
        id: 'triebwork.eci',
        version: '1.0.0',
        name: 'eCommerce Integrations for Saleor',
        about: 'The trieb.work ECI-Integration for saleors is a powerful App used for several services like data synchronisation to Zoho Inventory, Mailchimp, an advanced product data feed etc.. ',
        permissions: ['MANAGE_SHIPPING', 'MANAGE_PRODUCTS', 'MANAGE_ORDERS', 'MANAGE_GIFT_CARD', 'MANAGE_DISCOUNTS', 'MANAGE_CHECKOUTS', 'MANAGE_PRODUCT_TYPES_AND_ATTRIBUTES'],
        appUrl: 'http://localhost:3000/app',
        configurationUrl: `${baseUrl}/saleor/configuration/`,
        tokenTargetUrl: `${baseUrl}/api/saleor/register/`,

        dataPrivacy: 'Lorem ipsum',
        dataPrivacyUrl: 'http://localhost:3000/app-data-privacy',
        homepageUrl: 'https://trieb.work',
        supportUrl: 'http://localhost:3000/support',
    };
    return manifest;
};
