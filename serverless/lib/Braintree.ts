/* eslint-disable max-classes-per-file */
import braintree from 'braintree';

const dev = process.env.APM_ENV === 'dev';
type Config = {
    merchantId :string
    publicKey :string
    privateKey :string
};

export default class Braintree {
    private merchantId :string;

    private publicKey :string;

    private privateKey :string;

    private gateway :braintree.BraintreeGateway;

    constructor(config: Config) {
        this.merchantId = config.merchantId;
        this.publicKey = config.publicKey;
        this.privateKey = config.privateKey;
        this.gateway = new braintree.BraintreeGateway({
            environment: dev ? braintree.Environment.Sandbox : braintree.Environment.Production,
            merchantId: this.merchantId,
            publicKey: this.publicKey,
            privateKey: this.privateKey,
        });
    }

    getTransaction = async (transactionId :string) => {
        const details = await this.gateway.transaction.find(transactionId);
        return details as braintree.Transaction;
    };
}
