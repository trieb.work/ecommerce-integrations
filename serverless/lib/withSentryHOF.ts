import * as Sentry from '@sentry/node';
import { NextApiHandler, NextApiRequest, NextApiResponse } from 'next';

const { NODE_ENV } = process.env;

Sentry.init({
    dsn: process.env.NEXT_PUBLIC_SENTRY_DSN,
    environment: process.env.APM_ENV,
});

export const withSentry = (apiHandler :NextApiHandler) => {
    if (NODE_ENV === 'production') {
        return async (req :NextApiRequest, res :NextApiResponse) => {
            try {
                return await apiHandler(req, res);
            } catch (error) {
                Sentry.captureException(error);
                console.error(error);
                const sentryPromise = Sentry.flush(2000);
                await Promise.all([sentryPromise]);
                return error;
            }
        };
    }
    return async (req :NextApiRequest, res :NextApiResponse) => apiHandler(req, res);
};
