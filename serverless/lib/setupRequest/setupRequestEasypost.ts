import { NextApiRequest } from 'next';
import { logAndMeasure } from '@trieb.work/apm-logging';
import { ZohoClientInstance } from '@trieb.work/zoho-inventory-ts';
import { PackageTrackerInstance } from '@trieb.work/internal-package-trackers';
import { TrackAndTraceConf, TrackAndTraceEmails } from '@prisma/client';
import eciPrismaClient from './prismaSetup';

/**
 * Call this function for every API route trigger to configure the ECI tenant that this request is used for. Exposes all needed helper functions
 * @param req
 */
const setupRequestEasypost = async (req :NextApiRequest) => {
    const logger = logAndMeasure(req, { elasticLoggingServer: undefined });
    let valid = false;

    // Dynamic pages like easypost, have the CUID in the Query Object
    const cuid = req?.query?.cuid as string;

    const prisma = eciPrismaClient();
    const currentTentantConfig = await prisma.appConfig.findFirst({ where: { easypost: { cuid } },
        include: { easypost: true,
            zoho: true,
            mailgun: true,
            trackandtrace: true } });

    let zohoClient :ZohoClientInstance;
    let packageTrackerClient :PackageTrackerInstance;
    let trackAndTraceConf :TrackAndTraceConf & {
        trackAndTraceEmails: TrackAndTraceEmails[];
    };
    if (currentTentantConfig) {
        valid = true;
        zohoClient = new ZohoClientInstance({
            zohoClientId: currentTentantConfig.zoho.clientId,
            zohoClientSecret: currentTentantConfig.zoho.clientSecret,
            zohoOrgId: currentTentantConfig.zoho.orgId,
        });
        await zohoClient.authenticate();
        packageTrackerClient = new PackageTrackerInstance({
            easyPostApiToken: currentTentantConfig.easypost.apiToken,
            sendgridApikey: currentTentantConfig.mailgun.apiToken,
            fromTitle: currentTentantConfig.mailgun.fromTitle,
            fromEmail: currentTentantConfig.mailgun.fromEmail,
            logger,
            zohoClient,
        });
        if (currentTentantConfig.trackandtrace) {
            trackAndTraceConf = await prisma.trackAndTraceConf.findFirst({ where: { id: currentTentantConfig.trackandtrace.id }, include: { trackAndTraceEmails: true } });
        }
    }

    return { logger, valid, zohoClient, packageTrackerClient, trackAndTraceConf };
};

export default setupRequestEasypost;
