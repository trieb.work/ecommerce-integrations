import { ZohoClientInstance } from '@trieb.work/zoho-inventory-ts';
import { NextApiRequest } from 'next';
import SaleorClient, { ApolloClient, NormalizedCacheObject } from '@trieb.work/apollo-client';
import { logAndMeasure } from '@trieb.work/apm-logging';
import { PackageTrackerInstance } from '@trieb.work/internal-package-trackers';
import eciPrismaClient from './prismaSetup';
import getCommonConfigWithCache from './getCommonConfigWithCache';

/**
 * Call this function for every API route trigger to configure the ECI tenant that this request is used for. Exposes all needed helper functions
 * @param req
 */
const setupRequestZoho = async (req :NextApiRequest) => {
    const commonConf = await getCommonConfigWithCache();
    const redisOpts = commonConf.redis;
    const elasticConf = commonConf.elasticsearch;
    const logger = logAndMeasure(req, { elasticLoggingServer: elasticConf?.loggingServer });
    let valid = false;

    const zohoWebhookToken = req?.body?.token as string || '';
    const zohoOrgId = req?.headers?.['x-com-zoho-organizationid'] as string || req?.query?.['zoho-org-id'] as string || req?.headers?.['dre-scope-id'] as string || '';

    const prisma = eciPrismaClient();
    const currentTentantConfig = await prisma.appConfig.findFirst({ where: { zoho: { orgId: zohoOrgId } },
        include: { saleor: true, zoho: true, mailchimp: true, easypost: true } });

    let saleorGraphQLClient :ApolloClient<NormalizedCacheObject>;
    let zohoClient :ZohoClientInstance;
    if (currentTentantConfig && (zohoWebhookToken === currentTentantConfig.zoho?.webhookToken)) {
        valid = true;
        zohoClient = new ZohoClientInstance({
            zohoClientId: currentTentantConfig.zoho.clientId,
            zohoClientSecret: currentTentantConfig.zoho.clientSecret,
            zohoOrgId: currentTentantConfig.zoho.orgId,
        });
        const saleorGraphQLEndpointFromConfig = `https://${currentTentantConfig.saleor.domain}/graphql/`;
        saleorGraphQLClient = SaleorClient(saleorGraphQLEndpointFromConfig, currentTentantConfig.saleor.authToken);
    }
    if (!currentTentantConfig?.easypost?.apiToken && valid) {
        logger.error(`No Easypost Api-Token found in Tenant Config ${currentTentantConfig?.easypost}`);
    }
    const packageTrackerClient = new PackageTrackerInstance({
        easyPostApiToken: currentTentantConfig?.easypost?.apiToken,
        logger,
    });

    return {
        logger,
        valid,
        saleorGraphQLClient,
        fullTenantConfig: currentTentantConfig,
        redisOpts,
        zohoClient,
        packageTrackerClient,
    };
};
export default setupRequestZoho;
