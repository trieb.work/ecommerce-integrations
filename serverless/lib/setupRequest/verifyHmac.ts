import hmacSHA256 from 'crypto-js/hmac-sha256';
import hex from 'crypto-js/enc-hex';
import utf8 from 'utf8';

/**
 * This functions verifies for example a webhook request using the HMAC method.
 * @param receivedHmac The Signature we get from the requester
 * @param dataString The request body as stringified JSON
 * @param key The shared secret to verify
 */
const verifyHmac = (
    receivedHmac :string,
    dataString :string,
    key :string,
) => {
    const generatedHmac = hmacSHA256(utf8.encode(dataString), key);
    const generatedHmacBase64 = hex.stringify(generatedHmac);
    console.log('generated', generatedHmacBase64)
    return generatedHmacBase64 === receivedHmac;
};
export default verifyHmac;
