import { PrismaClient } from '@prisma/client';

let prisma: PrismaClient;

const eciPrismaClient = () => {
    // Ensure the prisma instance is re-used during hot-reloading
    // Otherwise, a new client will be created on every reload
    globalThis.prisma = globalThis.prisma || new PrismaClient();
    prisma = globalThis.prisma;

    return prisma;
};
export default eciPrismaClient;
