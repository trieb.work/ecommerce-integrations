import { NextApiRequest } from 'next';
import { logAndMeasure } from '@trieb.work/apm-logging';
import eciPrismaClient from './prismaSetup';
import getCommonConfigWithCache from './getCommonConfigWithCache';

/**
 * Call this function for every API route trigger to configure the ECI tenant that this request is used for. Exposes all needed helper functions
 * @param req
 */
const setupRequestBraintreeUpload = async (req :NextApiRequest) => {
    const commonConf = await getCommonConfigWithCache();
    const elasticConf = commonConf.elasticsearch;
    const logger = logAndMeasure(req, { elasticLoggingServer: elasticConf?.loggingServer });
    let valid = false;

    const redisOpts = commonConf.redis;
    // For dynamic pages like productdtafeed, have the CUID in the Query Object
    const cuid = req?.query?.cuid as string;

    const prisma = eciPrismaClient();
    const currentTentantConfig = await prisma.appConfig.findFirst({ where: { zoho: { orgId: cuid } } });
    if (currentTentantConfig) {
        valid = true;
    }

    return { logger, valid, redisOpts, currentTentantConfig };
};

export default setupRequestBraintreeUpload;
