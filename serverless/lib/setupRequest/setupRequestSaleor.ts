/* eslint-disable arrow-body-style */
import { GetServerSidePropsContext, NextApiRequest } from 'next';
import SaleorClient, { ApolloClient, NormalizedCacheObject } from '@trieb.work/apollo-client';
import { verifyAppToken, verifyUserToken } from 'modules/saleor/auth';
import parseBearerToken from 'parse-bearer-token';
import { IncomingMessage } from 'http';
import { ParsedUrlQuery } from 'querystring';
import { ZohoClientInstance } from '@trieb.work/zoho-inventory-ts';
import { logAndMeasure } from '@trieb.work/apm-logging';
import { AppConfig, BraintreeConf, MailchimpConf, ProductDataFeed, SaleorConf, ZohoConf } from '@prisma/client';
import onUpdateMailchimp from 'modules/mailchimp/onUpdate';
import onUpdateZoho from 'modules/zoho/onUpdate';
import eciPrismaClient from './prismaSetup';
import getCommonConfigWithCache from './getCommonConfigWithCache';

const baseUrl = process.env.ECI_BASE_URL;
if (!baseUrl) throw new Error('Mandatory ENV Variable ECI_BASE_URL not set! Can not proceed');

/**
 * Call this function for every API route trigger to configure the ECI tenant that this request is used for. Exposes all needed helper functions
 * @param req
 */
const setupRequestSaleor = async ({ req, ctx } :{ req? :NextApiRequest, ctx? :GetServerSidePropsContext<ParsedUrlQuery> }) => {
    const request = req || ctx?.req;

    const commonConf = await getCommonConfigWithCache();
    const elasticConf = commonConf?.elasticsearch;
    if (!elasticConf) console.error('No elastic config found! Can not log and Measure performance');
    const logger = (request && commonConf) ? logAndMeasure(request, { elasticLoggingServer: elasticConf?.loggingServer }) : null;
    let valid = false;

    const prisma = eciPrismaClient();

    const saleorDomain = request?.headers?.['saleor-domain'] as string || undefined;
    const saleorConnectUrl = saleorDomain ? `https://${saleorDomain}/graphql/` : '';
    const saleorAuthToken = parseBearerToken(request as IncomingMessage) || request?.headers?.['saleor-token'] as string || '';
    const saleorAppToken = req?.body?.auth_token as string || '';
    const saleorEvent = ((req?.headers?.['x-saleor-event'] as string) || (req?.headers?.['saleor-event'] as string) || '').toLowerCase();

    // When we have a connect URL directly in the headers, we can create the Apollo Client instance right here
    const unauthenticatedSaleorGraphQLClient = SaleorClient(saleorConnectUrl);

    /**
     * Tells if this saleor request is valid or not. Checks all possible headers for bearer or app tokens
     * and validates them with Saleor.
     */
    if (saleorAuthToken) valid = await verifyUserToken(saleorAuthToken, unauthenticatedSaleorGraphQLClient);
    if (saleorAppToken) valid = await verifyAppToken(saleorAppToken, unauthenticatedSaleorGraphQLClient);

    if (!saleorAppToken && !saleorAuthToken) valid = false;

    const redisOpts = {
        host: commonConf.redis.host,
        port: commonConf.redis.port,
        password: commonConf.redis.password,
    };

    const getTenantConfig = async ({ saleor = false, zoho = false, productdatafeed = false, mailchimp = false, braintree = false }
    :{saleor ?:boolean, zoho?:boolean, productdatafeed?:boolean, mailchimp?:boolean, braintree?:boolean } = {}) => {
        return prisma.appConfig.findFirst({ where: { saleor: { domain: saleorDomain } }, include: { saleor, zoho, productdatafeed, mailchimp, braintree } });
    };

    const updateTenantConfig = async (id :number, updateData?) => {
        const data = updateData || { saleor: { update: { authToken: saleorAppToken } }, baseUrl };
        const updatedConfig = await prisma.appConfig.update({
            where: { id },
            data,
        });
        // when you update the mailchimp settings, we also start a sync job for mailchimp
        if (updateData?.mailchimp) {
            logger.info(`Updating the Mailchimp Sync Job for AppId ${id}`);
            await onUpdateMailchimp(redisOpts, id);
        }
        // when you update the zoho settings, we also start a sync job for zoho
        if (updateData?.zoho) {
            logger.info(`Updating the Zoho Sync Job for AppId ${id}`);
            await onUpdateZoho(redisOpts, id);
        }
        return updatedConfig;
    };

    const createTenantConfig = async () => {
        if (!saleorDomain) throw new Error('No saleor domain on register! This is an error');
        const createdConfig = await prisma.appConfig.create({
            data: {
                saleor: { create: { domain: saleorDomain, authToken: saleorAppToken } },
                zoho: { create: { active: false } },
                mailchimp: { create: { active: false } },
                baseUrl,
            } });
        return createdConfig;
    };

    /**
     * The Mailchimp settings - default is disabled,
     */
    let mailchimpActive = false;

    let saleorGraphQLClient :ApolloClient<NormalizedCacheObject>;

    let fullTenantConfig : AppConfig & {
        saleor: SaleorConf;
        zoho: ZohoConf;
        productdatafeed: ProductDataFeed;
        mailchimp: MailchimpConf;
        braintree: BraintreeConf;
    };

    let zohoClient :ZohoClientInstance;
    // When we do not have a Saleor Registration request
    if (!saleorAppToken) {
        const currentTentantConfig = await getTenantConfig({ saleor: true, zoho: true, mailchimp: true, braintree: true });

        fullTenantConfig = currentTentantConfig;
        mailchimpActive = currentTentantConfig?.mailchimp?.active || false;

        // Validate the webhook request using the HMAC Secret from the Database
        // valid = verifyHmac(saleorSignature, JSON.stringify(req.body), currentTentantConfig.saleor.webhookToken);
        valid = true;
        // When we have a Zoho Org, we configure the Zoho Library to use this credentials and create
        // a Saleor Client Instance with the config values from the DB.
        if (currentTentantConfig?.zoho?.orgId && valid) {
            zohoClient = new ZohoClientInstance({
                zohoClientId: currentTentantConfig.zoho.clientId,
                zohoClientSecret: currentTentantConfig.zoho.clientSecret,
                zohoOrgId: currentTentantConfig.zoho.orgId,
            });
        }
        if (!currentTentantConfig) logger.error('Can\'t create a valid Zoho Client Instance, as the currentTenantConfig is missing an orgId');

        // Use the bearer token from our tenant configuration to configure the Saleor GraphQL Client
        const saleorGraphQLEndpointFromConfig = `https://${currentTentantConfig?.saleor?.domain}/graphql/`;
        saleorGraphQLClient = SaleorClient(saleorGraphQLEndpointFromConfig, currentTentantConfig?.saleor?.authToken);
    }

    return {
        saleorGraphQLClient,
        unauthenticatedSaleorGraphQLClient,
        valid,
        logger,
        redisOpts,
        saleorAuthToken,
        saleorAppToken,
        saleorDomain,
        saleorEvent,
        getTenantConfig,
        updateTenantConfig,
        createTenantConfig,
        mailchimpActive,
        prisma,
        fullTenantConfig,
        zohoClient,
    };
};
export default setupRequestSaleor;
