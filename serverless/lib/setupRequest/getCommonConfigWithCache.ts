import { CommonConfig, ElasticConf, GoogleOAuth, RedisConf } from '@prisma/client';
import eciPrismaClient from './prismaSetup';

let globalCommonConf :CommonConfig & {
    redis: RedisConf;
    googleOAuth: GoogleOAuth;
    elasticsearch: ElasticConf;
};

/**
 * Gets the standard application settings and caches them. They are needed just once
 */
const getCommonConfigWithCache = async () => {
    if (globalCommonConf && globalCommonConf.elasticsearch && globalCommonConf.redis) return globalCommonConf;
    const client = eciPrismaClient();
    const commonConf = await client.commonConfig.findFirst({ where: { id: 1 }, include: { redis: true, googleOAuth: true, elasticsearch: true } });
    globalCommonConf = commonConf;
    return commonConf;
};
export default getCommonConfigWithCache;
