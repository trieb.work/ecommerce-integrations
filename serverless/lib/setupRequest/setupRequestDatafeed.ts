import { NextApiRequest } from 'next';
import SaleorClient, { ApolloClient, NormalizedCacheObject } from '@trieb.work/apollo-client';
import { logAndMeasure } from '@trieb.work/apm-logging';
import eciPrismaClient from './prismaSetup';
import getCommonConfigWithCache from './getCommonConfigWithCache';

/**
 * Custom Typeguard to check if this querystring is just a string
 * @param arg
 */
function isValidRequest(arg: any): arg is 'googlemerchant'|'facebookcommerce' {
    return typeof arg === 'string' && ['googlemerchant', 'facebookcommerce'].includes(arg);
}

/**
 * Call this function for every API route trigger to configure the ECI tenant that this request is used for. Exposes all needed helper functions
 * @param req
 */
const setupRequestDataFeed = async (req :NextApiRequest) => {
    const commonConf = await getCommonConfigWithCache();
    const elasticConf = commonConf.elasticsearch;
    let valid = false;

    const feedVariant = isValidRequest(req.query?.variant) ? req.query.variant : 'googlemerchant';

    // For dynamic pages like productdtafeed, have the CUID in the Query Object
    const cuid = req?.query?.cuid as string;

    let channel = '';

    const prisma = eciPrismaClient();
    const currentTentantConfig = await prisma.appConfig.findFirst({ where: { productdatafeed: { cuid } }, include: { productdatafeed: true, saleor: true } });
    const storefrontProductUrl = currentTentantConfig?.productdatafeed?.productDetailStorefrontURL;
    channel = currentTentantConfig?.productdatafeed?.channel;
    const logger = logAndMeasure({
        'saleor-domain': currentTentantConfig?.saleor?.domain,
        appConfigId: currentTentantConfig?.id,
    }, { elasticLoggingServer: elasticConf?.loggingServer });
    if (!storefrontProductUrl) logger.info('This app has no productDetailStorefrontURL set. Can not generate Productdatafeed');

    let saleorGraphQLClient :ApolloClient<NormalizedCacheObject>;
    if (currentTentantConfig && currentTentantConfig.productdatafeed.active && currentTentantConfig.productdatafeed.productDetailStorefrontURL) {
        // Use the bearer token from our tenant configuration to configure the Saleor GraphQL Client
        const saleorGraphQLEndpointFromConfig = `https://${currentTentantConfig.saleor.domain}/graphql/`;
        saleorGraphQLClient = SaleorClient(saleorGraphQLEndpointFromConfig, currentTentantConfig.saleor.authToken);
        valid = true;
    }

    return { logger, saleorGraphQLClient, valid, storefrontProductUrl, channel, feedVariant };
};

export default setupRequestDataFeed;
