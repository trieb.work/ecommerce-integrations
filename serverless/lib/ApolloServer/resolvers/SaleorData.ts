/* eslint-disable class-methods-use-this */
/* eslint-disable max-classes-per-file */
import { ChannelsQuery, ChannelsQueryVariables } from 'gqlTypes/globalTypes';
import { getChannelsQuery } from 'lib/gql/saleor.gql';
import setupRequestSaleor from 'lib/setupRequest/setupRequestSaleor';
import { GetServerSidePropsContext } from 'next';
import { Query, Resolver, Ctx, ObjectType, Field } from 'type-graphql';

@ObjectType()
class Channel {
    @Field()
    id :string;

    @Field()
    slug :string;

    @Field()
    isActive :boolean;

    @Field()
    name :string;
}

@ObjectType()
class SaleorDataClass {
    @Field(() => [Channel])
    channels?: Channel[];
}

@Resolver(() => SaleorDataClass)
export class SaleorDataResolver {
    @Query(() => SaleorDataClass, { description: 'Get data from the origin Saleor instance - like Channels', nullable: true })
    async getSaleorData(@Ctx() ctx :GetServerSidePropsContext): Promise<SaleorDataClass|null> {
        const { saleorGraphQLClient, valid } = await setupRequestSaleor({ ctx });
        if (!valid) return null;
        const channelData = await saleorGraphQLClient.query<ChannelsQuery, ChannelsQueryVariables>({
            query: getChannelsQuery,
        });
        if (channelData.data.channels) return channelData!.data;
        return null;
    }
}
