/* eslint-disable class-methods-use-this */
import setupRequestSaleor from 'lib/setupRequest/setupRequestSaleor';
import { GetServerSidePropsContext } from 'next';
import { Query, Resolver, Mutation, Arg, Authorized, Ctx, ID } from 'type-graphql';
import { AppConfig, AppConfigUpdateInput } from '@generated/type-graphql';

@Resolver(() => AppConfig)
export class AppConfigResolver {
    @Authorized()
    @Query(() => AppConfig, { description: 'Get a users AppConfig', nullable: true })
    async getAppConfig(@Ctx() ctx :GetServerSidePropsContext): Promise<AppConfig | null> {
        const { getTenantConfig } = await setupRequestSaleor({ ctx });
        return getTenantConfig();
    }

    @Authorized()
    @Mutation(() => AppConfig)
    async updateAppConfig(
        @Arg('AppConfigInputData', () => AppConfigUpdateInput) AppConfigInputData :AppConfigUpdateInput,
            @Arg('id', () => ID!) id :string,
            @Ctx() ctx :GetServerSidePropsContext,
    ): Promise<AppConfig> {
        const { updateTenantConfig } = await setupRequestSaleor({ ctx });

        return updateTenantConfig(parseInt(id, 10), AppConfigInputData);
    }
}
