import countries from 'i18n-iso-countries';

type Address = {
    attention :string,
    address: string,
    street2: string,
    city: string,
    zip: string,
    country: string
};

function transform(address:
{ company_name: string; first_name: string; last_name: string; street_address_1: string; street_address_2: any; city: any; postal_code: any; country: string | number; },
contactName: string) {
    const attention = contactName;
    const answer = {
        attention,
        address: address.street_address_1,
        street2: address.street_address_2,
        city: address.city,
        zip: address.postal_code,
        country: countries.getName(address.country, 'de'),
    };

    return answer;
}

type ReturnObject = {
    company_name: string,
    billing_address: Address,
    shipping_address: Address,
};

export default (input) :ReturnObject => {
    if (input.default_billing_address) {
        input.billing_address = input.default_billing_address;
        delete input.default_billing_address;
    }
    if (input.default_shipping_address) {
        input.shipping_address = input.default_shipping_address;
        delete input.default_shipping_address;
    }
    if (input.billing_address) {
        // set the company name of a contact - coming from the billing address company name.
        if (input.billing_address.company_name) input.company_name = input.billing_address.company_name;

        // transform the billing address to match the Zoho Format
        input.billing_address = transform(input.billing_address, input.contact_name);
    }
    if (input.shipping_address) {
        // set the company name of a contact - coming from the shipping address company name. Some people have a company set here
        // we prefer the shipping address company name in this case, as the package should be delivered succesfully - invoices could be re-created
        if (input.shipping_address?.company_name && input?.company_name && (input.shipping_address?.company_name !== input.company_name)) {
            console.log(`We have a different shipping address company (${input.shipping_address?.company_name})
                than billing address company (${input.company_name}). Writing it to street 2`);
            input.shipping_address.street_address_2 = input.shipping_address.company_name;
        } else if (!input.company_name && input.shipping_address.company_name) {
            console.log('We have a shipping address company and no billing address company. Setting shipping address company as main company');
            input.company_name = input.shipping_address.company_name;
        }

        // when we have just a shipping address com
        const shippingAddressContactName = `${input.shipping_address.first_name} ${input.shipping_address.last_name}`;
        input.shipping_address = transform(input.shipping_address, shippingAddressContactName);
    }

    return input;
};
