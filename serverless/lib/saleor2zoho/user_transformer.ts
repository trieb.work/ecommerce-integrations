import assert from 'assert';

import winston from 'winston';
import address_transformer from './address_transformer';

/**
 * this function transforms the body of the customer_created webhook to match the zoho schema.
 * @param {object} input
 * @param {boolean} versandService If set to true, we use the shipping address as contact person Id.
 */
export default function (input, versandService :boolean = false, logger :winston.Logger) {
    if (input.first_name && input.last_name && input.email) {
        input.contact_name = `${input.first_name} ${input.last_name}`;
        input.contact_persons = [{
            first_name: input.first_name,
            last_name: input.last_name,
            email: input.email,
            is_primary_contact: true,
        }];
    } else {
        input.contact_name = `${input.billing_address.first_name} ${input.billing_address.last_name}`;
        input.contact_persons = [{
            first_name: input.billing_address.first_name,
            last_name: input.billing_address.last_name,
            email: input.user_email,
            is_primary_contact: true,
        }];
    }
    if (versandService) {
        input.contact_persons[0] = {
            first_name: input.shipping_address.first_name,
            last_name: input.shipping_address.last_name,
            email: input.user_email,
        };
    }
    input = address_transformer(input);

    assert.match(input.contact_persons[0].email, /@/);

    input.contact_type = 'customer';

    delete input.type;
    delete input.date_joined;
    delete input.private_meta;
    delete input.is_active;
    delete input.meta;
    delete input.email;

    return input;
}
