import { AppConfigUpdateInput, Channel, ProductDataFeedConfigFragment } from 'gqlTypes/eciTypes';
import { useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CardSpacer from 'components/saleor/CardSpacer';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import CustomSelect from 'components/saleor/CustomSelect';
import Grid from '@material-ui/core/Grid';

const ProductDataFeedConfigCard = ({ initialData, handleSubmit, loading, baseUrl, channels }
:{ loading :boolean, initialData :ProductDataFeedConfigFragment, channels :Channel[], baseUrl :string, handleSubmit :(variables: AppConfigUpdateInput) => Promise<void> }) => {
    const [data, setData] = useState<ProductDataFeedConfigFragment>(initialData);
    type Channel = {
        name: string,
        value: string,
    };
    const [channel, setChannel] = useState<Channel>();

    useEffect(() => {
        setData(initialData);
    }, [initialData]);

    useEffect(() => {
        if (initialData?.channel && channels) {
            const channelName = channels.find((x) => x.slug === initialData.channel)?.name;
            setChannel({ name: channelName, value: initialData.channel });
        }
    }, [channels, initialData]);

    const handleChannelChange = (event) => {
        setChannel({ name: event.target.label, value: event.target.value });
    };
    const handleChange = (event) => {
        const eventValue = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
        setData({ ...data, [event.target.name]: eventValue });
    };

    const updateOrUpsert = data?.id ? {
        productdatafeed: {
            update: {
                active: {
                    set: data?.active || false,
                },
                productDetailStorefrontURL: {
                    set: data?.productDetailStorefrontURL || '',
                },
                channel: {
                    set: channel?.value,
                },
            } },
    } : {
        productdatafeed: {
            create: {
                active: data?.active || false,
                productDetailStorefrontURL: data?.productDetailStorefrontURL,
                channel: channel?.value,
            },
        },
    };
    const availableChannels = channels?.map((x) => ({
        value: x?.slug,
        name: x?.name,
    }));

    return (
        <Card variant="outlined">
            <CardHeader title="Advanced Productdatafeed Settings" />
            <Divider />
            <CardContent>
                <Typography>Create a valid product data feed for Google Merchant, Facebook Commerce, etc.</Typography>
                <Typography>
                    The Unit Price gets calculated via the shipping weight of a product or variant.
                    Product Metadata Key "EAN" gets automatically added as GTIN.
                </Typography>
                <CardSpacer />
                <form noValidate autoComplete="off">
                    <Grid
                        container
                        direction="row"
                        justify="flex-start"
                        alignItems="center"
                    >
                        <Grid item>
                            <FormControlLabel
                                control={(
                                    <Checkbox
                                        checked={data?.active}
                                        onChange={handleChange}
                                        name="active"
                                    />
                                )}
                                label="Active"
                            />
                        </Grid>
                        <Grid item>
                            <CustomSelect menuItems={availableChannels} handleChange={handleChannelChange} activeMenuItem={channel} />
                        </Grid>
                    </Grid>

                    <CardSpacer />
                    <TextField
                        fullWidth
                        id="outlined-basic"
                        label="Product-Detail Storefront URL"
                        name="productDetailStorefrontURL"
                        value={data?.productDetailStorefrontURL}
                        variant="outlined"
                        helperText="The Product URL Pattern that gets generated for every product. {slug} gets replaced with the actual product slug."
                        onChange={handleChange}
                    />
                    <CardSpacer />
                    <TextField
                        fullWidth
                        id="datafeed.url.googlemerchant"
                        value={data?.cuid ? `${baseUrl}/api/productdatafeed/${data.cuid}/?variant=googlemerchant` : '***Activate first***'}
                        name="url"
                        helperText="This is the URL to your own product data feed. Add it to Google Merchant Center."
                        label="Google Merchant URL"
                        variant="outlined"
                        disabled
                    />
                    <CardSpacer />
                    <TextField
                        fullWidth
                        id="datafeed.url.facebookcommerce"
                        value={data?.cuid ? `${baseUrl}/api/productdatafeed/${data.cuid}/?variant=facebookcommerce` : '***Activate first***'}
                        name="url"
                        helperText="This is the URL to your own product data feed. Add it to Facebook Commerce."
                        label="Facebook Commerce URL"
                        variant="outlined"
                        disabled
                    />
                    <CardSpacer />
                </form>

                <CardSpacer />
                <Button
                    variant="contained"
                    disabled={loading}
                    onClick={() => handleSubmit(
                        updateOrUpsert,
                    )}
                >
                    Save
                </Button>
            </CardContent>
        </Card>
    );
};
export default ProductDataFeedConfigCard;
