import gql from 'graphql-tag';

const dataFeedFragment = gql`
    fragment ProductDataFeedConfig on ProductDataFeed {
        active
        productDetailStorefrontURL
        id
        cuid
        channel
    }
`;
const zohoFragment = gql`
    fragment ZohoConfig on ZohoConf {
        active
        id
        orgId
        clientSecret
        clientId
        appConfigId
        payPalAccountId
        creditCardAccountId
    }
`;
const braintreeFragment = gql`
    fragment BraintreeConfig on BraintreeConf {
        active
        id
        merchantId
        publicKey
        privateKey
    }
`;

const trackAndTraceEmailFragment = gql`
    fragment TaTEmail on TrackAndTraceEmails {
        id
        language
        inTransitEmailEnabled
        inTransitEmailTemplateName
        inTransitEmailSubject
        outForDeliveryEmailEnabled
        outForDeliveryEmailTemplateName
        outForDeliveryEmailSubject
    }
`;

const trackAndTraceFragment = gql`
    ${trackAndTraceEmailFragment}
    fragment TrackAndTraceConfig on TrackAndTraceConf {
        active
        id
        trackAndTraceEmails {
            ...TaTEmail
        }
    }
`;
const mailgunFragment = gql`
    fragment MailgunConfig on MailgunConf {
        id
        apiToken
        fromDomain
        fromTitle
        fromEmail
    }
`;
const easyPostFragment = gql`
    fragment EasyPostConfig on EasyPostConf {
        id
        apiToken
    }
`;
const mailchimpFragment = gql`
    fragment MailchimpConfig on MailchimpConf {
        active
        id
        apiToken
        listId
        storeUrl
        productUrlPattern
    }
`;

export const getSaleorDataQuery = gql`
    query saleorData {
        getSaleorData {
            channels {
                id
                slug
                name
                isActive
            }
        }
    }
`;

export const getAppConfig = gql`
    ${zohoFragment}
    ${dataFeedFragment}
    ${mailchimpFragment}
    ${braintreeFragment}
    ${trackAndTraceFragment}
    ${easyPostFragment}
    ${mailgunFragment}
    query appConfig {
        getAppConfig {
            id
            baseUrl
            saleor {
                id
                domain
                appId
            }
            zoho {
                ...ZohoConfig
            }
            productdatafeed {
                ...ProductDataFeedConfig
            }
            mailchimp {
                ...MailchimpConfig
            }
            braintree {
                ...BraintreeConfig
            }
            easypost {
                ...EasyPostConfig
            }
            mailgun {
                ...MailgunConfig
            }
            trackandtrace {
                ...TrackAndTraceConfig
            }
        }
    }
`;

export const updateAppConfig = gql`
    mutation AppConfigUpdate($id: ID!, $AppConfigInputData:  AppConfigUpdateInput! ) {
    updateAppConfig(
        id: $id,
        AppConfigInputData: $AppConfigInputData
    ) {
        zoho {
            active
            }
        }
    }
`;
