import { AppConfigUpdateInput, MailchimpConf } from 'gqlTypes/eciTypes';
import { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CardSpacer from 'components/saleor/CardSpacer';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import SecretInputField from 'components/saleor/SecretInputField';

const MailchimpConfigCard = ({ initialData, handleSubmit, loading } :
{ initialData :MailchimpConf, loading :boolean, handleSubmit :(variables: AppConfigUpdateInput) => Promise<void> }) => {
    const [data, setData] = useState<MailchimpConf>(initialData);

    const handleChange = (event) => {
        const eventValue = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
        setData({ ...data, [event.target.name]: eventValue });
    };

    const updateOrUpsert = data?.id ? {
        mailchimp: {
            update: {
                active: {
                    set: data?.active || false,
                },
                apiToken: {
                    set: data?.apiToken || '',
                },
                listId: {
                    set: data?.listId || '',
                },
                storeUrl: {
                    set: data?.storeUrl || '',
                },
                productUrlPattern: {
                    set: data?.productUrlPattern || '',
                },
            } },
    } : {
        mailchimp: {
            create: {
                active: data?.active || false,
                apiToken: data?.apiToken,
                listId: data?.listId,
                storeUrl: data?.storeUrl,
                productUrlPattern: data?.productUrlPattern,
            },
        },
    };

    return (
        <Card variant="outlined">
            <CardHeader title="MailChimp Settings" />
            <Divider />
            <CardContent>
                <form noValidate autoComplete="off">
                    <FormControlLabel
                        control={(
                            <Checkbox
                                checked={data?.active}
                                onChange={handleChange}
                                name="active"
                            />
                        )}
                        label="Active"
                    />
                    <CardSpacer />
                    <SecretInputField
                        fullWidth
                        label="Api-Token"
                        value={data?.apiToken || ''}
                        name="apiToken"
                        onChange={handleChange}
                    />
                    <CardSpacer />
                    <TextField
                        fullWidth
                        label="List-Id"
                        value={data?.listId || ''}
                        name="listId"
                        variant="outlined"
                        onChange={handleChange}
                    />
                    <CardSpacer />
                    <TextField
                        fullWidth
                        label="Store Domain"
                        value={data?.storeUrl}
                        name="storeUrl"
                        variant="outlined"
                        helperText="The Storefront URL that gets used in E-Commerce Notification E-Mails."
                        onChange={handleChange}
                    />
                    <CardSpacer />
                    <TextField
                        fullWidth
                        label="Product Link URL Pattern"
                        placeholder="https://store.example.de/product/{slug}"
                        value={data?.productUrlPattern}
                        name="productUrlPattern"
                        variant="outlined"
                        helperText="The Product URL Pattern that gets generated for every product. {slug} gets replaced with the actual product slug."
                        onChange={handleChange}
                    />
                </form>
                <CardSpacer />
                <Button
                    variant="contained"
                    disabled={loading}
                    onClick={() => handleSubmit(
                        updateOrUpsert,
                    )}
                >
                    Save
                </Button>
            </CardContent>
        </Card>
    );
};
export default MailchimpConfigCard;
