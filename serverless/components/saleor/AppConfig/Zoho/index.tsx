import { AppConfigUpdateInput, ZohoConfigFragment, BraintreeConfigFragment } from 'gqlTypes/eciTypes';
import { useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CardSpacer from 'components/saleor/CardSpacer';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import SecretInputField from 'components/saleor/SecretInputField';
import { Typography } from '@material-ui/core';

const ZohoConfigCard = ({ initialData, intitialBraintreeData, handleSubmit, loading, baseUrl }
:{ loading :boolean, initialData :ZohoConfigFragment, intitialBraintreeData :BraintreeConfigFragment, 
    baseUrl :string, handleSubmit :(variables: AppConfigUpdateInput) => Promise<void> }) => {
    const [data, setData] = useState<ZohoConfigFragment>(initialData);
    const [braintreeData, setBraintreeData] = useState<BraintreeConfigFragment>(intitialBraintreeData);

    useEffect(() => {
        setData(initialData);
    }, [initialData]);
    useEffect(() => {
        setBraintreeData(intitialBraintreeData);
    }, [intitialBraintreeData]);

    const handleChange = (event) => {
        const eventValue = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
        setData({ ...data, [event.target.name]: eventValue });
    };
    const handleBraintreeChange = (event) => {
        const eventValue = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
        setBraintreeData({ ...braintreeData, [event.target.name]: eventValue });
    };
    const updateOrUpsertBraintree = braintreeData?.id ? {
        braintree: {
            update: {
                active: {
                    set: braintreeData?.active || false,
                },
                merchantId: {
                    set: braintreeData?.merchantId || '',
                },
                publicKey: {
                    set: braintreeData?.publicKey || '',
                },
                privateKey: {
                    set: braintreeData?.privateKey || '',
                },
            },
        } } : {
        braintree: {
            create: {
                active: braintreeData?.active || false,
                merchantId: braintreeData?.merchantId || '',
                publicKey: braintreeData?.publicKey || '',
                privateKey: braintreeData?.privateKey || '',
            },
        },
    };

    const updateOrUpsert = data?.id ? {
        zoho: {
            update: {
                active: {
                    set: data?.active || false,
                },
                orgId: {
                    set: data?.orgId || '',
                },
                clientId: {
                    set: data?.clientId || '',
                },
                clientSecret: {
                    set: data?.clientSecret || '',
                },
                payPalAccountId: {
                    set: data?.payPalAccountId || '',
                },
                creditCardAccountId: {
                    set: data?.creditCardAccountId || '',
                },
            } },
    } : {
        zoho: {
            create: {
                active: data?.active || false,
                orgId: data?.orgId,
                clientId: data?.clientId,
                clientSecret: data?.clientSecret,
                payPalAccountId: data?.payPalAccountId,
                creditCardAccountId: data?.creditCardAccountId,
            },
        },
    };

    return (
        <Card variant="outlined">
            <CardHeader title="Zoho Inventory Settings" />
            <Divider />
            <CardContent>
                <form noValidate autoComplete="off">
                    <FormControlLabel
                        control={(
                            <Checkbox
                                checked={data?.active}
                                onChange={handleChange}
                                name="active"
                            />
                        )}
                        label="Active"
                    />
                    <CardSpacer />
                    <TextField
                        fullWidth
                        id="zoho.org-id"
                        label="Org-Id"
                        value={data?.orgId}
                        name="orgId"
                        variant="outlined"
                        onChange={handleChange}
                    />
                    <CardSpacer />
                    <TextField
                        fullWidth
                        label="Client-Id"
                        name="clientId"
                        value={data?.clientId}
                        variant="outlined"
                        onChange={handleChange}
                    />
                    <CardSpacer />
                    <SecretInputField
                        fullWidth
                        label="Client-Secret"
                        value={data?.clientSecret}
                        name="clientSecret"
                        onChange={handleChange}
                    />
                    <CardSpacer />
                    <TextField
                        fullWidth
                        label="PayPal Payments Account Id (optional)"
                        helperText="Zoho Books: add the banking account id where you want to deposit PayPal transactions and fees"
                        value={data?.payPalAccountId}
                        name="payPalAccountId"
                        onChange={handleChange}
                        variant="outlined"
                    />
                    <CardSpacer />
                    <TextField
                        fullWidth
                        label="Credit Card Payments Account Id (optional)"
                        helperText="Zoho Books: add the banking account id where you want to deposit CreditCard transactions and fees"
                        value={data?.creditCardAccountId}
                        name="creditCardAccountId"
                        onChange={handleChange}
                        variant="outlined"
                    />
                    <CardSpacer />
                    <Button
                        variant="contained"
                        disabled={loading}
                        onClick={() => handleSubmit(
                            updateOrUpsert,
                        )}
                    >
                        Save
                    </Button>
                    <CardSpacer />
                    <Divider />
                    <CardSpacer />
                    <Typography variant="h5">Braintree</Typography>
                    <Typography>When the Braintree-Gateway is used to process payments, we can use it to transfer banking-fees to Zoho</Typography>
                    <CardSpacer />
                    <FormControlLabel
                        control={(
                            <Checkbox
                                checked={braintreeData?.active}
                                onChange={handleBraintreeChange}
                                name="active"
                            />
                        )}
                        label="Active"
                    />
                    <CardSpacer />
                    <TextField
                        fullWidth
                        label="Braintree Merchant-Id"
                        value={braintreeData?.merchantId}
                        name="merchantId"
                        onChange={handleBraintreeChange}
                        variant="outlined"
                    />
                    <CardSpacer />
                    <TextField
                        fullWidth
                        label="Braintree Public-Key"
                        value={braintreeData?.publicKey}
                        name="publicKey"
                        onChange={handleBraintreeChange}
                        variant="outlined"
                    />
                    <CardSpacer />
                    <SecretInputField
                        fullWidth
                        label="Braintree Private-Key"
                        value={braintreeData?.privateKey}
                        name="privateKey"
                        onChange={handleBraintreeChange}
                    />
                    <CardSpacer />
                    <TextField
                        fullWidth
                        disabled
                        label="Manual Upload Transaction Fees .csv URL"
                        value={data?.orgId ? `${baseUrl}/saleor/btfeesupload?zohoOrg=${data.orgId}` : 'Add Zoho Org first'}
                        helperText="Manually upload the Braintree statement 'transaction fees monthly report' to automatically add transaction fees"
                        variant="outlined"
                    />
                    <CardSpacer />
                    <Button
                        variant="contained"
                        disabled={loading}
                        onClick={() => handleSubmit(
                            updateOrUpsertBraintree,
                        )}
                    >
                        Save
                    </Button>
                </form>

            </CardContent>
        </Card>
    );
};
export default ZohoConfigCard;
