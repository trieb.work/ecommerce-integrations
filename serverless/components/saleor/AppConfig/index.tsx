import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CardSpacer from 'components/saleor/CardSpacer';
import { useMutation, useQuery } from '@trieb.work/apollo-client';
import { CircularProgress } from '@material-ui/core';
import { AppConfigQuery, AppConfigUpdateInput, SaleorDataQuery } from 'gqlTypes/eciTypes';
import { getAppConfig, getSaleorDataQuery, updateAppConfig } from './index.gql';
import Unauthorized from '../Unauthorized';
import ZohoConfigCard from './Zoho';
import ProductDataFeedConfigCard from './ProductDataFeed';
import MailchimpConfigCard from './Mailchimp';
import TrackAndTraceConfigCard from './TrackAndTrace';

const AppConfiguration = () => {
    const { data, loading, error, refetch } = useQuery<AppConfigQuery>(getAppConfig);
    const { data: channelData } = useQuery<SaleorDataQuery>(getSaleorDataQuery);

    const [updateData, { loading: mutating }] = useMutation(updateAppConfig);

    const handleSubmit = async (variables :AppConfigUpdateInput) => {
        await updateData({ variables: {
            id: data.getAppConfig.id,
            AppConfigInputData: variables,
        } });
        refetch();
    };

    return (
        <Box>
            <Container>
                {
                    (loading || process.browser === undefined) && <CircularProgress />
                }
                {
                    error && !data && <Unauthorized />
                }
                <div>
                    { !loading && data?.getAppConfig.saleor && (
                        <div id={data.getAppConfig.id.toString()}>
                            <Card variant="outlined">
                                <CardHeader title="Saleor Data" />
                                <Divider />
                                <CardContent>
                                    <form noValidate autoComplete="off">
                                        <TextField fullWidth disabled id="outlined-basic" label="Domain" value={data.getAppConfig.saleor.domain} variant="outlined" />
                                    </form>
                                </CardContent>
                            </Card>
                            <CardSpacer />
                            <ZohoConfigCard
                                initialData={data.getAppConfig.zoho}
                                intitialBraintreeData={data.getAppConfig.braintree}
                                handleSubmit={handleSubmit}
                                baseUrl={data.getAppConfig.baseUrl}
                                loading={mutating}
                            />
                            <CardSpacer />
                            <ProductDataFeedConfigCard
                                initialData={data.getAppConfig.productdatafeed}
                                baseUrl={data.getAppConfig.baseUrl}
                                channels={channelData?.getSaleorData?.channels}
                                handleSubmit={handleSubmit}
                                loading={mutating}
                            />
                            <CardSpacer />
                            <MailchimpConfigCard
                                initialData={data.getAppConfig.mailchimp}
                                handleSubmit={handleSubmit}
                                loading={mutating}
                            />
                            <CardSpacer />
                            <TrackAndTraceConfigCard
                                initialData={data.getAppConfig.trackandtrace}
                                mailgunConfig={data.getAppConfig.mailgun}
                                easypostConfig={data.getAppConfig.easypost}
                                handleSubmit={handleSubmit}
                                loading={mutating}
                            />

                        </div>
                    )}
                </div>

            </Container>

        </Box>
    );
};
export default AppConfiguration;
