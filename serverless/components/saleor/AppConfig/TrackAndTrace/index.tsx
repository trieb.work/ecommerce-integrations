import { AppConfigUpdateInput, EasyPostConfigFragment, Language, MailgunConfigFragment, TrackAndTraceConfigFragment } from 'gqlTypes/eciTypes';
import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CardSpacer from 'components/saleor/CardSpacer';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Paper from '@material-ui/core/Paper';
import Tab from '@material-ui/core/Tab';
import TabContext from '@material-ui/lab/TabContext';
import TabList from '@material-ui/lab/TabList';
import TabPanel from '@material-ui/lab/TabPanel';
import Typography from '@material-ui/core/Typography';
import SecretInputField from 'components/saleor/SecretInputField';
import EmailTab from './emailTab';

const TrackAndTraceConfigCard = ({ initialData, handleSubmit, loading, mailgunConfig, easypostConfig } :
{ initialData :TrackAndTraceConfigFragment, mailgunConfig :MailgunConfigFragment, easypostConfig :EasyPostConfigFragment,
    loading :boolean, handleSubmit :(variables: AppConfigUpdateInput) => Promise<void> }) => {
    const [data, setData] = useState<TrackAndTraceConfigFragment>(initialData);
    const [trackAndTraceEmailsData, setTrackAndTraceEmailsData] = useState(initialData?.trackAndTraceEmails);
    const [easypostData, setEasypostData] = useState<EasyPostConfigFragment>(easypostConfig);
    const [mailgunData, setmailgunData] = useState<MailgunConfigFragment>(mailgunConfig);

    const [activeTab, setActiveTab] = useState('1');

    const handleTabChange = (event, newValue) => {
        setActiveTab(newValue);
    };

    const handleChange = (event) => {
        const eventValue = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
        setData({ ...data, [event.target.name]: eventValue });
    };
    const handleChangeEasypost = (event) => {
        const eventValue = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
        setEasypostData({ ...easypostData, [event.target.name]: eventValue });
    };
    const handleChangeMailgun = (event) => {
        const eventValue = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
        setmailgunData({ ...mailgunData, [event.target.name]: eventValue });
    };

    const easypostUpsert = easypostData?.id ? {
        update: {
            apiToken: { set: easypostData?.apiToken || '' },
        },
    } : {
        create: {
            apiToken: easypostData?.apiToken || '',
        } };
    const mailgunUpsert = mailgunData?.id ? {
        update: {
            apiToken: { set: mailgunData?.apiToken || '' },
            fromDomain: { set: mailgunData?.fromDomain || '' },
            fromEmail: { set: mailgunData?.fromEmail || '' },
            fromTitle: { set: mailgunData?.fromTitle || '' },
        },
    } : {
        create: {
            apiToken: mailgunData?.apiToken || '',
            fromDomain: mailgunData?.fromDomain || '',
            fromEmail: mailgunData?.fromEmail || '',
            fromTitle: mailgunData?.fromTitle || '',
        },
    };
    const trackAndTraceEmailsUpsert = trackAndTraceEmailsData?.map((email) => ({
        where: { language: email.language },
        create: {
            language: email.language,
            inTransitEmailEnabled: email.inTransitEmailEnabled,
            inTransitEmailTemplateName: email.inTransitEmailTemplateName,
            inTransitEmailSubject: email.inTransitEmailSubject,
            outForDeliveryEmailEnabled: email.outForDeliveryEmailEnabled,
            outForDeliveryEmailTemplateName: email.outForDeliveryEmailTemplateName,
            outForDeliveryEmailSubject: email.outForDeliveryEmailSubject,
        },
        update: {
            inTransitEmailEnabled: { set: email.inTransitEmailEnabled || false },
            inTransitEmailTemplateName: { set: email.inTransitEmailTemplateName || '' },
            inTransitEmailSubject: { set: email.inTransitEmailSubject || '' },
            outForDeliveryEmailEnabled: { set: email.outForDeliveryEmailEnabled || false },
            outForDeliveryEmailTemplateName: { set: email.outForDeliveryEmailTemplateName || '' },
            outForDeliveryEmailSubject: { set: email.outForDeliveryEmailSubject || '' },
        },
    }));
    const trackAndTraceEmailsCreateOnly = trackAndTraceEmailsData?.map((email) => ({
        language: email.language,
        inTransitEmailEnabled: email.inTransitEmailEnabled,
        inTransitEmailTemplateName: email.inTransitEmailTemplateName,
        outForDeliveryEmailEnabled: email.outForDeliveryEmailEnabled,
        outForDeliveryEmailTemplateName: email.outForDeliveryEmailTemplateName,
    }));
    const trackAndTraceEmailsTotal = {
        upsert: trackAndTraceEmailsUpsert,
    };

    const trackAndTraceUpsert = data?.id ? {
        update: {
            active: {
                set: data?.active || false,
            },
            trackAndTraceEmails: trackAndTraceEmailsTotal,
        } } : {
        create: {
            active: data?.active || false,
            trackAndTraceEmails: {
                create: trackAndTraceEmailsCreateOnly,
            },
        },
    };

    const updateOrUpsert = {
        trackandtrace: trackAndTraceUpsert,
        mailgun: mailgunUpsert,
        easypost: easypostUpsert,
    };

    return (
        <Card variant="outlined">
            <CardHeader title="Track & Trace" />
            <Divider />
            <CardContent>
                <form noValidate autoComplete="off">
                    <FormControlLabel
                        control={(
                            <Checkbox
                                checked={data?.active}
                                onChange={handleChange}
                                name="active"
                            />
                        )}
                        label="Active"
                    />
                    <CardSpacer />
                    <SecretInputField
                        onChange={handleChangeEasypost}
                        error={!easypostData?.apiToken}
                        name="apiToken"
                        value={easypostData?.apiToken || ''}
                        label="EasyPost API Token"
                    />
                    <CardSpacer />
                    <SecretInputField
                        label="Sendgrid API Token"
                        value={mailgunData?.apiToken}
                        error={!mailgunData?.apiToken}
                        name="apiToken"
                        onChange={handleChangeMailgun}
                    />
                    <CardSpacer />
                    <TextField
                        fullWidth
                        label="From Email"
                        value={mailgunData?.fromEmail}
                        placeholder="noreply@example.com"
                        name="fromEmail"
                        variant="outlined"
                        onChange={handleChangeMailgun}
                    />
                    <CardSpacer />
                    <TextField
                        fullWidth
                        label="From Title"
                        placeholder="Acme Corp."
                        value={mailgunData?.fromTitle}
                        name="fromTitle"
                        variant="outlined"
                        onChange={handleChangeMailgun}
                    />
                </form>
                <CardSpacer />
                <Typography style={{ marginBottom: '10px' }}>
                    Change the E-Mail Template Settings here for the different user languages. You can setup any HTML E-Mail template in Sendgrid.
                    The placeholders FIRSTNAME, LASTNAME, TRACKINGNUMBER, TRACKINGPROVIDER, CURRENTYEAR and TRACKINGPORTALURL
                    get replaced automatically.
                    T&T currently needs Zoho Inventory to work.
                </Typography>
                <TabContext value={activeTab}>
                    <Paper square>
                        <TabList
                            textColor="primary"
                            onChange={handleTabChange}
                            aria-label="disabled tabs example"
                        >
                            <Tab label="German" value="1" />
                            <Tab label="English" value="2" />
                        </TabList>
                    </Paper>
                    <TabPanel value="1">
                        <EmailTab mailData={trackAndTraceEmailsData} setMailData={setTrackAndTraceEmailsData} language={Language.De} />
                    </TabPanel>
                    <TabPanel value="2">
                        <EmailTab mailData={trackAndTraceEmailsData} setMailData={setTrackAndTraceEmailsData} language={Language.En} />
                    </TabPanel>
                </TabContext>
                <CardSpacer />
                <Button
                    variant="contained"
                    disabled={loading}
                    onClick={() => handleSubmit(
                        updateOrUpsert,
                    )}
                >
                    Save
                </Button>
            </CardContent>
        </Card>
    );
};
export default TrackAndTraceConfigCard;
