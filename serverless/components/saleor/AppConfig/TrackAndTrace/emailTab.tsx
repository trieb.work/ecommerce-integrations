import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Language, TaTEmailFragment } from 'gqlTypes/eciTypes';
import { useEffect, useState } from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import CardSpacer from 'components/saleor/CardSpacer';
import Divider from '@material-ui/core/Divider';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
}));

const EmailTab = ({ mailData, setMailData, language } :{ mailData :TaTEmailFragment[], setMailData, language :Language}) => {
    const [currentLanguageData, setCurrentLanguageData] = useState(() => mailData?.find((x) => x.language === language));
    const [index, setIndex] = useState<number>(() => mailData?.findIndex((x) => x.language === language));
    useEffect(() => {
        setCurrentLanguageData(mailData?.find((x) => x.language === language));
        setIndex(() => mailData?.findIndex((x) => x.language === language));
    }, [mailData]);
    const classes = useStyles();
    const handleChangeTaTMail = (event) => {
        const eventValue = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
        if (!currentLanguageData) {
            const newMailData = mailData ? [...mailData] as any : [];
            newMailData.push({
                [event.target.name]: eventValue,
                language,
            });

            setMailData(newMailData);
        } else {
            const newMailData = [...mailData];
            const copyOverObject = {
                ...currentLanguageData,
                [event.target.name]: eventValue,
            };
            newMailData[index] = copyOverObject;
            setMailData(newMailData);
        }
    };

    return (
        <div className={classes.root}>
            <Typography variant="subtitle1">In Transit E-Mail</Typography>
            <Typography>This E-Mail gets triggered when the parcel is first scanned. In most cases when arriving at the origin parcel centre</Typography>
            <FormControlLabel
                control={(
                    <Checkbox
                        key="inTransitEmailenabled"
                        checked={currentLanguageData?.inTransitEmailEnabled || false}
                        name="inTransitEmailEnabled"
                        onChange={handleChangeTaTMail}
                    />
                )}
                label="Active"
            />
            <CardSpacer />
            <TextField
                key="inTransitEmailSubject"
                fullWidth
                label="E-Mail Subject"
                name="inTransitEmailSubject"
                variant="outlined"
                value={currentLanguageData?.inTransitEmailSubject || ''}
                onChange={handleChangeTaTMail}
            />
            <CardSpacer />
            <TextField
                key="inTransitEmailTemplateName"
                fullWidth
                label="Mailgun Template Name"
                name="inTransitEmailTemplateName"
                variant="outlined"
                value={currentLanguageData?.inTransitEmailTemplateName || ''}
                onChange={handleChangeTaTMail}
            />
            <CardSpacer />
            <Divider />
            <CardSpacer />
            <Typography variant="subtitle1">Out For Delivery E-Mail</Typography>
            <Typography>This E-Mail gets triggered when the parcel gets delivered soon</Typography>
            <FormControlLabel
                control={(
                    <Checkbox
                        key="outForDeliveryEmailEnabled"
                        checked={currentLanguageData?.outForDeliveryEmailEnabled || false}
                        name="outForDeliveryEmailEnabled"
                        onChange={handleChangeTaTMail}
                    />
                )}
                label="Active"
            />
            <CardSpacer />
            <TextField
                key="outForDeliveryEmailSubject"
                fullWidth
                label="E-Mail Subject"
                name="outForDeliveryEmailSubject"
                variant="outlined"
                value={currentLanguageData?.outForDeliveryEmailSubject || ''}
                onChange={handleChangeTaTMail}
            />
            <CardSpacer />
            <TextField
                key="outfordelivery-mailgun-template"
                fullWidth
                label="Mailgun Template Name"
                name="outForDeliveryEmailTemplateName"
                variant="outlined"
                value={currentLanguageData?.outForDeliveryEmailTemplateName || ''}
                onChange={handleChangeTaTMail}
            />
            <CardSpacer />
        </div>

    );
};
export default EmailTab;
