/* eslint-disable react/require-default-props */
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import { useState } from 'react';

const SecretInputField = ({ value, error, onChange, label, name, fullWidth = true, autocomplete = 'off' }:
{ value, error? :boolean, onChange, label:string, name:string, fullWidth? :boolean, autocomplete? :string }) => {
    const [showPassword, setShowPassword] = useState(false);
    const handleClickShowPassword = () => {
        setShowPassword(!showPassword);
    };
    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };
    return (
        <FormControl variant="outlined" fullWidth>
            <InputLabel htmlFor={`input-${label}`} error={error}>{label}</InputLabel>
            <OutlinedInput
                fullWidth={fullWidth}
                id={`input-${label}`}
                value={value}
                error={error}
                name={name}
                onChange={onChange}
                type={showPassword ? 'text' : 'password'}
                label={label}
                autoComplete={autocomplete}
                endAdornment={(
                    <InputAdornment position="end">
                        <IconButton
                            aria-label="toggle password visibility"
                            onClick={handleClickShowPassword}
                            onMouseDown={handleMouseDownPassword}
                            edge="end"
                        >
                            {showPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                    </InputAdornment>
                )}
            />
        </FormControl>
    );
};
export default SecretInputField;
