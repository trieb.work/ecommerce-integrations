/* eslint-disable arrow-body-style */
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme) => ({
    formControl: {
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

type CustomMenuItem = {
    value :any,
    name :string,
};

const CustomSelect = ({ menuItems, activeMenuItem, handleChange }: {menuItems :CustomMenuItem[], activeMenuItem :CustomMenuItem, handleChange :(event: any) => void}) => {
    const classes = useStyles();

    return (
        <FormControl variant="outlined" className={classes.formControl}>
            <InputLabel id="simple-select-outlined-label">Channel</InputLabel>
            <Select
                labelId="simple-select-outlined-label"
                id="simple-select-outlined"
                value={activeMenuItem ? activeMenuItem?.value : ' '}
                onChange={handleChange}
                label="Channel"
            >
                <MenuItem value="">
                    <em>None</em>
                </MenuItem>
                {
                    menuItems?.length > 0 && menuItems?.map((item) => (
                        <MenuItem key={item?.name} value={item?.value}>{item.name}</MenuItem>
                    ))
                }
            </Select>
        </FormControl>
    );
};
export default CustomSelect;
