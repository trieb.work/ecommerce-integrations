const Unauthorized = () => (
    <div>You are not authorized to access the App Configuration! If you think this is an error, please contact the support</div>
);
export default Unauthorized;
