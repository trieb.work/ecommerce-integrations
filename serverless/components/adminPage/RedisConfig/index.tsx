import { AppConfigUpdateInput } from 'gqlTypes/eciTypes';
import { useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CardSpacer from 'components/saleor/CardSpacer';
import Button from '@material-ui/core/Button';
import { RedisConf } from '@prisma/client';

const RedisConfigCard = ({ initialData, handleSubmit, loading }
:{ loading :boolean, initialData :RedisConf, handleSubmit :(variables: AppConfigUpdateInput) => Promise<void> }) => {
    const [data, setData] = useState<RedisConf>(initialData);

    useEffect(() => {
        setData(initialData);
    }, [initialData]);

    const handleChange = (event) => {
        const eventValue = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
        setData({ ...data, [event.target.name]: eventValue });
    };

    const updateOrUpsert = data.id ? {
        redis: {
            update: {
                host: {
                    set: data.host || '',
                },
                port: {
                    set: data.port || '',
                },
                password: {
                    set: data.password || '',
                },
            } },
    } : {
        redis: {
            create: {
                active: data.host || false,
                orgId: data.port,
                clientId: data.password,
            },
        },
    };

    return (
        <Card variant="outlined">
            <CardHeader title="Redis Settings" />
            <Divider />
            <CardContent>
                <form noValidate autoComplete="off">
                    <CardSpacer />
                    <TextField
                        fullWidth
                        id="zoho.org-id"
                        label="Org-Id"
                        value={data.host}
                        name="orgId"
                        variant="outlined"
                        onChange={handleChange}
                    />
                    <CardSpacer />
                    <TextField
                        fullWidth
                        id="outlined-basic"
                        label="Client-Id"
                        name="clientId"
                        value={data.port}
                        variant="outlined"
                        onChange={handleChange}
                    />
                    <CardSpacer />
                    <TextField
                        fullWidth
                        id="outlined-basic"
                        label="Client-Secret"
                        value={data.password}
                        name="clientSecret"
                        onChange={handleChange}
                        variant="outlined"
                    />
                </form>
                <CardSpacer />
                <Button
                    variant="contained"
                    disabled={loading}
                    // onClick={() => handleSubmit(
                    //     updateOrUpsert,
                    // )}
                >
                    Save
                </Button>
            </CardContent>
        </Card>
    );
};
export default RedisConfigCard;
