import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        type: 'dark',
        background: {
            default: 'initial',
        },
    },
});

export default theme;
