import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        type: 'light',
        background: {
            default: 'initial',
        },
    },
});

export default theme;
