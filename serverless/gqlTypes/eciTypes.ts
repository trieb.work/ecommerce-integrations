export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Query = {
  __typename?: 'Query';
  /** Get a users AppConfig */
  getAppConfig?: Maybe<AppConfig>;
  /** Get data from the origin Saleor instance - like Channels */
  getSaleorData?: Maybe<SaleorDataClass>;
};

export type AppConfig = {
  __typename?: 'AppConfig';
  id: Scalars['Int'];
  active?: Maybe<Scalars['Boolean']>;
  baseUrl?: Maybe<Scalars['String']>;
  saleor?: Maybe<SaleorConf>;
  zoho?: Maybe<ZohoConf>;
  productdatafeed?: Maybe<ProductDataFeed>;
  mailchimp?: Maybe<MailchimpConf>;
  easypost?: Maybe<EasyPostConf>;
  braintree?: Maybe<BraintreeConf>;
  lexoffice?: Maybe<LexofficeConf>;
  trackandtrace?: Maybe<TrackAndTraceConf>;
  mailgun?: Maybe<MailgunConf>;
};

export type SaleorConf = {
  __typename?: 'SaleorConf';
  id: Scalars['Int'];
  domain?: Maybe<Scalars['String']>;
  appId?: Maybe<Scalars['String']>;
  authToken?: Maybe<Scalars['String']>;
  webhookToken: Scalars['String'];
  webhookID?: Maybe<Scalars['String']>;
  appConfigId: Scalars['Int'];
};

export type ZohoConf = {
  __typename?: 'ZohoConf';
  id: Scalars['Int'];
  active?: Maybe<Scalars['Boolean']>;
  cuid: Scalars['String'];
  orgId?: Maybe<Scalars['String']>;
  clientId?: Maybe<Scalars['String']>;
  clientSecret?: Maybe<Scalars['String']>;
  payPalAccountId?: Maybe<Scalars['String']>;
  creditCardAccountId?: Maybe<Scalars['String']>;
  webhookToken: Scalars['String'];
  webhookID?: Maybe<Scalars['String']>;
  customFunctionID?: Maybe<Scalars['String']>;
  appConfigId: Scalars['Int'];
};

export type ProductDataFeed = {
  __typename?: 'ProductDataFeed';
  id: Scalars['Int'];
  cuid: Scalars['String'];
  active?: Maybe<Scalars['Boolean']>;
  productDetailStorefrontURL?: Maybe<Scalars['String']>;
  channel?: Maybe<Scalars['String']>;
  appConfigId: Scalars['Int'];
};

export type MailchimpConf = {
  __typename?: 'MailchimpConf';
  id: Scalars['Int'];
  active?: Maybe<Scalars['Boolean']>;
  listId?: Maybe<Scalars['String']>;
  apiToken?: Maybe<Scalars['String']>;
  storeUrl?: Maybe<Scalars['String']>;
  storeId?: Maybe<Scalars['String']>;
  productUrlPattern?: Maybe<Scalars['String']>;
  channel?: Maybe<Scalars['String']>;
  appConfigId?: Maybe<Scalars['Int']>;
};

export type EasyPostConf = {
  __typename?: 'EasyPostConf';
  id: Scalars['Int'];
  cuid: Scalars['String'];
  active?: Maybe<Scalars['Boolean']>;
  apiToken?: Maybe<Scalars['String']>;
  webhookID?: Maybe<Scalars['String']>;
  appConfigId: Scalars['Int'];
  trackAndTraceConfigId?: Maybe<Scalars['Int']>;
};

export type BraintreeConf = {
  __typename?: 'BraintreeConf';
  id: Scalars['Int'];
  active: Scalars['Boolean'];
  merchantId?: Maybe<Scalars['String']>;
  publicKey?: Maybe<Scalars['String']>;
  privateKey?: Maybe<Scalars['String']>;
  appConfigId: Scalars['Int'];
};

export type LexofficeConf = {
  __typename?: 'LexofficeConf';
  id: Scalars['Int'];
  active: Scalars['Boolean'];
  apiToken?: Maybe<Scalars['String']>;
  appConfigId: Scalars['Int'];
};

export type TrackAndTraceConf = {
  __typename?: 'TrackAndTraceConf';
  id: Scalars['Int'];
  active?: Maybe<Scalars['Boolean']>;
  cuid: Scalars['String'];
  appConfigId: Scalars['Int'];
  mailgun?: Maybe<MailgunConf>;
  easypost?: Maybe<EasyPostConf>;
  appConfig: AppConfig;
  trackAndTraceEmails: Array<TrackAndTraceEmails>;
};


export type TrackAndTraceConfTrackAndTraceEmailsArgs = {
  where?: Maybe<TrackAndTraceEmailsWhereInput>;
  orderBy?: Maybe<Array<TrackAndTraceEmailsOrderByInput>>;
  cursor?: Maybe<TrackAndTraceEmailsWhereUniqueInput>;
  take?: Maybe<Scalars['Int']>;
  skip?: Maybe<Scalars['Int']>;
  distinct?: Maybe<Array<TrackAndTraceEmailsScalarFieldEnum>>;
};

export type MailgunConf = {
  __typename?: 'MailgunConf';
  id: Scalars['Int'];
  cuid?: Maybe<Scalars['String']>;
  apiToken: Scalars['String'];
  fromDomain: Scalars['String'];
  fromTitle?: Maybe<Scalars['String']>;
  fromEmail?: Maybe<Scalars['String']>;
  apiHost: Scalars['String'];
  appConfigId?: Maybe<Scalars['Int']>;
  trackAndTraceConfigId?: Maybe<Scalars['Int']>;
};

export type TrackAndTraceEmails = {
  __typename?: 'TrackAndTraceEmails';
  id: Scalars['Int'];
  language: Language;
  inTransitEmailEnabled?: Maybe<Scalars['Boolean']>;
  inTransitEmailSubject?: Maybe<Scalars['String']>;
  inTransitEmailTemplateName?: Maybe<Scalars['String']>;
  outForDeliveryEmailEnabled?: Maybe<Scalars['Boolean']>;
  outForDeliveryEmailTemplateName?: Maybe<Scalars['String']>;
  outForDeliveryEmailSubject?: Maybe<Scalars['String']>;
  trackAndTraceConfId: Scalars['Int'];
};

export enum Language {
  De = 'DE',
  En = 'EN'
}

export type TrackAndTraceEmailsWhereInput = {
  AND?: Maybe<Array<TrackAndTraceEmailsWhereInput>>;
  OR?: Maybe<Array<TrackAndTraceEmailsWhereInput>>;
  NOT?: Maybe<Array<TrackAndTraceEmailsWhereInput>>;
  id?: Maybe<IntFilter>;
  language?: Maybe<EnumLanguageFilter>;
  inTransitEmailEnabled?: Maybe<BoolNullableFilter>;
  inTransitEmailSubject?: Maybe<StringNullableFilter>;
  inTransitEmailTemplateName?: Maybe<StringNullableFilter>;
  outForDeliveryEmailEnabled?: Maybe<BoolNullableFilter>;
  outForDeliveryEmailTemplateName?: Maybe<StringNullableFilter>;
  outForDeliveryEmailSubject?: Maybe<StringNullableFilter>;
  trackAndTraceConf?: Maybe<TrackAndTraceConfRelationFilter>;
  trackAndTraceConfId?: Maybe<IntFilter>;
};

export type IntFilter = {
  equals?: Maybe<Scalars['Int']>;
  in?: Maybe<Array<Scalars['Int']>>;
  notIn?: Maybe<Array<Scalars['Int']>>;
  lt?: Maybe<Scalars['Int']>;
  lte?: Maybe<Scalars['Int']>;
  gt?: Maybe<Scalars['Int']>;
  gte?: Maybe<Scalars['Int']>;
  not?: Maybe<NestedIntFilter>;
};

export type NestedIntFilter = {
  equals?: Maybe<Scalars['Int']>;
  in?: Maybe<Array<Scalars['Int']>>;
  notIn?: Maybe<Array<Scalars['Int']>>;
  lt?: Maybe<Scalars['Int']>;
  lte?: Maybe<Scalars['Int']>;
  gt?: Maybe<Scalars['Int']>;
  gte?: Maybe<Scalars['Int']>;
  not?: Maybe<NestedIntFilter>;
};

export type EnumLanguageFilter = {
  equals?: Maybe<Language>;
  in?: Maybe<Array<Language>>;
  notIn?: Maybe<Array<Language>>;
  not?: Maybe<NestedEnumLanguageFilter>;
};

export type NestedEnumLanguageFilter = {
  equals?: Maybe<Language>;
  in?: Maybe<Array<Language>>;
  notIn?: Maybe<Array<Language>>;
  not?: Maybe<NestedEnumLanguageFilter>;
};

export type BoolNullableFilter = {
  equals?: Maybe<Scalars['Boolean']>;
  not?: Maybe<NestedBoolNullableFilter>;
};

export type NestedBoolNullableFilter = {
  equals?: Maybe<Scalars['Boolean']>;
  not?: Maybe<NestedBoolNullableFilter>;
};

export type StringNullableFilter = {
  equals?: Maybe<Scalars['String']>;
  in?: Maybe<Array<Scalars['String']>>;
  notIn?: Maybe<Array<Scalars['String']>>;
  lt?: Maybe<Scalars['String']>;
  lte?: Maybe<Scalars['String']>;
  gt?: Maybe<Scalars['String']>;
  gte?: Maybe<Scalars['String']>;
  contains?: Maybe<Scalars['String']>;
  startsWith?: Maybe<Scalars['String']>;
  endsWith?: Maybe<Scalars['String']>;
  mode?: Maybe<QueryMode>;
  not?: Maybe<NestedStringNullableFilter>;
};

export enum QueryMode {
  Default = 'default',
  Insensitive = 'insensitive'
}

export type NestedStringNullableFilter = {
  equals?: Maybe<Scalars['String']>;
  in?: Maybe<Array<Scalars['String']>>;
  notIn?: Maybe<Array<Scalars['String']>>;
  lt?: Maybe<Scalars['String']>;
  lte?: Maybe<Scalars['String']>;
  gt?: Maybe<Scalars['String']>;
  gte?: Maybe<Scalars['String']>;
  contains?: Maybe<Scalars['String']>;
  startsWith?: Maybe<Scalars['String']>;
  endsWith?: Maybe<Scalars['String']>;
  not?: Maybe<NestedStringNullableFilter>;
};

export type TrackAndTraceConfRelationFilter = {
  is?: Maybe<TrackAndTraceConfWhereInput>;
  isNot?: Maybe<TrackAndTraceConfWhereInput>;
};

export type TrackAndTraceConfWhereInput = {
  AND?: Maybe<Array<TrackAndTraceConfWhereInput>>;
  OR?: Maybe<Array<TrackAndTraceConfWhereInput>>;
  NOT?: Maybe<Array<TrackAndTraceConfWhereInput>>;
  id?: Maybe<IntFilter>;
  active?: Maybe<BoolNullableFilter>;
  cuid?: Maybe<StringFilter>;
  mailgun?: Maybe<MailgunConfRelationFilter>;
  easypost?: Maybe<EasyPostConfRelationFilter>;
  appConfig?: Maybe<AppConfigRelationFilter>;
  appConfigId?: Maybe<IntFilter>;
  trackAndTraceEmails?: Maybe<TrackAndTraceEmailsListRelationFilter>;
};

export type StringFilter = {
  equals?: Maybe<Scalars['String']>;
  in?: Maybe<Array<Scalars['String']>>;
  notIn?: Maybe<Array<Scalars['String']>>;
  lt?: Maybe<Scalars['String']>;
  lte?: Maybe<Scalars['String']>;
  gt?: Maybe<Scalars['String']>;
  gte?: Maybe<Scalars['String']>;
  contains?: Maybe<Scalars['String']>;
  startsWith?: Maybe<Scalars['String']>;
  endsWith?: Maybe<Scalars['String']>;
  mode?: Maybe<QueryMode>;
  not?: Maybe<NestedStringFilter>;
};

export type NestedStringFilter = {
  equals?: Maybe<Scalars['String']>;
  in?: Maybe<Array<Scalars['String']>>;
  notIn?: Maybe<Array<Scalars['String']>>;
  lt?: Maybe<Scalars['String']>;
  lte?: Maybe<Scalars['String']>;
  gt?: Maybe<Scalars['String']>;
  gte?: Maybe<Scalars['String']>;
  contains?: Maybe<Scalars['String']>;
  startsWith?: Maybe<Scalars['String']>;
  endsWith?: Maybe<Scalars['String']>;
  not?: Maybe<NestedStringFilter>;
};

export type MailgunConfRelationFilter = {
  is?: Maybe<MailgunConfWhereInput>;
  isNot?: Maybe<MailgunConfWhereInput>;
};

export type MailgunConfWhereInput = {
  AND?: Maybe<Array<MailgunConfWhereInput>>;
  OR?: Maybe<Array<MailgunConfWhereInput>>;
  NOT?: Maybe<Array<MailgunConfWhereInput>>;
  id?: Maybe<IntFilter>;
  cuid?: Maybe<StringNullableFilter>;
  apiToken?: Maybe<StringFilter>;
  fromDomain?: Maybe<StringFilter>;
  fromTitle?: Maybe<StringNullableFilter>;
  fromEmail?: Maybe<StringNullableFilter>;
  apiHost?: Maybe<StringFilter>;
  appConfig?: Maybe<AppConfigRelationFilter>;
  appConfigId?: Maybe<IntNullableFilter>;
  trackAndTraceConfig?: Maybe<TrackAndTraceConfRelationFilter>;
  trackAndTraceConfigId?: Maybe<IntNullableFilter>;
};

export type AppConfigRelationFilter = {
  is?: Maybe<AppConfigWhereInput>;
  isNot?: Maybe<AppConfigWhereInput>;
};

export type AppConfigWhereInput = {
  AND?: Maybe<Array<AppConfigWhereInput>>;
  OR?: Maybe<Array<AppConfigWhereInput>>;
  NOT?: Maybe<Array<AppConfigWhereInput>>;
  id?: Maybe<IntFilter>;
  active?: Maybe<BoolNullableFilter>;
  baseUrl?: Maybe<StringNullableFilter>;
  saleor?: Maybe<SaleorConfRelationFilter>;
  zoho?: Maybe<ZohoConfRelationFilter>;
  productdatafeed?: Maybe<ProductDataFeedRelationFilter>;
  mailchimp?: Maybe<MailchimpConfRelationFilter>;
  easypost?: Maybe<EasyPostConfRelationFilter>;
  braintree?: Maybe<BraintreeConfRelationFilter>;
  lexoffice?: Maybe<LexofficeConfRelationFilter>;
  trackandtrace?: Maybe<TrackAndTraceConfRelationFilter>;
  mailgun?: Maybe<MailgunConfRelationFilter>;
};

export type SaleorConfRelationFilter = {
  is?: Maybe<SaleorConfWhereInput>;
  isNot?: Maybe<SaleorConfWhereInput>;
};

export type SaleorConfWhereInput = {
  AND?: Maybe<Array<SaleorConfWhereInput>>;
  OR?: Maybe<Array<SaleorConfWhereInput>>;
  NOT?: Maybe<Array<SaleorConfWhereInput>>;
  id?: Maybe<IntFilter>;
  domain?: Maybe<StringNullableFilter>;
  appId?: Maybe<StringNullableFilter>;
  authToken?: Maybe<StringNullableFilter>;
  webhookToken?: Maybe<StringFilter>;
  webhookID?: Maybe<StringNullableFilter>;
  appConfig?: Maybe<AppConfigRelationFilter>;
  appConfigId?: Maybe<IntFilter>;
};

export type ZohoConfRelationFilter = {
  is?: Maybe<ZohoConfWhereInput>;
  isNot?: Maybe<ZohoConfWhereInput>;
};

export type ZohoConfWhereInput = {
  AND?: Maybe<Array<ZohoConfWhereInput>>;
  OR?: Maybe<Array<ZohoConfWhereInput>>;
  NOT?: Maybe<Array<ZohoConfWhereInput>>;
  id?: Maybe<IntFilter>;
  active?: Maybe<BoolNullableFilter>;
  cuid?: Maybe<StringFilter>;
  orgId?: Maybe<StringNullableFilter>;
  clientId?: Maybe<StringNullableFilter>;
  clientSecret?: Maybe<StringNullableFilter>;
  payPalAccountId?: Maybe<StringNullableFilter>;
  creditCardAccountId?: Maybe<StringNullableFilter>;
  webhookToken?: Maybe<StringFilter>;
  webhookID?: Maybe<StringNullableFilter>;
  customFunctionID?: Maybe<StringNullableFilter>;
  appConfig?: Maybe<AppConfigRelationFilter>;
  appConfigId?: Maybe<IntFilter>;
};

export type ProductDataFeedRelationFilter = {
  is?: Maybe<ProductDataFeedWhereInput>;
  isNot?: Maybe<ProductDataFeedWhereInput>;
};

export type ProductDataFeedWhereInput = {
  AND?: Maybe<Array<ProductDataFeedWhereInput>>;
  OR?: Maybe<Array<ProductDataFeedWhereInput>>;
  NOT?: Maybe<Array<ProductDataFeedWhereInput>>;
  id?: Maybe<IntFilter>;
  cuid?: Maybe<StringFilter>;
  active?: Maybe<BoolNullableFilter>;
  productDetailStorefrontURL?: Maybe<StringNullableFilter>;
  channel?: Maybe<StringNullableFilter>;
  appConfig?: Maybe<AppConfigRelationFilter>;
  appConfigId?: Maybe<IntFilter>;
};

export type MailchimpConfRelationFilter = {
  is?: Maybe<MailchimpConfWhereInput>;
  isNot?: Maybe<MailchimpConfWhereInput>;
};

export type MailchimpConfWhereInput = {
  AND?: Maybe<Array<MailchimpConfWhereInput>>;
  OR?: Maybe<Array<MailchimpConfWhereInput>>;
  NOT?: Maybe<Array<MailchimpConfWhereInput>>;
  id?: Maybe<IntFilter>;
  active?: Maybe<BoolNullableFilter>;
  listId?: Maybe<StringNullableFilter>;
  apiToken?: Maybe<StringNullableFilter>;
  storeUrl?: Maybe<StringNullableFilter>;
  storeId?: Maybe<StringNullableFilter>;
  productUrlPattern?: Maybe<StringNullableFilter>;
  channel?: Maybe<StringNullableFilter>;
  appConfig?: Maybe<AppConfigRelationFilter>;
  appConfigId?: Maybe<IntNullableFilter>;
};

export type IntNullableFilter = {
  equals?: Maybe<Scalars['Int']>;
  in?: Maybe<Array<Scalars['Int']>>;
  notIn?: Maybe<Array<Scalars['Int']>>;
  lt?: Maybe<Scalars['Int']>;
  lte?: Maybe<Scalars['Int']>;
  gt?: Maybe<Scalars['Int']>;
  gte?: Maybe<Scalars['Int']>;
  not?: Maybe<NestedIntNullableFilter>;
};

export type NestedIntNullableFilter = {
  equals?: Maybe<Scalars['Int']>;
  in?: Maybe<Array<Scalars['Int']>>;
  notIn?: Maybe<Array<Scalars['Int']>>;
  lt?: Maybe<Scalars['Int']>;
  lte?: Maybe<Scalars['Int']>;
  gt?: Maybe<Scalars['Int']>;
  gte?: Maybe<Scalars['Int']>;
  not?: Maybe<NestedIntNullableFilter>;
};

export type EasyPostConfRelationFilter = {
  is?: Maybe<EasyPostConfWhereInput>;
  isNot?: Maybe<EasyPostConfWhereInput>;
};

export type EasyPostConfWhereInput = {
  AND?: Maybe<Array<EasyPostConfWhereInput>>;
  OR?: Maybe<Array<EasyPostConfWhereInput>>;
  NOT?: Maybe<Array<EasyPostConfWhereInput>>;
  id?: Maybe<IntFilter>;
  cuid?: Maybe<StringFilter>;
  active?: Maybe<BoolNullableFilter>;
  apiToken?: Maybe<StringNullableFilter>;
  webhookID?: Maybe<StringNullableFilter>;
  appConfig?: Maybe<AppConfigRelationFilter>;
  appConfigId?: Maybe<IntFilter>;
  trackAndTraceConfig?: Maybe<TrackAndTraceConfRelationFilter>;
  trackAndTraceConfigId?: Maybe<IntNullableFilter>;
  AddressVerificationConf?: Maybe<AddressVerificationConfRelationFilter>;
};

export type AddressVerificationConfRelationFilter = {
  is?: Maybe<AddressVerificationConfWhereInput>;
  isNot?: Maybe<AddressVerificationConfWhereInput>;
};

export type AddressVerificationConfWhereInput = {
  AND?: Maybe<Array<AddressVerificationConfWhereInput>>;
  OR?: Maybe<Array<AddressVerificationConfWhereInput>>;
  NOT?: Maybe<Array<AddressVerificationConfWhereInput>>;
  id?: Maybe<IntFilter>;
  active?: Maybe<BoolNullableFilter>;
  easypost?: Maybe<EasyPostConfRelationFilter>;
  easyPostConfId?: Maybe<IntFilter>;
};

export type BraintreeConfRelationFilter = {
  is?: Maybe<BraintreeConfWhereInput>;
  isNot?: Maybe<BraintreeConfWhereInput>;
};

export type BraintreeConfWhereInput = {
  AND?: Maybe<Array<BraintreeConfWhereInput>>;
  OR?: Maybe<Array<BraintreeConfWhereInput>>;
  NOT?: Maybe<Array<BraintreeConfWhereInput>>;
  id?: Maybe<IntFilter>;
  active?: Maybe<BoolFilter>;
  merchantId?: Maybe<StringNullableFilter>;
  publicKey?: Maybe<StringNullableFilter>;
  privateKey?: Maybe<StringNullableFilter>;
  appConfig?: Maybe<AppConfigRelationFilter>;
  appConfigId?: Maybe<IntFilter>;
};

export type BoolFilter = {
  equals?: Maybe<Scalars['Boolean']>;
  not?: Maybe<NestedBoolFilter>;
};

export type NestedBoolFilter = {
  equals?: Maybe<Scalars['Boolean']>;
  not?: Maybe<NestedBoolFilter>;
};

export type LexofficeConfRelationFilter = {
  is?: Maybe<LexofficeConfWhereInput>;
  isNot?: Maybe<LexofficeConfWhereInput>;
};

export type LexofficeConfWhereInput = {
  AND?: Maybe<Array<LexofficeConfWhereInput>>;
  OR?: Maybe<Array<LexofficeConfWhereInput>>;
  NOT?: Maybe<Array<LexofficeConfWhereInput>>;
  id?: Maybe<IntFilter>;
  active?: Maybe<BoolFilter>;
  apiToken?: Maybe<StringNullableFilter>;
  appConfig?: Maybe<AppConfigRelationFilter>;
  appConfigId?: Maybe<IntFilter>;
};

export type TrackAndTraceEmailsListRelationFilter = {
  every?: Maybe<TrackAndTraceEmailsWhereInput>;
  some?: Maybe<TrackAndTraceEmailsWhereInput>;
  none?: Maybe<TrackAndTraceEmailsWhereInput>;
};

export type TrackAndTraceEmailsOrderByInput = {
  id?: Maybe<SortOrder>;
  language?: Maybe<SortOrder>;
  inTransitEmailEnabled?: Maybe<SortOrder>;
  inTransitEmailSubject?: Maybe<SortOrder>;
  inTransitEmailTemplateName?: Maybe<SortOrder>;
  outForDeliveryEmailEnabled?: Maybe<SortOrder>;
  outForDeliveryEmailTemplateName?: Maybe<SortOrder>;
  outForDeliveryEmailSubject?: Maybe<SortOrder>;
  trackAndTraceConfId?: Maybe<SortOrder>;
};

export enum SortOrder {
  Asc = 'asc',
  Desc = 'desc'
}

export type TrackAndTraceEmailsWhereUniqueInput = {
  id?: Maybe<Scalars['Int']>;
  language?: Maybe<Language>;
};

export enum TrackAndTraceEmailsScalarFieldEnum {
  Id = 'id',
  Language = 'language',
  InTransitEmailEnabled = 'inTransitEmailEnabled',
  InTransitEmailSubject = 'inTransitEmailSubject',
  InTransitEmailTemplateName = 'inTransitEmailTemplateName',
  OutForDeliveryEmailEnabled = 'outForDeliveryEmailEnabled',
  OutForDeliveryEmailTemplateName = 'outForDeliveryEmailTemplateName',
  OutForDeliveryEmailSubject = 'outForDeliveryEmailSubject',
  TrackAndTraceConfId = 'trackAndTraceConfId'
}

export type SaleorDataClass = {
  __typename?: 'SaleorDataClass';
  channels: Array<Channel>;
};

export type Channel = {
  __typename?: 'Channel';
  id: Scalars['String'];
  slug: Scalars['String'];
  isActive: Scalars['Boolean'];
  name: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  updateAppConfig: AppConfig;
};


export type MutationUpdateAppConfigArgs = {
  id: Scalars['ID'];
  AppConfigInputData: AppConfigUpdateInput;
};

export type AppConfigUpdateInput = {
  active?: Maybe<NullableBoolFieldUpdateOperationsInput>;
  baseUrl?: Maybe<NullableStringFieldUpdateOperationsInput>;
  saleor?: Maybe<SaleorConfUpdateOneWithoutAppConfigInput>;
  zoho?: Maybe<ZohoConfUpdateOneWithoutAppConfigInput>;
  productdatafeed?: Maybe<ProductDataFeedUpdateOneWithoutAppConfigInput>;
  mailchimp?: Maybe<MailchimpConfUpdateOneWithoutAppConfigInput>;
  easypost?: Maybe<EasyPostConfUpdateOneWithoutAppConfigInput>;
  braintree?: Maybe<BraintreeConfUpdateOneWithoutAppConfigInput>;
  lexoffice?: Maybe<LexofficeConfUpdateOneWithoutAppConfigInput>;
  trackandtrace?: Maybe<TrackAndTraceConfUpdateOneWithoutAppConfigInput>;
  mailgun?: Maybe<MailgunConfUpdateOneWithoutAppConfigInput>;
};

export type NullableBoolFieldUpdateOperationsInput = {
  set?: Maybe<Scalars['Boolean']>;
};

export type NullableStringFieldUpdateOperationsInput = {
  set?: Maybe<Scalars['String']>;
};

export type SaleorConfUpdateOneWithoutAppConfigInput = {
  create?: Maybe<SaleorConfCreateWithoutAppConfigInput>;
  connectOrCreate?: Maybe<SaleorConfCreateOrConnectWithoutAppConfigInput>;
  upsert?: Maybe<SaleorConfUpsertWithoutAppConfigInput>;
  connect?: Maybe<SaleorConfWhereUniqueInput>;
  disconnect?: Maybe<Scalars['Boolean']>;
  delete?: Maybe<Scalars['Boolean']>;
  update?: Maybe<SaleorConfUpdateWithoutAppConfigInput>;
};

export type SaleorConfCreateWithoutAppConfigInput = {
  domain?: Maybe<Scalars['String']>;
  appId?: Maybe<Scalars['String']>;
  authToken?: Maybe<Scalars['String']>;
  webhookToken?: Maybe<Scalars['String']>;
  webhookID?: Maybe<Scalars['String']>;
};

export type SaleorConfCreateOrConnectWithoutAppConfigInput = {
  where: SaleorConfWhereUniqueInput;
  create: SaleorConfCreateWithoutAppConfigInput;
};

export type SaleorConfWhereUniqueInput = {
  id?: Maybe<Scalars['Int']>;
  domain?: Maybe<Scalars['String']>;
  appConfigId?: Maybe<Scalars['Int']>;
};

export type SaleorConfUpsertWithoutAppConfigInput = {
  update: SaleorConfUpdateWithoutAppConfigInput;
  create: SaleorConfCreateWithoutAppConfigInput;
};

export type SaleorConfUpdateWithoutAppConfigInput = {
  domain?: Maybe<NullableStringFieldUpdateOperationsInput>;
  appId?: Maybe<NullableStringFieldUpdateOperationsInput>;
  authToken?: Maybe<NullableStringFieldUpdateOperationsInput>;
  webhookToken?: Maybe<StringFieldUpdateOperationsInput>;
  webhookID?: Maybe<NullableStringFieldUpdateOperationsInput>;
};

export type StringFieldUpdateOperationsInput = {
  set?: Maybe<Scalars['String']>;
};

export type ZohoConfUpdateOneWithoutAppConfigInput = {
  create?: Maybe<ZohoConfCreateWithoutAppConfigInput>;
  connectOrCreate?: Maybe<ZohoConfCreateOrConnectWithoutAppConfigInput>;
  upsert?: Maybe<ZohoConfUpsertWithoutAppConfigInput>;
  connect?: Maybe<ZohoConfWhereUniqueInput>;
  disconnect?: Maybe<Scalars['Boolean']>;
  delete?: Maybe<Scalars['Boolean']>;
  update?: Maybe<ZohoConfUpdateWithoutAppConfigInput>;
};

export type ZohoConfCreateWithoutAppConfigInput = {
  active?: Maybe<Scalars['Boolean']>;
  cuid?: Maybe<Scalars['String']>;
  orgId?: Maybe<Scalars['String']>;
  clientId?: Maybe<Scalars['String']>;
  clientSecret?: Maybe<Scalars['String']>;
  payPalAccountId?: Maybe<Scalars['String']>;
  creditCardAccountId?: Maybe<Scalars['String']>;
  webhookToken?: Maybe<Scalars['String']>;
  webhookID?: Maybe<Scalars['String']>;
  customFunctionID?: Maybe<Scalars['String']>;
};

export type ZohoConfCreateOrConnectWithoutAppConfigInput = {
  where: ZohoConfWhereUniqueInput;
  create: ZohoConfCreateWithoutAppConfigInput;
};

export type ZohoConfWhereUniqueInput = {
  id?: Maybe<Scalars['Int']>;
  appConfigId?: Maybe<Scalars['Int']>;
};

export type ZohoConfUpsertWithoutAppConfigInput = {
  update: ZohoConfUpdateWithoutAppConfigInput;
  create: ZohoConfCreateWithoutAppConfigInput;
};

export type ZohoConfUpdateWithoutAppConfigInput = {
  active?: Maybe<NullableBoolFieldUpdateOperationsInput>;
  cuid?: Maybe<StringFieldUpdateOperationsInput>;
  orgId?: Maybe<NullableStringFieldUpdateOperationsInput>;
  clientId?: Maybe<NullableStringFieldUpdateOperationsInput>;
  clientSecret?: Maybe<NullableStringFieldUpdateOperationsInput>;
  payPalAccountId?: Maybe<NullableStringFieldUpdateOperationsInput>;
  creditCardAccountId?: Maybe<NullableStringFieldUpdateOperationsInput>;
  webhookToken?: Maybe<StringFieldUpdateOperationsInput>;
  webhookID?: Maybe<NullableStringFieldUpdateOperationsInput>;
  customFunctionID?: Maybe<NullableStringFieldUpdateOperationsInput>;
};

export type ProductDataFeedUpdateOneWithoutAppConfigInput = {
  create?: Maybe<ProductDataFeedCreateWithoutAppConfigInput>;
  connectOrCreate?: Maybe<ProductDataFeedCreateOrConnectWithoutAppConfigInput>;
  upsert?: Maybe<ProductDataFeedUpsertWithoutAppConfigInput>;
  connect?: Maybe<ProductDataFeedWhereUniqueInput>;
  disconnect?: Maybe<Scalars['Boolean']>;
  delete?: Maybe<Scalars['Boolean']>;
  update?: Maybe<ProductDataFeedUpdateWithoutAppConfigInput>;
};

export type ProductDataFeedCreateWithoutAppConfigInput = {
  cuid?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  productDetailStorefrontURL?: Maybe<Scalars['String']>;
  channel?: Maybe<Scalars['String']>;
};

export type ProductDataFeedCreateOrConnectWithoutAppConfigInput = {
  where: ProductDataFeedWhereUniqueInput;
  create: ProductDataFeedCreateWithoutAppConfigInput;
};

export type ProductDataFeedWhereUniqueInput = {
  id?: Maybe<Scalars['Int']>;
  appConfigId?: Maybe<Scalars['Int']>;
};

export type ProductDataFeedUpsertWithoutAppConfigInput = {
  update: ProductDataFeedUpdateWithoutAppConfigInput;
  create: ProductDataFeedCreateWithoutAppConfigInput;
};

export type ProductDataFeedUpdateWithoutAppConfigInput = {
  cuid?: Maybe<StringFieldUpdateOperationsInput>;
  active?: Maybe<NullableBoolFieldUpdateOperationsInput>;
  productDetailStorefrontURL?: Maybe<NullableStringFieldUpdateOperationsInput>;
  channel?: Maybe<NullableStringFieldUpdateOperationsInput>;
};

export type MailchimpConfUpdateOneWithoutAppConfigInput = {
  create?: Maybe<MailchimpConfCreateWithoutAppConfigInput>;
  connectOrCreate?: Maybe<MailchimpConfCreateOrConnectWithoutAppConfigInput>;
  upsert?: Maybe<MailchimpConfUpsertWithoutAppConfigInput>;
  connect?: Maybe<MailchimpConfWhereUniqueInput>;
  disconnect?: Maybe<Scalars['Boolean']>;
  delete?: Maybe<Scalars['Boolean']>;
  update?: Maybe<MailchimpConfUpdateWithoutAppConfigInput>;
};

export type MailchimpConfCreateWithoutAppConfigInput = {
  active?: Maybe<Scalars['Boolean']>;
  listId?: Maybe<Scalars['String']>;
  apiToken?: Maybe<Scalars['String']>;
  storeUrl?: Maybe<Scalars['String']>;
  storeId?: Maybe<Scalars['String']>;
  productUrlPattern?: Maybe<Scalars['String']>;
  channel?: Maybe<Scalars['String']>;
};

export type MailchimpConfCreateOrConnectWithoutAppConfigInput = {
  where: MailchimpConfWhereUniqueInput;
  create: MailchimpConfCreateWithoutAppConfigInput;
};

export type MailchimpConfWhereUniqueInput = {
  id?: Maybe<Scalars['Int']>;
  appConfigId?: Maybe<Scalars['Int']>;
};

export type MailchimpConfUpsertWithoutAppConfigInput = {
  update: MailchimpConfUpdateWithoutAppConfigInput;
  create: MailchimpConfCreateWithoutAppConfigInput;
};

export type MailchimpConfUpdateWithoutAppConfigInput = {
  active?: Maybe<NullableBoolFieldUpdateOperationsInput>;
  listId?: Maybe<NullableStringFieldUpdateOperationsInput>;
  apiToken?: Maybe<NullableStringFieldUpdateOperationsInput>;
  storeUrl?: Maybe<NullableStringFieldUpdateOperationsInput>;
  storeId?: Maybe<NullableStringFieldUpdateOperationsInput>;
  productUrlPattern?: Maybe<NullableStringFieldUpdateOperationsInput>;
  channel?: Maybe<NullableStringFieldUpdateOperationsInput>;
};

export type EasyPostConfUpdateOneWithoutAppConfigInput = {
  create?: Maybe<EasyPostConfCreateWithoutAppConfigInput>;
  connectOrCreate?: Maybe<EasyPostConfCreateOrConnectWithoutAppConfigInput>;
  upsert?: Maybe<EasyPostConfUpsertWithoutAppConfigInput>;
  connect?: Maybe<EasyPostConfWhereUniqueInput>;
  disconnect?: Maybe<Scalars['Boolean']>;
  delete?: Maybe<Scalars['Boolean']>;
  update?: Maybe<EasyPostConfUpdateWithoutAppConfigInput>;
};

export type EasyPostConfCreateWithoutAppConfigInput = {
  cuid?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  apiToken?: Maybe<Scalars['String']>;
  webhookID?: Maybe<Scalars['String']>;
  trackAndTraceConfig?: Maybe<TrackAndTraceConfCreateNestedOneWithoutEasypostInput>;
  AddressVerificationConf?: Maybe<AddressVerificationConfCreateNestedOneWithoutEasypostInput>;
};

export type TrackAndTraceConfCreateNestedOneWithoutEasypostInput = {
  create?: Maybe<TrackAndTraceConfCreateWithoutEasypostInput>;
  connectOrCreate?: Maybe<TrackAndTraceConfCreateOrConnectWithoutEasypostInput>;
  connect?: Maybe<TrackAndTraceConfWhereUniqueInput>;
};

export type TrackAndTraceConfCreateWithoutEasypostInput = {
  active?: Maybe<Scalars['Boolean']>;
  cuid?: Maybe<Scalars['String']>;
  mailgun?: Maybe<MailgunConfCreateNestedOneWithoutTrackAndTraceConfigInput>;
  appConfig: AppConfigCreateNestedOneWithoutTrackandtraceInput;
  trackAndTraceEmails?: Maybe<TrackAndTraceEmailsCreateNestedManyWithoutTrackAndTraceConfInput>;
};

export type MailgunConfCreateNestedOneWithoutTrackAndTraceConfigInput = {
  create?: Maybe<MailgunConfCreateWithoutTrackAndTraceConfigInput>;
  connectOrCreate?: Maybe<MailgunConfCreateOrConnectWithoutTrackAndTraceConfigInput>;
  connect?: Maybe<MailgunConfWhereUniqueInput>;
};

export type MailgunConfCreateWithoutTrackAndTraceConfigInput = {
  cuid?: Maybe<Scalars['String']>;
  apiToken: Scalars['String'];
  fromDomain: Scalars['String'];
  fromTitle?: Maybe<Scalars['String']>;
  fromEmail?: Maybe<Scalars['String']>;
  apiHost?: Maybe<Scalars['String']>;
  appConfig?: Maybe<AppConfigCreateNestedOneWithoutMailgunInput>;
};

export type AppConfigCreateNestedOneWithoutMailgunInput = {
  create?: Maybe<AppConfigCreateWithoutMailgunInput>;
  connectOrCreate?: Maybe<AppConfigCreateOrConnectWithoutMailgunInput>;
  connect?: Maybe<AppConfigWhereUniqueInput>;
};

export type AppConfigCreateWithoutMailgunInput = {
  active?: Maybe<Scalars['Boolean']>;
  baseUrl?: Maybe<Scalars['String']>;
  saleor?: Maybe<SaleorConfCreateNestedOneWithoutAppConfigInput>;
  zoho?: Maybe<ZohoConfCreateNestedOneWithoutAppConfigInput>;
  productdatafeed?: Maybe<ProductDataFeedCreateNestedOneWithoutAppConfigInput>;
  mailchimp?: Maybe<MailchimpConfCreateNestedOneWithoutAppConfigInput>;
  easypost?: Maybe<EasyPostConfCreateNestedOneWithoutAppConfigInput>;
  braintree?: Maybe<BraintreeConfCreateNestedOneWithoutAppConfigInput>;
  lexoffice?: Maybe<LexofficeConfCreateNestedOneWithoutAppConfigInput>;
  trackandtrace?: Maybe<TrackAndTraceConfCreateNestedOneWithoutAppConfigInput>;
};

export type SaleorConfCreateNestedOneWithoutAppConfigInput = {
  create?: Maybe<SaleorConfCreateWithoutAppConfigInput>;
  connectOrCreate?: Maybe<SaleorConfCreateOrConnectWithoutAppConfigInput>;
  connect?: Maybe<SaleorConfWhereUniqueInput>;
};

export type ZohoConfCreateNestedOneWithoutAppConfigInput = {
  create?: Maybe<ZohoConfCreateWithoutAppConfigInput>;
  connectOrCreate?: Maybe<ZohoConfCreateOrConnectWithoutAppConfigInput>;
  connect?: Maybe<ZohoConfWhereUniqueInput>;
};

export type ProductDataFeedCreateNestedOneWithoutAppConfigInput = {
  create?: Maybe<ProductDataFeedCreateWithoutAppConfigInput>;
  connectOrCreate?: Maybe<ProductDataFeedCreateOrConnectWithoutAppConfigInput>;
  connect?: Maybe<ProductDataFeedWhereUniqueInput>;
};

export type MailchimpConfCreateNestedOneWithoutAppConfigInput = {
  create?: Maybe<MailchimpConfCreateWithoutAppConfigInput>;
  connectOrCreate?: Maybe<MailchimpConfCreateOrConnectWithoutAppConfigInput>;
  connect?: Maybe<MailchimpConfWhereUniqueInput>;
};

export type EasyPostConfCreateNestedOneWithoutAppConfigInput = {
  create?: Maybe<EasyPostConfCreateWithoutAppConfigInput>;
  connectOrCreate?: Maybe<EasyPostConfCreateOrConnectWithoutAppConfigInput>;
  connect?: Maybe<EasyPostConfWhereUniqueInput>;
};

export type EasyPostConfCreateOrConnectWithoutAppConfigInput = {
  where: EasyPostConfWhereUniqueInput;
  create: EasyPostConfCreateWithoutAppConfigInput;
};

export type EasyPostConfWhereUniqueInput = {
  id?: Maybe<Scalars['Int']>;
  appConfigId?: Maybe<Scalars['Int']>;
  trackAndTraceConfigId?: Maybe<Scalars['Int']>;
};

export type BraintreeConfCreateNestedOneWithoutAppConfigInput = {
  create?: Maybe<BraintreeConfCreateWithoutAppConfigInput>;
  connectOrCreate?: Maybe<BraintreeConfCreateOrConnectWithoutAppConfigInput>;
  connect?: Maybe<BraintreeConfWhereUniqueInput>;
};

export type BraintreeConfCreateWithoutAppConfigInput = {
  active?: Maybe<Scalars['Boolean']>;
  merchantId?: Maybe<Scalars['String']>;
  publicKey?: Maybe<Scalars['String']>;
  privateKey?: Maybe<Scalars['String']>;
};

export type BraintreeConfCreateOrConnectWithoutAppConfigInput = {
  where: BraintreeConfWhereUniqueInput;
  create: BraintreeConfCreateWithoutAppConfigInput;
};

export type BraintreeConfWhereUniqueInput = {
  id?: Maybe<Scalars['Int']>;
  appConfigId?: Maybe<Scalars['Int']>;
};

export type LexofficeConfCreateNestedOneWithoutAppConfigInput = {
  create?: Maybe<LexofficeConfCreateWithoutAppConfigInput>;
  connectOrCreate?: Maybe<LexofficeConfCreateOrConnectWithoutAppConfigInput>;
  connect?: Maybe<LexofficeConfWhereUniqueInput>;
};

export type LexofficeConfCreateWithoutAppConfigInput = {
  active?: Maybe<Scalars['Boolean']>;
  apiToken?: Maybe<Scalars['String']>;
};

export type LexofficeConfCreateOrConnectWithoutAppConfigInput = {
  where: LexofficeConfWhereUniqueInput;
  create: LexofficeConfCreateWithoutAppConfigInput;
};

export type LexofficeConfWhereUniqueInput = {
  id?: Maybe<Scalars['Int']>;
  appConfigId?: Maybe<Scalars['Int']>;
};

export type TrackAndTraceConfCreateNestedOneWithoutAppConfigInput = {
  create?: Maybe<TrackAndTraceConfCreateWithoutAppConfigInput>;
  connectOrCreate?: Maybe<TrackAndTraceConfCreateOrConnectWithoutAppConfigInput>;
  connect?: Maybe<TrackAndTraceConfWhereUniqueInput>;
};

export type TrackAndTraceConfCreateWithoutAppConfigInput = {
  active?: Maybe<Scalars['Boolean']>;
  cuid?: Maybe<Scalars['String']>;
  mailgun?: Maybe<MailgunConfCreateNestedOneWithoutTrackAndTraceConfigInput>;
  easypost?: Maybe<EasyPostConfCreateNestedOneWithoutTrackAndTraceConfigInput>;
  trackAndTraceEmails?: Maybe<TrackAndTraceEmailsCreateNestedManyWithoutTrackAndTraceConfInput>;
};

export type EasyPostConfCreateNestedOneWithoutTrackAndTraceConfigInput = {
  create?: Maybe<EasyPostConfCreateWithoutTrackAndTraceConfigInput>;
  connectOrCreate?: Maybe<EasyPostConfCreateOrConnectWithoutTrackAndTraceConfigInput>;
  connect?: Maybe<EasyPostConfWhereUniqueInput>;
};

export type EasyPostConfCreateWithoutTrackAndTraceConfigInput = {
  cuid?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  apiToken?: Maybe<Scalars['String']>;
  webhookID?: Maybe<Scalars['String']>;
  appConfig: AppConfigCreateNestedOneWithoutEasypostInput;
  AddressVerificationConf?: Maybe<AddressVerificationConfCreateNestedOneWithoutEasypostInput>;
};

export type AppConfigCreateNestedOneWithoutEasypostInput = {
  create?: Maybe<AppConfigCreateWithoutEasypostInput>;
  connectOrCreate?: Maybe<AppConfigCreateOrConnectWithoutEasypostInput>;
  connect?: Maybe<AppConfigWhereUniqueInput>;
};

export type AppConfigCreateWithoutEasypostInput = {
  active?: Maybe<Scalars['Boolean']>;
  baseUrl?: Maybe<Scalars['String']>;
  saleor?: Maybe<SaleorConfCreateNestedOneWithoutAppConfigInput>;
  zoho?: Maybe<ZohoConfCreateNestedOneWithoutAppConfigInput>;
  productdatafeed?: Maybe<ProductDataFeedCreateNestedOneWithoutAppConfigInput>;
  mailchimp?: Maybe<MailchimpConfCreateNestedOneWithoutAppConfigInput>;
  braintree?: Maybe<BraintreeConfCreateNestedOneWithoutAppConfigInput>;
  lexoffice?: Maybe<LexofficeConfCreateNestedOneWithoutAppConfigInput>;
  trackandtrace?: Maybe<TrackAndTraceConfCreateNestedOneWithoutAppConfigInput>;
  mailgun?: Maybe<MailgunConfCreateNestedOneWithoutAppConfigInput>;
};

export type MailgunConfCreateNestedOneWithoutAppConfigInput = {
  create?: Maybe<MailgunConfCreateWithoutAppConfigInput>;
  connectOrCreate?: Maybe<MailgunConfCreateOrConnectWithoutAppConfigInput>;
  connect?: Maybe<MailgunConfWhereUniqueInput>;
};

export type MailgunConfCreateWithoutAppConfigInput = {
  cuid?: Maybe<Scalars['String']>;
  apiToken: Scalars['String'];
  fromDomain: Scalars['String'];
  fromTitle?: Maybe<Scalars['String']>;
  fromEmail?: Maybe<Scalars['String']>;
  apiHost?: Maybe<Scalars['String']>;
  trackAndTraceConfig?: Maybe<TrackAndTraceConfCreateNestedOneWithoutMailgunInput>;
};

export type TrackAndTraceConfCreateNestedOneWithoutMailgunInput = {
  create?: Maybe<TrackAndTraceConfCreateWithoutMailgunInput>;
  connectOrCreate?: Maybe<TrackAndTraceConfCreateOrConnectWithoutMailgunInput>;
  connect?: Maybe<TrackAndTraceConfWhereUniqueInput>;
};

export type TrackAndTraceConfCreateWithoutMailgunInput = {
  active?: Maybe<Scalars['Boolean']>;
  cuid?: Maybe<Scalars['String']>;
  easypost?: Maybe<EasyPostConfCreateNestedOneWithoutTrackAndTraceConfigInput>;
  appConfig: AppConfigCreateNestedOneWithoutTrackandtraceInput;
  trackAndTraceEmails?: Maybe<TrackAndTraceEmailsCreateNestedManyWithoutTrackAndTraceConfInput>;
};

export type AppConfigCreateNestedOneWithoutTrackandtraceInput = {
  create?: Maybe<AppConfigCreateWithoutTrackandtraceInput>;
  connectOrCreate?: Maybe<AppConfigCreateOrConnectWithoutTrackandtraceInput>;
  connect?: Maybe<AppConfigWhereUniqueInput>;
};

export type AppConfigCreateWithoutTrackandtraceInput = {
  active?: Maybe<Scalars['Boolean']>;
  baseUrl?: Maybe<Scalars['String']>;
  saleor?: Maybe<SaleorConfCreateNestedOneWithoutAppConfigInput>;
  zoho?: Maybe<ZohoConfCreateNestedOneWithoutAppConfigInput>;
  productdatafeed?: Maybe<ProductDataFeedCreateNestedOneWithoutAppConfigInput>;
  mailchimp?: Maybe<MailchimpConfCreateNestedOneWithoutAppConfigInput>;
  easypost?: Maybe<EasyPostConfCreateNestedOneWithoutAppConfigInput>;
  braintree?: Maybe<BraintreeConfCreateNestedOneWithoutAppConfigInput>;
  lexoffice?: Maybe<LexofficeConfCreateNestedOneWithoutAppConfigInput>;
  mailgun?: Maybe<MailgunConfCreateNestedOneWithoutAppConfigInput>;
};

export type AppConfigCreateOrConnectWithoutTrackandtraceInput = {
  where: AppConfigWhereUniqueInput;
  create: AppConfigCreateWithoutTrackandtraceInput;
};

export type AppConfigWhereUniqueInput = {
  id?: Maybe<Scalars['Int']>;
};

export type TrackAndTraceEmailsCreateNestedManyWithoutTrackAndTraceConfInput = {
  create?: Maybe<Array<TrackAndTraceEmailsCreateWithoutTrackAndTraceConfInput>>;
  connectOrCreate?: Maybe<Array<TrackAndTraceEmailsCreateOrConnectWithoutTrackAndTraceConfInput>>;
  connect?: Maybe<Array<TrackAndTraceEmailsWhereUniqueInput>>;
};

export type TrackAndTraceEmailsCreateWithoutTrackAndTraceConfInput = {
  language?: Maybe<Language>;
  inTransitEmailEnabled?: Maybe<Scalars['Boolean']>;
  inTransitEmailSubject?: Maybe<Scalars['String']>;
  inTransitEmailTemplateName?: Maybe<Scalars['String']>;
  outForDeliveryEmailEnabled?: Maybe<Scalars['Boolean']>;
  outForDeliveryEmailTemplateName?: Maybe<Scalars['String']>;
  outForDeliveryEmailSubject?: Maybe<Scalars['String']>;
};

export type TrackAndTraceEmailsCreateOrConnectWithoutTrackAndTraceConfInput = {
  where: TrackAndTraceEmailsWhereUniqueInput;
  create: TrackAndTraceEmailsCreateWithoutTrackAndTraceConfInput;
};

export type TrackAndTraceConfCreateOrConnectWithoutMailgunInput = {
  where: TrackAndTraceConfWhereUniqueInput;
  create: TrackAndTraceConfCreateWithoutMailgunInput;
};

export type TrackAndTraceConfWhereUniqueInput = {
  id?: Maybe<Scalars['Int']>;
  appConfigId?: Maybe<Scalars['Int']>;
};

export type MailgunConfCreateOrConnectWithoutAppConfigInput = {
  where: MailgunConfWhereUniqueInput;
  create: MailgunConfCreateWithoutAppConfigInput;
};

export type MailgunConfWhereUniqueInput = {
  id?: Maybe<Scalars['Int']>;
  appConfigId?: Maybe<Scalars['Int']>;
  trackAndTraceConfigId?: Maybe<Scalars['Int']>;
};

export type AppConfigCreateOrConnectWithoutEasypostInput = {
  where: AppConfigWhereUniqueInput;
  create: AppConfigCreateWithoutEasypostInput;
};

export type AddressVerificationConfCreateNestedOneWithoutEasypostInput = {
  create?: Maybe<AddressVerificationConfCreateWithoutEasypostInput>;
  connectOrCreate?: Maybe<AddressVerificationConfCreateOrConnectWithoutEasypostInput>;
  connect?: Maybe<AddressVerificationConfWhereUniqueInput>;
};

export type AddressVerificationConfCreateWithoutEasypostInput = {
  active?: Maybe<Scalars['Boolean']>;
};

export type AddressVerificationConfCreateOrConnectWithoutEasypostInput = {
  where: AddressVerificationConfWhereUniqueInput;
  create: AddressVerificationConfCreateWithoutEasypostInput;
};

export type AddressVerificationConfWhereUniqueInput = {
  id?: Maybe<Scalars['Int']>;
};

export type EasyPostConfCreateOrConnectWithoutTrackAndTraceConfigInput = {
  where: EasyPostConfWhereUniqueInput;
  create: EasyPostConfCreateWithoutTrackAndTraceConfigInput;
};

export type TrackAndTraceConfCreateOrConnectWithoutAppConfigInput = {
  where: TrackAndTraceConfWhereUniqueInput;
  create: TrackAndTraceConfCreateWithoutAppConfigInput;
};

export type AppConfigCreateOrConnectWithoutMailgunInput = {
  where: AppConfigWhereUniqueInput;
  create: AppConfigCreateWithoutMailgunInput;
};

export type MailgunConfCreateOrConnectWithoutTrackAndTraceConfigInput = {
  where: MailgunConfWhereUniqueInput;
  create: MailgunConfCreateWithoutTrackAndTraceConfigInput;
};

export type TrackAndTraceConfCreateOrConnectWithoutEasypostInput = {
  where: TrackAndTraceConfWhereUniqueInput;
  create: TrackAndTraceConfCreateWithoutEasypostInput;
};

export type EasyPostConfUpsertWithoutAppConfigInput = {
  update: EasyPostConfUpdateWithoutAppConfigInput;
  create: EasyPostConfCreateWithoutAppConfigInput;
};

export type EasyPostConfUpdateWithoutAppConfigInput = {
  cuid?: Maybe<StringFieldUpdateOperationsInput>;
  active?: Maybe<NullableBoolFieldUpdateOperationsInput>;
  apiToken?: Maybe<NullableStringFieldUpdateOperationsInput>;
  webhookID?: Maybe<NullableStringFieldUpdateOperationsInput>;
  trackAndTraceConfig?: Maybe<TrackAndTraceConfUpdateOneWithoutEasypostInput>;
  AddressVerificationConf?: Maybe<AddressVerificationConfUpdateOneWithoutEasypostInput>;
};

export type TrackAndTraceConfUpdateOneWithoutEasypostInput = {
  create?: Maybe<TrackAndTraceConfCreateWithoutEasypostInput>;
  connectOrCreate?: Maybe<TrackAndTraceConfCreateOrConnectWithoutEasypostInput>;
  upsert?: Maybe<TrackAndTraceConfUpsertWithoutEasypostInput>;
  connect?: Maybe<TrackAndTraceConfWhereUniqueInput>;
  disconnect?: Maybe<Scalars['Boolean']>;
  delete?: Maybe<Scalars['Boolean']>;
  update?: Maybe<TrackAndTraceConfUpdateWithoutEasypostInput>;
};

export type TrackAndTraceConfUpsertWithoutEasypostInput = {
  update: TrackAndTraceConfUpdateWithoutEasypostInput;
  create: TrackAndTraceConfCreateWithoutEasypostInput;
};

export type TrackAndTraceConfUpdateWithoutEasypostInput = {
  active?: Maybe<NullableBoolFieldUpdateOperationsInput>;
  cuid?: Maybe<StringFieldUpdateOperationsInput>;
  mailgun?: Maybe<MailgunConfUpdateOneWithoutTrackAndTraceConfigInput>;
  appConfig?: Maybe<AppConfigUpdateOneRequiredWithoutTrackandtraceInput>;
  trackAndTraceEmails?: Maybe<TrackAndTraceEmailsUpdateManyWithoutTrackAndTraceConfInput>;
};

export type MailgunConfUpdateOneWithoutTrackAndTraceConfigInput = {
  create?: Maybe<MailgunConfCreateWithoutTrackAndTraceConfigInput>;
  connectOrCreate?: Maybe<MailgunConfCreateOrConnectWithoutTrackAndTraceConfigInput>;
  upsert?: Maybe<MailgunConfUpsertWithoutTrackAndTraceConfigInput>;
  connect?: Maybe<MailgunConfWhereUniqueInput>;
  disconnect?: Maybe<Scalars['Boolean']>;
  delete?: Maybe<Scalars['Boolean']>;
  update?: Maybe<MailgunConfUpdateWithoutTrackAndTraceConfigInput>;
};

export type MailgunConfUpsertWithoutTrackAndTraceConfigInput = {
  update: MailgunConfUpdateWithoutTrackAndTraceConfigInput;
  create: MailgunConfCreateWithoutTrackAndTraceConfigInput;
};

export type MailgunConfUpdateWithoutTrackAndTraceConfigInput = {
  cuid?: Maybe<NullableStringFieldUpdateOperationsInput>;
  apiToken?: Maybe<StringFieldUpdateOperationsInput>;
  fromDomain?: Maybe<StringFieldUpdateOperationsInput>;
  fromTitle?: Maybe<NullableStringFieldUpdateOperationsInput>;
  fromEmail?: Maybe<NullableStringFieldUpdateOperationsInput>;
  apiHost?: Maybe<StringFieldUpdateOperationsInput>;
  appConfig?: Maybe<AppConfigUpdateOneWithoutMailgunInput>;
};

export type AppConfigUpdateOneWithoutMailgunInput = {
  create?: Maybe<AppConfigCreateWithoutMailgunInput>;
  connectOrCreate?: Maybe<AppConfigCreateOrConnectWithoutMailgunInput>;
  upsert?: Maybe<AppConfigUpsertWithoutMailgunInput>;
  connect?: Maybe<AppConfigWhereUniqueInput>;
  disconnect?: Maybe<Scalars['Boolean']>;
  delete?: Maybe<Scalars['Boolean']>;
  update?: Maybe<AppConfigUpdateWithoutMailgunInput>;
};

export type AppConfigUpsertWithoutMailgunInput = {
  update: AppConfigUpdateWithoutMailgunInput;
  create: AppConfigCreateWithoutMailgunInput;
};

export type AppConfigUpdateWithoutMailgunInput = {
  active?: Maybe<NullableBoolFieldUpdateOperationsInput>;
  baseUrl?: Maybe<NullableStringFieldUpdateOperationsInput>;
  saleor?: Maybe<SaleorConfUpdateOneWithoutAppConfigInput>;
  zoho?: Maybe<ZohoConfUpdateOneWithoutAppConfigInput>;
  productdatafeed?: Maybe<ProductDataFeedUpdateOneWithoutAppConfigInput>;
  mailchimp?: Maybe<MailchimpConfUpdateOneWithoutAppConfigInput>;
  easypost?: Maybe<EasyPostConfUpdateOneWithoutAppConfigInput>;
  braintree?: Maybe<BraintreeConfUpdateOneWithoutAppConfigInput>;
  lexoffice?: Maybe<LexofficeConfUpdateOneWithoutAppConfigInput>;
  trackandtrace?: Maybe<TrackAndTraceConfUpdateOneWithoutAppConfigInput>;
};

export type BraintreeConfUpdateOneWithoutAppConfigInput = {
  create?: Maybe<BraintreeConfCreateWithoutAppConfigInput>;
  connectOrCreate?: Maybe<BraintreeConfCreateOrConnectWithoutAppConfigInput>;
  upsert?: Maybe<BraintreeConfUpsertWithoutAppConfigInput>;
  connect?: Maybe<BraintreeConfWhereUniqueInput>;
  disconnect?: Maybe<Scalars['Boolean']>;
  delete?: Maybe<Scalars['Boolean']>;
  update?: Maybe<BraintreeConfUpdateWithoutAppConfigInput>;
};

export type BraintreeConfUpsertWithoutAppConfigInput = {
  update: BraintreeConfUpdateWithoutAppConfigInput;
  create: BraintreeConfCreateWithoutAppConfigInput;
};

export type BraintreeConfUpdateWithoutAppConfigInput = {
  active?: Maybe<BoolFieldUpdateOperationsInput>;
  merchantId?: Maybe<NullableStringFieldUpdateOperationsInput>;
  publicKey?: Maybe<NullableStringFieldUpdateOperationsInput>;
  privateKey?: Maybe<NullableStringFieldUpdateOperationsInput>;
};

export type BoolFieldUpdateOperationsInput = {
  set?: Maybe<Scalars['Boolean']>;
};

export type LexofficeConfUpdateOneWithoutAppConfigInput = {
  create?: Maybe<LexofficeConfCreateWithoutAppConfigInput>;
  connectOrCreate?: Maybe<LexofficeConfCreateOrConnectWithoutAppConfigInput>;
  upsert?: Maybe<LexofficeConfUpsertWithoutAppConfigInput>;
  connect?: Maybe<LexofficeConfWhereUniqueInput>;
  disconnect?: Maybe<Scalars['Boolean']>;
  delete?: Maybe<Scalars['Boolean']>;
  update?: Maybe<LexofficeConfUpdateWithoutAppConfigInput>;
};

export type LexofficeConfUpsertWithoutAppConfigInput = {
  update: LexofficeConfUpdateWithoutAppConfigInput;
  create: LexofficeConfCreateWithoutAppConfigInput;
};

export type LexofficeConfUpdateWithoutAppConfigInput = {
  active?: Maybe<BoolFieldUpdateOperationsInput>;
  apiToken?: Maybe<NullableStringFieldUpdateOperationsInput>;
};

export type TrackAndTraceConfUpdateOneWithoutAppConfigInput = {
  create?: Maybe<TrackAndTraceConfCreateWithoutAppConfigInput>;
  connectOrCreate?: Maybe<TrackAndTraceConfCreateOrConnectWithoutAppConfigInput>;
  upsert?: Maybe<TrackAndTraceConfUpsertWithoutAppConfigInput>;
  connect?: Maybe<TrackAndTraceConfWhereUniqueInput>;
  disconnect?: Maybe<Scalars['Boolean']>;
  delete?: Maybe<Scalars['Boolean']>;
  update?: Maybe<TrackAndTraceConfUpdateWithoutAppConfigInput>;
};

export type TrackAndTraceConfUpsertWithoutAppConfigInput = {
  update: TrackAndTraceConfUpdateWithoutAppConfigInput;
  create: TrackAndTraceConfCreateWithoutAppConfigInput;
};

export type TrackAndTraceConfUpdateWithoutAppConfigInput = {
  active?: Maybe<NullableBoolFieldUpdateOperationsInput>;
  cuid?: Maybe<StringFieldUpdateOperationsInput>;
  mailgun?: Maybe<MailgunConfUpdateOneWithoutTrackAndTraceConfigInput>;
  easypost?: Maybe<EasyPostConfUpdateOneWithoutTrackAndTraceConfigInput>;
  trackAndTraceEmails?: Maybe<TrackAndTraceEmailsUpdateManyWithoutTrackAndTraceConfInput>;
};

export type EasyPostConfUpdateOneWithoutTrackAndTraceConfigInput = {
  create?: Maybe<EasyPostConfCreateWithoutTrackAndTraceConfigInput>;
  connectOrCreate?: Maybe<EasyPostConfCreateOrConnectWithoutTrackAndTraceConfigInput>;
  upsert?: Maybe<EasyPostConfUpsertWithoutTrackAndTraceConfigInput>;
  connect?: Maybe<EasyPostConfWhereUniqueInput>;
  disconnect?: Maybe<Scalars['Boolean']>;
  delete?: Maybe<Scalars['Boolean']>;
  update?: Maybe<EasyPostConfUpdateWithoutTrackAndTraceConfigInput>;
};

export type EasyPostConfUpsertWithoutTrackAndTraceConfigInput = {
  update: EasyPostConfUpdateWithoutTrackAndTraceConfigInput;
  create: EasyPostConfCreateWithoutTrackAndTraceConfigInput;
};

export type EasyPostConfUpdateWithoutTrackAndTraceConfigInput = {
  cuid?: Maybe<StringFieldUpdateOperationsInput>;
  active?: Maybe<NullableBoolFieldUpdateOperationsInput>;
  apiToken?: Maybe<NullableStringFieldUpdateOperationsInput>;
  webhookID?: Maybe<NullableStringFieldUpdateOperationsInput>;
  appConfig?: Maybe<AppConfigUpdateOneRequiredWithoutEasypostInput>;
  AddressVerificationConf?: Maybe<AddressVerificationConfUpdateOneWithoutEasypostInput>;
};

export type AppConfigUpdateOneRequiredWithoutEasypostInput = {
  create?: Maybe<AppConfigCreateWithoutEasypostInput>;
  connectOrCreate?: Maybe<AppConfigCreateOrConnectWithoutEasypostInput>;
  upsert?: Maybe<AppConfigUpsertWithoutEasypostInput>;
  connect?: Maybe<AppConfigWhereUniqueInput>;
  update?: Maybe<AppConfigUpdateWithoutEasypostInput>;
};

export type AppConfigUpsertWithoutEasypostInput = {
  update: AppConfigUpdateWithoutEasypostInput;
  create: AppConfigCreateWithoutEasypostInput;
};

export type AppConfigUpdateWithoutEasypostInput = {
  active?: Maybe<NullableBoolFieldUpdateOperationsInput>;
  baseUrl?: Maybe<NullableStringFieldUpdateOperationsInput>;
  saleor?: Maybe<SaleorConfUpdateOneWithoutAppConfigInput>;
  zoho?: Maybe<ZohoConfUpdateOneWithoutAppConfigInput>;
  productdatafeed?: Maybe<ProductDataFeedUpdateOneWithoutAppConfigInput>;
  mailchimp?: Maybe<MailchimpConfUpdateOneWithoutAppConfigInput>;
  braintree?: Maybe<BraintreeConfUpdateOneWithoutAppConfigInput>;
  lexoffice?: Maybe<LexofficeConfUpdateOneWithoutAppConfigInput>;
  trackandtrace?: Maybe<TrackAndTraceConfUpdateOneWithoutAppConfigInput>;
  mailgun?: Maybe<MailgunConfUpdateOneWithoutAppConfigInput>;
};

export type MailgunConfUpdateOneWithoutAppConfigInput = {
  create?: Maybe<MailgunConfCreateWithoutAppConfigInput>;
  connectOrCreate?: Maybe<MailgunConfCreateOrConnectWithoutAppConfigInput>;
  upsert?: Maybe<MailgunConfUpsertWithoutAppConfigInput>;
  connect?: Maybe<MailgunConfWhereUniqueInput>;
  disconnect?: Maybe<Scalars['Boolean']>;
  delete?: Maybe<Scalars['Boolean']>;
  update?: Maybe<MailgunConfUpdateWithoutAppConfigInput>;
};

export type MailgunConfUpsertWithoutAppConfigInput = {
  update: MailgunConfUpdateWithoutAppConfigInput;
  create: MailgunConfCreateWithoutAppConfigInput;
};

export type MailgunConfUpdateWithoutAppConfigInput = {
  cuid?: Maybe<NullableStringFieldUpdateOperationsInput>;
  apiToken?: Maybe<StringFieldUpdateOperationsInput>;
  fromDomain?: Maybe<StringFieldUpdateOperationsInput>;
  fromTitle?: Maybe<NullableStringFieldUpdateOperationsInput>;
  fromEmail?: Maybe<NullableStringFieldUpdateOperationsInput>;
  apiHost?: Maybe<StringFieldUpdateOperationsInput>;
  trackAndTraceConfig?: Maybe<TrackAndTraceConfUpdateOneWithoutMailgunInput>;
};

export type TrackAndTraceConfUpdateOneWithoutMailgunInput = {
  create?: Maybe<TrackAndTraceConfCreateWithoutMailgunInput>;
  connectOrCreate?: Maybe<TrackAndTraceConfCreateOrConnectWithoutMailgunInput>;
  upsert?: Maybe<TrackAndTraceConfUpsertWithoutMailgunInput>;
  connect?: Maybe<TrackAndTraceConfWhereUniqueInput>;
  disconnect?: Maybe<Scalars['Boolean']>;
  delete?: Maybe<Scalars['Boolean']>;
  update?: Maybe<TrackAndTraceConfUpdateWithoutMailgunInput>;
};

export type TrackAndTraceConfUpsertWithoutMailgunInput = {
  update: TrackAndTraceConfUpdateWithoutMailgunInput;
  create: TrackAndTraceConfCreateWithoutMailgunInput;
};

export type TrackAndTraceConfUpdateWithoutMailgunInput = {
  active?: Maybe<NullableBoolFieldUpdateOperationsInput>;
  cuid?: Maybe<StringFieldUpdateOperationsInput>;
  easypost?: Maybe<EasyPostConfUpdateOneWithoutTrackAndTraceConfigInput>;
  appConfig?: Maybe<AppConfigUpdateOneRequiredWithoutTrackandtraceInput>;
  trackAndTraceEmails?: Maybe<TrackAndTraceEmailsUpdateManyWithoutTrackAndTraceConfInput>;
};

export type AppConfigUpdateOneRequiredWithoutTrackandtraceInput = {
  create?: Maybe<AppConfigCreateWithoutTrackandtraceInput>;
  connectOrCreate?: Maybe<AppConfigCreateOrConnectWithoutTrackandtraceInput>;
  upsert?: Maybe<AppConfigUpsertWithoutTrackandtraceInput>;
  connect?: Maybe<AppConfigWhereUniqueInput>;
  update?: Maybe<AppConfigUpdateWithoutTrackandtraceInput>;
};

export type AppConfigUpsertWithoutTrackandtraceInput = {
  update: AppConfigUpdateWithoutTrackandtraceInput;
  create: AppConfigCreateWithoutTrackandtraceInput;
};

export type AppConfigUpdateWithoutTrackandtraceInput = {
  active?: Maybe<NullableBoolFieldUpdateOperationsInput>;
  baseUrl?: Maybe<NullableStringFieldUpdateOperationsInput>;
  saleor?: Maybe<SaleorConfUpdateOneWithoutAppConfigInput>;
  zoho?: Maybe<ZohoConfUpdateOneWithoutAppConfigInput>;
  productdatafeed?: Maybe<ProductDataFeedUpdateOneWithoutAppConfigInput>;
  mailchimp?: Maybe<MailchimpConfUpdateOneWithoutAppConfigInput>;
  easypost?: Maybe<EasyPostConfUpdateOneWithoutAppConfigInput>;
  braintree?: Maybe<BraintreeConfUpdateOneWithoutAppConfigInput>;
  lexoffice?: Maybe<LexofficeConfUpdateOneWithoutAppConfigInput>;
  mailgun?: Maybe<MailgunConfUpdateOneWithoutAppConfigInput>;
};

export type TrackAndTraceEmailsUpdateManyWithoutTrackAndTraceConfInput = {
  create?: Maybe<Array<TrackAndTraceEmailsCreateWithoutTrackAndTraceConfInput>>;
  connectOrCreate?: Maybe<Array<TrackAndTraceEmailsCreateOrConnectWithoutTrackAndTraceConfInput>>;
  upsert?: Maybe<Array<TrackAndTraceEmailsUpsertWithWhereUniqueWithoutTrackAndTraceConfInput>>;
  connect?: Maybe<Array<TrackAndTraceEmailsWhereUniqueInput>>;
  set?: Maybe<Array<TrackAndTraceEmailsWhereUniqueInput>>;
  disconnect?: Maybe<Array<TrackAndTraceEmailsWhereUniqueInput>>;
  delete?: Maybe<Array<TrackAndTraceEmailsWhereUniqueInput>>;
  update?: Maybe<Array<TrackAndTraceEmailsUpdateWithWhereUniqueWithoutTrackAndTraceConfInput>>;
  updateMany?: Maybe<Array<TrackAndTraceEmailsUpdateManyWithWhereWithoutTrackAndTraceConfInput>>;
  deleteMany?: Maybe<Array<TrackAndTraceEmailsScalarWhereInput>>;
};

export type TrackAndTraceEmailsUpsertWithWhereUniqueWithoutTrackAndTraceConfInput = {
  where: TrackAndTraceEmailsWhereUniqueInput;
  update: TrackAndTraceEmailsUpdateWithoutTrackAndTraceConfInput;
  create: TrackAndTraceEmailsCreateWithoutTrackAndTraceConfInput;
};

export type TrackAndTraceEmailsUpdateWithoutTrackAndTraceConfInput = {
  language?: Maybe<EnumLanguageFieldUpdateOperationsInput>;
  inTransitEmailEnabled?: Maybe<NullableBoolFieldUpdateOperationsInput>;
  inTransitEmailSubject?: Maybe<NullableStringFieldUpdateOperationsInput>;
  inTransitEmailTemplateName?: Maybe<NullableStringFieldUpdateOperationsInput>;
  outForDeliveryEmailEnabled?: Maybe<NullableBoolFieldUpdateOperationsInput>;
  outForDeliveryEmailTemplateName?: Maybe<NullableStringFieldUpdateOperationsInput>;
  outForDeliveryEmailSubject?: Maybe<NullableStringFieldUpdateOperationsInput>;
};

export type EnumLanguageFieldUpdateOperationsInput = {
  set?: Maybe<Language>;
};

export type TrackAndTraceEmailsUpdateWithWhereUniqueWithoutTrackAndTraceConfInput = {
  where: TrackAndTraceEmailsWhereUniqueInput;
  data: TrackAndTraceEmailsUpdateWithoutTrackAndTraceConfInput;
};

export type TrackAndTraceEmailsUpdateManyWithWhereWithoutTrackAndTraceConfInput = {
  where: TrackAndTraceEmailsScalarWhereInput;
  data: TrackAndTraceEmailsUpdateManyMutationInput;
};

export type TrackAndTraceEmailsScalarWhereInput = {
  AND?: Maybe<Array<TrackAndTraceEmailsScalarWhereInput>>;
  OR?: Maybe<Array<TrackAndTraceEmailsScalarWhereInput>>;
  NOT?: Maybe<Array<TrackAndTraceEmailsScalarWhereInput>>;
  id?: Maybe<IntFilter>;
  language?: Maybe<EnumLanguageFilter>;
  inTransitEmailEnabled?: Maybe<BoolNullableFilter>;
  inTransitEmailSubject?: Maybe<StringNullableFilter>;
  inTransitEmailTemplateName?: Maybe<StringNullableFilter>;
  outForDeliveryEmailEnabled?: Maybe<BoolNullableFilter>;
  outForDeliveryEmailTemplateName?: Maybe<StringNullableFilter>;
  outForDeliveryEmailSubject?: Maybe<StringNullableFilter>;
  trackAndTraceConfId?: Maybe<IntFilter>;
};

export type TrackAndTraceEmailsUpdateManyMutationInput = {
  language?: Maybe<EnumLanguageFieldUpdateOperationsInput>;
  inTransitEmailEnabled?: Maybe<NullableBoolFieldUpdateOperationsInput>;
  inTransitEmailSubject?: Maybe<NullableStringFieldUpdateOperationsInput>;
  inTransitEmailTemplateName?: Maybe<NullableStringFieldUpdateOperationsInput>;
  outForDeliveryEmailEnabled?: Maybe<NullableBoolFieldUpdateOperationsInput>;
  outForDeliveryEmailTemplateName?: Maybe<NullableStringFieldUpdateOperationsInput>;
  outForDeliveryEmailSubject?: Maybe<NullableStringFieldUpdateOperationsInput>;
};

export type AddressVerificationConfUpdateOneWithoutEasypostInput = {
  create?: Maybe<AddressVerificationConfCreateWithoutEasypostInput>;
  connectOrCreate?: Maybe<AddressVerificationConfCreateOrConnectWithoutEasypostInput>;
  upsert?: Maybe<AddressVerificationConfUpsertWithoutEasypostInput>;
  connect?: Maybe<AddressVerificationConfWhereUniqueInput>;
  disconnect?: Maybe<Scalars['Boolean']>;
  delete?: Maybe<Scalars['Boolean']>;
  update?: Maybe<AddressVerificationConfUpdateWithoutEasypostInput>;
};

export type AddressVerificationConfUpsertWithoutEasypostInput = {
  update: AddressVerificationConfUpdateWithoutEasypostInput;
  create: AddressVerificationConfCreateWithoutEasypostInput;
};

export type AddressVerificationConfUpdateWithoutEasypostInput = {
  active?: Maybe<NullableBoolFieldUpdateOperationsInput>;
};

export type ProductDataFeedConfigFragment = (
  { __typename?: 'ProductDataFeed' }
  & Pick<ProductDataFeed, 'active' | 'productDetailStorefrontURL' | 'id' | 'cuid' | 'channel'>
);

export type ZohoConfigFragment = (
  { __typename?: 'ZohoConf' }
  & Pick<ZohoConf, 'active' | 'id' | 'orgId' | 'clientSecret' | 'clientId' | 'appConfigId' | 'payPalAccountId' | 'creditCardAccountId'>
);

export type BraintreeConfigFragment = (
  { __typename?: 'BraintreeConf' }
  & Pick<BraintreeConf, 'active' | 'id' | 'merchantId' | 'publicKey' | 'privateKey'>
);

export type TaTEmailFragment = (
  { __typename?: 'TrackAndTraceEmails' }
  & Pick<TrackAndTraceEmails, 'id' | 'language' | 'inTransitEmailEnabled' | 'inTransitEmailTemplateName' | 'inTransitEmailSubject' | 'outForDeliveryEmailEnabled' | 'outForDeliveryEmailTemplateName' | 'outForDeliveryEmailSubject'>
);

export type TrackAndTraceConfigFragment = (
  { __typename?: 'TrackAndTraceConf' }
  & Pick<TrackAndTraceConf, 'active' | 'id'>
  & { trackAndTraceEmails: Array<(
    { __typename?: 'TrackAndTraceEmails' }
    & TaTEmailFragment
  )> }
);

export type MailgunConfigFragment = (
  { __typename?: 'MailgunConf' }
  & Pick<MailgunConf, 'id' | 'apiToken' | 'fromDomain' | 'fromTitle' | 'fromEmail'>
);

export type EasyPostConfigFragment = (
  { __typename?: 'EasyPostConf' }
  & Pick<EasyPostConf, 'id' | 'apiToken'>
);

export type MailchimpConfigFragment = (
  { __typename?: 'MailchimpConf' }
  & Pick<MailchimpConf, 'active' | 'id' | 'apiToken' | 'listId' | 'storeUrl' | 'productUrlPattern'>
);

export type SaleorDataQueryVariables = Exact<{ [key: string]: never; }>;


export type SaleorDataQuery = (
  { __typename?: 'Query' }
  & { getSaleorData?: Maybe<(
    { __typename?: 'SaleorDataClass' }
    & { channels: Array<(
      { __typename?: 'Channel' }
      & Pick<Channel, 'id' | 'slug' | 'name' | 'isActive'>
    )> }
  )> }
);

export type AppConfigQueryVariables = Exact<{ [key: string]: never; }>;


export type AppConfigQuery = (
  { __typename?: 'Query' }
  & { getAppConfig?: Maybe<(
    { __typename?: 'AppConfig' }
    & Pick<AppConfig, 'id' | 'baseUrl'>
    & { saleor?: Maybe<(
      { __typename?: 'SaleorConf' }
      & Pick<SaleorConf, 'id' | 'domain' | 'appId'>
    )>, zoho?: Maybe<(
      { __typename?: 'ZohoConf' }
      & ZohoConfigFragment
    )>, productdatafeed?: Maybe<(
      { __typename?: 'ProductDataFeed' }
      & ProductDataFeedConfigFragment
    )>, mailchimp?: Maybe<(
      { __typename?: 'MailchimpConf' }
      & MailchimpConfigFragment
    )>, braintree?: Maybe<(
      { __typename?: 'BraintreeConf' }
      & BraintreeConfigFragment
    )>, easypost?: Maybe<(
      { __typename?: 'EasyPostConf' }
      & EasyPostConfigFragment
    )>, mailgun?: Maybe<(
      { __typename?: 'MailgunConf' }
      & MailgunConfigFragment
    )>, trackandtrace?: Maybe<(
      { __typename?: 'TrackAndTraceConf' }
      & TrackAndTraceConfigFragment
    )> }
  )> }
);

export type AppConfigUpdateMutationVariables = Exact<{
  id: Scalars['ID'];
  AppConfigInputData: AppConfigUpdateInput;
}>;


export type AppConfigUpdateMutation = (
  { __typename?: 'Mutation' }
  & { updateAppConfig: (
    { __typename?: 'AppConfig' }
    & { zoho?: Maybe<(
      { __typename?: 'ZohoConf' }
      & Pick<ZohoConf, 'active'>
    )> }
  ) }
);
