require('dotenv').config();
const withSourceMaps = require('@zeit/next-source-maps')();

// Use the SentryWebpack plugin to upload the source maps during build step
const SentryWebpackPlugin = require('@sentry/webpack-plugin');

const {
    NEXT_PUBLIC_SENTRY_DSN,
    SENTRY_ORG,
    SENTRY_PROJECT,
    SENTRY_AUTH_TOKEN,
    NODE_ENV,
} = process.env;

module.exports = withSourceMaps({
    env: {
        NEXT_PUBLIC_SENTRY_DSN: process.env.NEXT_PUBLIC_SENTRY_DSN,
    },
    assetPrefix: process.env.ECI_BASE_URL,
    trailingSlash: true,
    poweredByHeader: false,
    async headers() {
        return [
            {
                source: '/(.*)?',
                headers: [
                    {
                        key: 'Access-Control-Allow-Origin',
                        value: '*',
                    },
                    {
                        key: 'Access-Control-Allow-Headers',
                        value: '*',
                    },
                    {
                        key: 'Access-Control-Allow-Methods',
                        value: 'GET,HEAD,OPTIONS',
                    },
                ],
            },
        ];
    },
    webpack: (config, options) => {
    // When all the Sentry configuration env variables are available/configured
        // The Sentry webpack plugin gets pushed to the webpack plugins to build
        // and upload the source maps to sentry.
        // This is an alternative to manually uploading the source maps
        // Note: This is disabled in development mode.
        if (
            NEXT_PUBLIC_SENTRY_DSN
      && SENTRY_ORG
      && SENTRY_PROJECT
      && SENTRY_AUTH_TOKEN
      && NODE_ENV === 'production'
        ) {
            config.plugins.push(
                new SentryWebpackPlugin({
                    release: options.buildId,
                    include: '.next',
                    ignore: ['node_modules'],
                    urlPrefix: '~/_next',
                }),
            );
        }

        return config;
    },
});
