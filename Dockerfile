FROM node:12.18.1-alpine as builder
WORKDIR /app
COPY . .
WORKDIR /app/worker
ENV CI=true
RUN yarn workspaces focus
RUN yarn build
RUN yarn bundle
RUN apk update && apk add unzip
RUN unzip bundle.zip
RUN rm -rf bundle/*/src



FROM node:12.18.1-alpine
WORKDIR /app
ENV NODE_ENV production
COPY --from=builder /app/worker/bundle /app/bundle
WORKDIR /app/bundle/worker

CMD [ "node", "dist/index.js" ]