-- CreateEnum
CREATE TYPE "Language" AS ENUM ('DE', 'EN');

-- CreateTable
CREATE TABLE "TrackAndTraceEmails" (
    "id" SERIAL NOT NULL,
    "language" "Language" NOT NULL DEFAULT E'DE',
    "inTransitEmailEnabled" BOOLEAN,
    "inTransitEmailTemplateName" TEXT,
    "outForDeliveryEmailEnabled" BOOLEAN,
    "outForDeliveryEmailTemplateName" TEXT,
    "trackAndTraceConfId" INTEGER NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "TrackAndTraceEmails.language_unique" ON "TrackAndTraceEmails"("language");

-- AddForeignKey
ALTER TABLE "TrackAndTraceEmails" ADD FOREIGN KEY ("trackAndTraceConfId") REFERENCES "TrackAndTraceConf"("id") ON DELETE CASCADE ON UPDATE CASCADE;
