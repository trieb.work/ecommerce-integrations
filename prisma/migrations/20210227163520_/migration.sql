/*
  Warnings:

  - You are about to drop the column `mailgunConfId` on the `AppConfig` table. All the data in the column will be lost.
  - The migration will add a unique constraint covering the columns `[appConfigId]` on the table `MailgunConf`. If there are existing duplicate values, the migration will fail.

*/
-- DropForeignKey
ALTER TABLE "AppConfig" DROP CONSTRAINT "AppConfig_mailgunConfId_fkey";

-- AlterTable
ALTER TABLE "AppConfig" DROP COLUMN "mailgunConfId";

-- AlterTable
ALTER TABLE "MailgunConf" ADD COLUMN     "appConfigId" INTEGER;

-- CreateIndex
CREATE UNIQUE INDEX "MailgunConf.appConfigId_unique" ON "MailgunConf"("appConfigId");

-- AddForeignKey
ALTER TABLE "MailgunConf" ADD FOREIGN KEY ("appConfigId") REFERENCES "AppConfig"("id") ON DELETE SET NULL ON UPDATE CASCADE;
