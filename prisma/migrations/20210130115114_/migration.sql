-- CreateTable
CREATE TABLE "BraintreeConf" (
    "id" SERIAL NOT NULL,
    "active" BOOLEAN DEFAULT false,
    "merchantId" TEXT NOT NULL,
    "clientId" TEXT NOT NULL,
    "clientSecret" TEXT NOT NULL,
    "zohoConfId" INTEGER NOT NULL,

    PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "BraintreeConf" ADD FOREIGN KEY ("zohoConfId") REFERENCES "ZohoConf"("id") ON DELETE CASCADE ON UPDATE CASCADE;
