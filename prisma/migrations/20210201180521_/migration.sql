-- CreateTable
CREATE TABLE "ElasticConf" (
    "id" SERIAL NOT NULL,
    "apmServer" TEXT,
    "apmSecretToken" TEXT,
    "loggingServer" TEXT,
    "commonConfigId" INTEGER NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "ElasticConf_commonConfigId_unique" ON "ElasticConf"("commonConfigId");

-- AddForeignKey
ALTER TABLE "ElasticConf" ADD FOREIGN KEY ("commonConfigId") REFERENCES "CommonConfig"("id") ON DELETE CASCADE ON UPDATE CASCADE;
