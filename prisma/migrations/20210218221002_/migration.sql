/*
  Warnings:

  - You are about to drop the column `webhookID` on the `LexofficeConf` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "LexofficeConf" DROP COLUMN "webhookID",
ADD COLUMN     "apiToken" TEXT;

-- AlterTable
ALTER TABLE "ZohoConf" ALTER COLUMN "cuid" DROP NOT NULL;
