-- CreateTable
CREATE TABLE "MailchimpConf" (
    "id" SERIAL NOT NULL,
    "active" BOOLEAN DEFAULT false,
    "listId" TEXT,
    "apiToken" TEXT,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "ProductDataFeed" (
    "id" SERIAL NOT NULL,
    "cuid" TEXT NOT NULL,
    "active" BOOLEAN DEFAULT false,
    "productDetailStorefrontURL" TEXT,
    "appConfigId" INTEGER NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "ZohoConf" (
    "id" SERIAL NOT NULL,
    "active" BOOLEAN DEFAULT false,
    "orgId" TEXT DEFAULT E'',
    "clientId" TEXT DEFAULT E'',
    "clientSecret" TEXT DEFAULT E'',
    "webhookToken" TEXT,
    "webhookID" TEXT,
    "appConfigId" INTEGER NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "SaleorConf" (
    "id" SERIAL NOT NULL,
    "domain" TEXT,
    "appId" TEXT,
    "authToken" TEXT,
    "webhookToken" TEXT,
    "webhookID" TEXT,
    "appConfigId" INTEGER NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "AppConfig" (
    "id" SERIAL NOT NULL,
    "active" BOOLEAN,
    "baseUrl" TEXT,
    "mailchimpConfId" INTEGER,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "RedisConf" (
    "id" SERIAL NOT NULL,
    "host" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "port" INTEGER NOT NULL,
    "commonConfigId" INTEGER NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "GoogleOAuth" (
    "id" SERIAL NOT NULL,
    "clientId" TEXT NOT NULL,
    "clientSecret" TEXT NOT NULL,
    "commonConfigId" INTEGER NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "CommonConfig" (
    "id" SERIAL NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "ProductDataFeed_appConfigId_unique" ON "ProductDataFeed"("appConfigId");

-- CreateIndex
CREATE UNIQUE INDEX "ZohoConf_appConfigId_unique" ON "ZohoConf"("appConfigId");

-- CreateIndex
CREATE UNIQUE INDEX "SaleorConf.domain_unique" ON "SaleorConf"("domain");

-- CreateIndex
CREATE UNIQUE INDEX "SaleorConf_appConfigId_unique" ON "SaleorConf"("appConfigId");

-- CreateIndex
CREATE UNIQUE INDEX "RedisConf_commonConfigId_unique" ON "RedisConf"("commonConfigId");

-- CreateIndex
CREATE UNIQUE INDEX "GoogleOAuth_commonConfigId_unique" ON "GoogleOAuth"("commonConfigId");

-- AddForeignKey
ALTER TABLE "ProductDataFeed" ADD FOREIGN KEY ("appConfigId") REFERENCES "AppConfig"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ZohoConf" ADD FOREIGN KEY ("appConfigId") REFERENCES "AppConfig"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "SaleorConf" ADD FOREIGN KEY ("appConfigId") REFERENCES "AppConfig"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "AppConfig" ADD FOREIGN KEY ("mailchimpConfId") REFERENCES "MailchimpConf"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "RedisConf" ADD FOREIGN KEY ("commonConfigId") REFERENCES "CommonConfig"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "GoogleOAuth" ADD FOREIGN KEY ("commonConfigId") REFERENCES "CommonConfig"("id") ON DELETE CASCADE ON UPDATE CASCADE;
