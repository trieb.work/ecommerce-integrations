-- AlterTable
ALTER TABLE "MailchimpConf" ADD COLUMN     "channel" TEXT;

-- CreateTable
CREATE TABLE "AddressVerificationConf" (
    "id" SERIAL NOT NULL,
    "active" BOOLEAN DEFAULT false,
    "easyPostConfId" INTEGER,

    PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "AddressVerificationConf" ADD FOREIGN KEY ("easyPostConfId") REFERENCES "EasyPostConf"("id") ON DELETE SET NULL ON UPDATE CASCADE;
