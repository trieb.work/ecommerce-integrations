/*
  Warnings:

  - Made the column `cuid` on table `ZohoConf` required. The migration will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "ZohoConf" ADD COLUMN     "customFunctionID" TEXT,
ALTER COLUMN "cuid" SET NOT NULL;
