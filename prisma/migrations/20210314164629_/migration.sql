/*
  Warnings:

  - You are about to drop the column `mailchimpConfId` on the `AppConfig` table. All the data in the column will be lost.
  - The migration will add a unique constraint covering the columns `[easyPostConfId]` on the table `AddressVerificationConf`. If there are existing duplicate values, the migration will fail.
  - The migration will add a unique constraint covering the columns `[appConfigId]` on the table `MailchimpConf`. If there are existing duplicate values, the migration will fail.
  - Made the column `easyPostConfId` on table `AddressVerificationConf` required. The migration will fail if there are existing NULL values in that column.

*/
-- DropForeignKey
ALTER TABLE "AppConfig" DROP CONSTRAINT "AppConfig_mailchimpConfId_fkey";

-- AlterTable
ALTER TABLE "AddressVerificationConf" ALTER COLUMN "easyPostConfId" SET NOT NULL;

-- AlterTable
ALTER TABLE "AppConfig" DROP COLUMN "mailchimpConfId";

-- AlterTable
ALTER TABLE "MailchimpConf" ADD COLUMN     "appConfigId" INTEGER;

-- CreateIndex
CREATE UNIQUE INDEX "AddressVerificationConf_easyPostConfId_unique" ON "AddressVerificationConf"("easyPostConfId");

-- CreateIndex
CREATE UNIQUE INDEX "MailchimpConf.appConfigId_unique" ON "MailchimpConf"("appConfigId");

-- AddForeignKey
ALTER TABLE "MailchimpConf" ADD FOREIGN KEY ("appConfigId") REFERENCES "AppConfig"("id") ON DELETE SET NULL ON UPDATE CASCADE;
