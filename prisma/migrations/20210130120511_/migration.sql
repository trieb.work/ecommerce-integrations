/*
  Warnings:

  - The migration will add a unique constraint covering the columns `[zohoConfId]` on the table `BraintreeConf`. If there are existing duplicate values, the migration will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "BraintreeConf_zohoConfId_unique" ON "BraintreeConf"("zohoConfId");
