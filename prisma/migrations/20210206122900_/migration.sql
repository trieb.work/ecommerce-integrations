/*
  Warnings:

  - Made the column `active` on table `BraintreeConf` required. The migration will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "BraintreeConf" ALTER COLUMN "active" SET NOT NULL,
ALTER COLUMN "merchantId" DROP NOT NULL,
ALTER COLUMN "clientId" DROP NOT NULL,
ALTER COLUMN "clientSecret" DROP NOT NULL;
