-- CreateTable
CREATE TABLE "MailgunConf" (
    "id" SERIAL NOT NULL,
    "apiToken" TEXT NOT NULL,
    "fromDomain" TEXT NOT NULL,
    "fromTitle" TEXT,
    "fromEmail" TEXT,
    "apiHost" TEXT,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "TrackAndTrace" (
    "id" SERIAL NOT NULL,
    "active" BOOLEAN DEFAULT false,
    "cuid" TEXT NOT NULL,
    "appConfigId" INTEGER NOT NULL,
    "mailgunConfId" INTEGER,
    "easyPostConfId" INTEGER,

    PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "TrackAndTrace.appConfigId_unique" ON "TrackAndTrace"("appConfigId");

-- AddForeignKey
ALTER TABLE "TrackAndTrace" ADD FOREIGN KEY ("mailgunConfId") REFERENCES "MailgunConf"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TrackAndTrace" ADD FOREIGN KEY ("easyPostConfId") REFERENCES "EasyPostConf"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TrackAndTrace" ADD FOREIGN KEY ("appConfigId") REFERENCES "AppConfig"("id") ON DELETE CASCADE ON UPDATE CASCADE;
