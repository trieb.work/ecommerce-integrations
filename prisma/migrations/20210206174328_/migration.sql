/*
  Warnings:

  - You are about to drop the column `clientId` on the `BraintreeConf` table. All the data in the column will be lost.
  - You are about to drop the column `clientSecret` on the `BraintreeConf` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "BraintreeConf" DROP COLUMN "clientId",
DROP COLUMN "clientSecret",
ADD COLUMN     "publicKey" TEXT,
ADD COLUMN     "privateKey" TEXT;
