/*
  Warnings:

  - You are about to drop the column `zohoConfId` on the `BraintreeConf` table. All the data in the column will be lost.
  - The migration will add a unique constraint covering the columns `[appConfigId]` on the table `BraintreeConf`. If there are existing duplicate values, the migration will fail.
  - Added the required column `appConfigId` to the `BraintreeConf` table without a default value. This is not possible if the table is not empty.

*/
-- DropIndex
DROP INDEX "BraintreeConf_zohoConfId_unique";

-- DropForeignKey
ALTER TABLE "BraintreeConf" DROP CONSTRAINT "BraintreeConf_zohoConfId_fkey";

-- AlterTable
ALTER TABLE "BraintreeConf" DROP COLUMN "zohoConfId",
ADD COLUMN     "appConfigId" INTEGER NOT NULL;

-- AlterTable
ALTER TABLE "ZohoConf" ADD COLUMN     "braintreeConfId" INTEGER;

-- CreateIndex
CREATE UNIQUE INDEX "BraintreeConf.appConfigId_unique" ON "BraintreeConf"("appConfigId");

-- AddForeignKey
ALTER TABLE "BraintreeConf" ADD FOREIGN KEY ("appConfigId") REFERENCES "AppConfig"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ZohoConf" ADD FOREIGN KEY ("braintreeConfId") REFERENCES "BraintreeConf"("id") ON DELETE SET NULL ON UPDATE CASCADE;
