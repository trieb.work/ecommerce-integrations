-- AlterTable
ALTER TABLE "AppConfig" ADD COLUMN     "mailgunConfId" INTEGER;

-- AddForeignKey
ALTER TABLE "AppConfig" ADD FOREIGN KEY ("mailgunConfId") REFERENCES "MailgunConf"("id") ON DELETE SET NULL ON UPDATE CASCADE;
