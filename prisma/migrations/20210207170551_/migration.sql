/*
  Warnings:

  - You are about to drop the column `braintreeConfId` on the `ZohoConf` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "ZohoConf" DROP CONSTRAINT "ZohoConf_braintreeConfId_fkey";

-- AlterTable
ALTER TABLE "ZohoConf" DROP COLUMN "braintreeConfId";
