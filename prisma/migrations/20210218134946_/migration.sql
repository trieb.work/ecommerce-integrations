-- CreateTable
CREATE TABLE "LexofficeConf" (
    "id" SERIAL NOT NULL,
    "active" BOOLEAN NOT NULL DEFAULT false,
    "webhookID" TEXT,
    "appConfigId" INTEGER NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "LexofficeConf.appConfigId_unique" ON "LexofficeConf"("appConfigId");

-- AddForeignKey
ALTER TABLE "LexofficeConf" ADD FOREIGN KEY ("appConfigId") REFERENCES "AppConfig"("id") ON DELETE CASCADE ON UPDATE CASCADE;
