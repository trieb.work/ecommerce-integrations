/*
  Warnings:

  - You are about to drop the `TrackAndTrace` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "TrackAndTrace" DROP CONSTRAINT "TrackAndTrace_appConfigId_fkey";

-- DropForeignKey
ALTER TABLE "TrackAndTrace" DROP CONSTRAINT "TrackAndTrace_easyPostConfId_fkey";

-- DropForeignKey
ALTER TABLE "TrackAndTrace" DROP CONSTRAINT "TrackAndTrace_mailgunConfId_fkey";

-- CreateTable
CREATE TABLE "TrackAndTraceConf" (
    "id" SERIAL NOT NULL,
    "active" BOOLEAN DEFAULT false,
    "cuid" TEXT NOT NULL,
    "appConfigId" INTEGER NOT NULL,
    "mailgunConfId" INTEGER,
    "easyPostConfId" INTEGER,

    PRIMARY KEY ("id")
);

-- DropTable
DROP TABLE "TrackAndTrace";

-- CreateIndex
CREATE UNIQUE INDEX "TrackAndTraceConf.appConfigId_unique" ON "TrackAndTraceConf"("appConfigId");

-- AddForeignKey
ALTER TABLE "TrackAndTraceConf" ADD FOREIGN KEY ("mailgunConfId") REFERENCES "MailgunConf"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TrackAndTraceConf" ADD FOREIGN KEY ("easyPostConfId") REFERENCES "EasyPostConf"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TrackAndTraceConf" ADD FOREIGN KEY ("appConfigId") REFERENCES "AppConfig"("id") ON DELETE CASCADE ON UPDATE CASCADE;
