/*
  Warnings:

  - You are about to drop the column `mailgunConfId` on the `TrackAndTraceConf` table. All the data in the column will be lost.
  - You are about to drop the column `easyPostConfId` on the `TrackAndTraceConf` table. All the data in the column will be lost.
  - The migration will add a unique constraint covering the columns `[trackAndTraceConfigId]` on the table `EasyPostConf`. If there are existing duplicate values, the migration will fail.
  - The migration will add a unique constraint covering the columns `[trackAndTraceConfigId]` on the table `MailgunConf`. If there are existing duplicate values, the migration will fail.
  - Made the column `apiHost` on table `MailgunConf` required. The migration will fail if there are existing NULL values in that column.

*/
-- DropForeignKey
ALTER TABLE "TrackAndTraceConf" DROP CONSTRAINT "TrackAndTraceConf_easyPostConfId_fkey";

-- DropForeignKey
ALTER TABLE "TrackAndTraceConf" DROP CONSTRAINT "TrackAndTraceConf_mailgunConfId_fkey";

-- AlterTable
ALTER TABLE "EasyPostConf" ADD COLUMN     "trackAndTraceConfigId" INTEGER;

-- AlterTable
ALTER TABLE "MailgunConf" ADD COLUMN     "trackAndTraceConfigId" INTEGER,
ALTER COLUMN "apiHost" SET NOT NULL,
ALTER COLUMN "apiHost" SET DEFAULT E'api.eu.mailgun.net';

-- AlterTable
ALTER TABLE "TrackAndTraceConf" DROP COLUMN "mailgunConfId",
DROP COLUMN "easyPostConfId";

-- CreateIndex
CREATE UNIQUE INDEX "EasyPostConf.trackAndTraceConfigId_unique" ON "EasyPostConf"("trackAndTraceConfigId");

-- CreateIndex
CREATE UNIQUE INDEX "MailgunConf.trackAndTraceConfigId_unique" ON "MailgunConf"("trackAndTraceConfigId");

-- AddForeignKey
ALTER TABLE "EasyPostConf" ADD FOREIGN KEY ("trackAndTraceConfigId") REFERENCES "TrackAndTraceConf"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "MailgunConf" ADD FOREIGN KEY ("trackAndTraceConfigId") REFERENCES "TrackAndTraceConf"("id") ON DELETE SET NULL ON UPDATE CASCADE;
