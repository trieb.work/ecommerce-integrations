/*
  Warnings:

  - Made the column `webhookToken` on table `SaleorConf` required. The migration will fail if there are existing NULL values in that column.
  - Made the column `webhookToken` on table `ZohoConf` required. The migration will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "SaleorConf" ALTER COLUMN "webhookToken" SET NOT NULL;

-- AlterTable
ALTER TABLE "ZohoConf" ALTER COLUMN "webhookToken" SET NOT NULL;

-- CreateTable
CREATE TABLE "EasyPostConf" (
    "id" SERIAL NOT NULL,
    "cuid" TEXT NOT NULL,
    "active" BOOLEAN DEFAULT false,
    "appConfigId" INTEGER NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "EasyPostConf.appConfigId_unique" ON "EasyPostConf"("appConfigId");

-- AddForeignKey
ALTER TABLE "EasyPostConf" ADD FOREIGN KEY ("appConfigId") REFERENCES "AppConfig"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AlterIndex
ALTER INDEX "ProductDataFeed_appConfigId_unique" RENAME TO "ProductDataFeed.appConfigId_unique";
