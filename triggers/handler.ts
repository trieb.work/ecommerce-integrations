/* eslint-disable import/prefer-default-export */
/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-console */
import { Handler } from 'aws-lambda';
import axios from 'axios';
import { map } from 'async';
import Bull from 'bull';

const redisOpts = {
    host: process.env.REDIS_HOST as string,
    port: parseInt(process.env.REDIS_PORT as string, 10),
    password: process.env.REDIS_PWD as string,
    keyPrefix: 'prod_',
};

if (!process.env.TRIGGER_URLS) throw new Error('No trigger URLs defined');

export const trigger :Handler = async (event :any) => {
    const URLs = (process.env.TRIGGER_URLS as string).split(',');
    // const invoiceSyncQueue = 'invoicesync';
    /**
     * Subscribe to different channels on redis
     */
    // const invoiceWorker = new Bull(invoiceSyncQueue, {
    //     redis: redisOpts,
    // });

    // console.log('Starting to trigger MailChimp at this URLs:', JSON.stringify(URLs));
    // map(URLs, async (link: string) => {
    //     const result = await axios({
    //         url: link,
    //         method: 'GET',
    //         timeout: 10000,
    //     });
    //     const text = result.data;
    //     console.log(`${link}: ${text}`);
    //     return true;
    // }, (err) => {
    //     if (err) console.error(err);
    // });

    return {
        statusCode: 200,
        body: JSON.stringify(
            {
                message: 'Go Serverless v1.0! Your function executed successfully!',
                input: event,
            },
            null,
            2,
        ),
    };
};
