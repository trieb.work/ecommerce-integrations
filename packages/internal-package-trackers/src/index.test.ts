/* eslint-disable @typescript-eslint/no-unused-expressions */
import dotenv from 'dotenv';
import { expect } from 'chai';
import { PackageTrackerInstance } from './index';
import moment from 'moment-business-days';

dotenv.config({ path: '../../.env' });

describe('Import Tests', () => {
    it('should return the next business day as date', () => {
        console.log(`The next business day is: ${PackageTrackerInstance.getNextBusinessDay().rfc2822}`);
    });
    it('Should create a new Package Tracker Instance without easypost, and with all Sendgrid settings', () => {
        const testClient = new PackageTrackerInstance({
            sendgridApikey: process.env.SENDGRID_API_KEY,
            fromEmail: 'donotreply@pfefferundfrost.de',
        });
        expect(testClient).to.exist;
    });
    it('should crate a new Package Tracker Instance with just easypost API token', () => {
        const testClient = new PackageTrackerInstance({
            easyPostApiToken: '1234',
        });
        expect(testClient).to.exist;
    });
    it('should create a test email using a template with delay', async () => {
        const testClient = new PackageTrackerInstance({
            sendgridApikey: process.env.SENDGRID_API_KEY,
            fromEmail: 'donotreply@pfefferundfrost.de',
            fromTitle: 'Pfeffer & Frost Testing Client',
            testMode: false,
        });
        const delay = moment().add(150, 'seconds').unix();

        const response = await testClient.sendMail({ toEmail: 'zinkljannik@gmail.com',
            subject: 'this is a test',
            template: 'd-1cc7eddda85c4d8f8d9405f13e32d9bb',
            deliveryTime: delay,
            variables: {
                TRACKINGNUMBER: '2345',
                TRACKINGPROVIDER: 'DPD',
                FIRSTNAME: 'Jannik',
                LASTNAME: 'Zinkl',
                CURRENTYEAR: '2021',
                TRACKINGPORTALURL: 'https://pfefferundfrost.de',
            } });
        expect(response).to.exist;
    });
});
