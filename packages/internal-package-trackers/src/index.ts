/* eslint-disable @typescript-eslint/dot-notation */
import EasyPost from '@easypost/api';
import { WinstonLoggerType } from '@trieb.work/apm-logging';
import { ZohoClientInstance } from '@trieb.work/zoho-inventory-ts';
import { MailService } from '@sendgrid/mail';
import moment from 'moment-business-days';
import { Carrier, Tracker } from './types';

type Config = {
    logger? :WinstonLoggerType,
    zohoClient? :ZohoClientInstance,
    easyPostApiToken? :string
    mailgunApiKey? :string
    sendgridApikey? :string
    mailgunFromTitle? :string;
    mailgunFromEmail? :string;
    fromEmail? :string;
    fromTitle? :string;
    testMode? :boolean;
};

class PackageTrackerInstance {
    private easyPostApiToken :string;

    private easyPostClient :EasyPost;

    private logger :WinstonLoggerType|Console;

    private zohoClient :ZohoClientInstance;

    private mailer :MailService;

    private sender :string;

    private fromTitle :string;

    private testMode :boolean;

    constructor(config :Config) {
        this.easyPostApiToken = config.easyPostApiToken;
        this.easyPostClient = new EasyPost(this.easyPostApiToken || '1234');
        this.logger = config.logger || console;
        this.zohoClient = config.zohoClient;
        this.sender = config.mailgunFromEmail || config.fromEmail;
        this.fromTitle = config.fromTitle;
        this.mailer = new MailService();
        if (config.sendgridApikey) this.mailer.setApiKey(config.sendgridApikey);
        this.testMode = config.testMode || false;
    }

    /**
     * Add an easypost tracker
     * @param trackingCode
     * @param carrier
     */
    addTracker = async (trackingCode :string, carrier :Carrier) => {
        const tracker = new this.easyPostClient.Tracker({
            tracking_code: trackingCode,
            carrier,
        });
        return tracker.save() as Tracker;
    };

    getTracker = async (trackerId :string) => this.easyPostClient.Tracker.retrieve(trackerId) as Tracker;

    /**
     * Creates or updates a webhook in Easypost. Returns the Easypost ID of the Webhook
     * @param url
     * @param id
     */
    upsertEasypostWebhook = async (url :string, id? :string) => {
        if (id) {
            this.logger.info(`Updating EasyPost Webhook with ID ${id}`);
            const webhook = await this.easyPostClient.Webhook.retrieve(id);
            webhook.url = url;
            await webhook.save();
            return id;
        }

        const currentHooks = await this.easyPostClient.Webhook.all();
        const isAlreadyExisting = currentHooks.find((x) => x.url === url);
        if (isAlreadyExisting) {
            this.logger.info(`We already have a valid Webhook with Id ${isAlreadyExisting.id}`);
            return isAlreadyExisting.id as string;
        }
    };

    /**
     * send out an email via mailgun
     * @param toEmail
     * @param subject
     * @param html
     * @param replyTo
     * @param deliveryTime
     */
    sendMail = async ({ toEmail, subject, html, replyTo, deliveryTime, variables, template }
    :{toEmail :string, subject :string, html? :string, replyTo?:string, deliveryTime? :number, variables? :{ }, template? :string}) => {
        const additionals = {};
        if (replyTo) additionals['h:Reply-To'] = replyTo;
        if (deliveryTime) additionals['o:deliverytime'] = deliveryTime;
        if (template) additionals['template'] = template;
        if (variables) additionals['h:X-Mailgun-Variables'] = JSON.stringify(variables);

        const msg = {
            to: toEmail,
            from: {
                email: this.sender,
                name: this.fromTitle,
            },
            replyTo,
            sendAt: deliveryTime,
            templateId: template,
            subject,
            html,
            dynamicTemplateData: variables,
            mailSettings: {
                sandboxMode: {
                    enable: this.testMode,
                },
            },
        };
        const response = await this.mailer.send(msg);
        return response;
    };

    /**
     * Get the next business day as date object. Always returns the next business day, mornings at 8:00;
     * If the current day is a business day before 14:00, we return now + 10 mins for security;
     */
    static getNextBusinessDay() {
        const now = moment();
        const endOfBusinessDay = moment().set('hour', 15);
        if (now.isBusinessDay() && now < endOfBusinessDay) {
            const dateObject = now.add(10, 'minutes').toDate();
            const rfc2822 = dateObject.toUTCString();
            const unixSeconds = now.add(10, 'minutes').unix();
            return { unixSeconds, rfc2822, dateObject };
        }
        const momentDate = moment().nextBusinessDay().set('hour', 8);
        const dateObject = momentDate.toDate();
        const rfc2822 = dateObject.toUTCString();
        const unixSeconds = momentDate.unix();

        return { unixSeconds, rfc2822, dateObject };
    }
}

export { PackageTrackerInstance };
export * from './types';
