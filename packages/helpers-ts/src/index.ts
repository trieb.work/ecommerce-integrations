import assert from 'assert';
import dayjs from 'dayjs';
import { btoa } from 'js-base64';

type TaxIdentifier = 'tax_rate' | 'tax_percentage';
/**
 * This function takes all saleor orderlines and calculates the corresponding taxrate for the shipping costs.
 * Always the highest tax rate of all order lines is the shipping cost tax rate
 * @param {array} lines Saleor Orderline with field tax_rate
 */
export function calculateShippingTaxRate(lines: {tax_rate?:string, tax_percentage?:number }[], identifier :TaxIdentifier) {
    const parsedLines = identifier === 'tax_rate' ? lines.map((line) => parseInt(line[identifier], 10)) : lines.map((line) => line.tax_percentage);
    const Max = Math.max(...parsedLines);
    assert(Number.isInteger(Max));
    return Max;
}

/**
 * Validates a date for plausability. November for example has just 30 days - this function will give you false
 * for 2020-11-31
 * @param date the date string
 * @param format the format of the date string. For exampl 'YYYY-MM-DD'
 */
export function validateDate(date :string, format :string) {
    return dayjs(date, format).format(format) === date;
}

/**
 * Returns the Saleor Order Id from an order number.
 * @param orderNumber String like 356, 1502, etc..
 */
export function getGqlOrderId(orderNumber: string): string {
    return btoa(`Order:${orderNumber}`);
}
