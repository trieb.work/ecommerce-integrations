import { ApolloClient, InMemoryCache, ApolloLink, HttpLink } from '@apollo/client';
import { RetryLink } from '@apollo/client/link/retry';
import { onError } from '@apollo/client/link/error';
import fetch from 'isomorphic-unfetch';

export * from '@apollo/client';

const loggerLink = new ApolloLink((operation, forward) => {
    console.log(`GraphQL Request: ${operation.operationName}`);
    operation.setContext({ start: new Date() });
    return forward(operation).map((response) => {
        const responseTime = new Date().getTime() - new Date(operation.getContext().start).getTime();
        console.log(`GraphQL Response took: ${responseTime}`);
        return response;
    });
});

const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors) {
        graphQLErrors.map(({ message }) => { throw new Error(`GraphQL Error: ${message}`)});
    }
    if (networkError) {
        throw new Error(`Network Error: ${networkError.message}`);
    }
});

/**
 * Wrapper around the GraphQL Apollo library used for communication with the Saleor API
 * @param uri The URI where we connect to
 * @param bearerToken The JWT token to authenticate the client. Is optional
 */
export default function Client(uri :string, bearerToken? :string) {
    const token = bearerToken;

    const links = ApolloLink.from([
        loggerLink,
        new RetryLink({
            attempts: {
                max: 3,
            } }),
        errorLink,
        new HttpLink({
            fetch,
            uri,
            headers: {
                authorization: token ? `Bearer ${token}` : '',
            } }),
    ]);

    const client = new ApolloClient({
        link: links,
        cache: new InMemoryCache(),
    });

    return client;
}
