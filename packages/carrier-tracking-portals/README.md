# Carrier Tracking Portals Generator
Different carriers in different countries use different package tracking portals where customers can check their package status and make changes to their delivery preferences.
Especially when parcels are sent to diferent countries, it is important to provide the receiver with the correct portal URL so that they can make changes to their delivery. A package coming from Germany, going to Switzerland via DPD needs to be tracked in the portal from DPD Switzerland.

This library should help you with the genneration of the corresponding portal URL. Currently only DPD in Europe is supported.