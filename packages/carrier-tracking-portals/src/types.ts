export type Carrier = 'DPD';
export type CountryCode = 'de' | 'ch' |'at' | 'es' | 'uk' | 'nl';