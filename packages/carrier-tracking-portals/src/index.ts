import { Carrier, CountryCode } from './types';

/**
 * Generates a tracking portal URL for different carriers and countries.
 * @param carrier
 * @param countryCode
 * @param trackingCode
 * @param zip
 */
const generateTrackingPortalURL = (carrier :Carrier, countryCode :CountryCode, trackingCode :string, zip :string) => {
    const patterns = {
        de: {
            DPD: `https://my.dpd.de/redirect.aspx?action=1&pno=${trackingCode}&zip=${zip}&lng=De_de`,
        },
        ch: {
            DPD: `https://www.dpdgroup.com/ch/mydpd/tmp/basicsearch?parcel_id=${trackingCode}`,
        },
        at: {
            DPD: `https://www.mydpd.at/meine-pakete?pno=${trackingCode}`,
        },
        es: {
            DPD: `https://www.seur.com/livetracking/?segOnlineIdentificador=${trackingCode}&segOnlineIdioma=es`,
        },
        fr: {
            DPD: `https://www.dpd.fr/trace/${trackingCode}`,
        },
        no: {
            DPD: `https://tracking.postnord.com/no/?id=${trackingCode}`,
        },
        uk: {
            DPD: `https://apis.track.dpd.co.uk/v1/track?parcel=${trackingCode}`,
        },
        be: {
            DPD: `https://www.dpdgroup.com/be/mydpd/tmp/basicsearch?parcel_id=${trackingCode}`,
        },
        nl: {
            DPD: `https://tracking.dpd.de/status/nl_NL/parcel/${trackingCode}`,
        },
    };

    const returnUrl = countryCode ? patterns[countryCode][carrier] : null || null;

    return returnUrl;
};
export default generateTrackingPortalURL;
export * from './types';
