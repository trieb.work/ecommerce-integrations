import { IncomingHttpHeaders } from 'http';
import winston from 'winston';
import { ElasticsearchTransport, ElasticsearchTransportOptions } from 'winston-elasticsearch';

let globalEsTransport :ElasticsearchTransport;
let globalWinston :winston.Logger;

const env = process.env.APM_ENV || 'dev';

/**
 * Flush logs and APM data to elasticsearch before ending the session
 */
const apmFlush = async () => {
    const res1 = globalEsTransport ? globalEsTransport.flush() : true;

    await Promise.all([res1]);
    return true;
};

interface CommonConfigData {
    elasticLoggingServer :string,
}

const loggingAndApmSetup = (commonConfig :CommonConfigData) => {
    if (!commonConfig?.elasticLoggingServer) {
        const winstonLogger = globalWinston || winston.createLogger({
            level: 'info',
            transports: [
                new winston.transports.Console(),
            ],
        });
        globalWinston = winstonLogger;

        return winstonLogger;
    }

    const esTransportOpts :ElasticsearchTransportOptions = {
        dataStream: true,
        clientOpts: {
            node: commonConfig.elasticLoggingServer,
            auth: {
                username: 'logger',
                password: 'logger',
            } },
    };
    const esTransport = globalEsTransport || new ElasticsearchTransport(esTransportOpts);
    globalEsTransport = esTransport;

    const winstonLogger = globalWinston || winston.createLogger({
        level: 'info',
        transports: [
            esTransport,
            new winston.transports.Console(),
        ],
    });
    globalWinston = winstonLogger;

    return winstonLogger;
};

type MetaObject = {
    'saleor-domain'?: string,
    'saleor-event'?: string,
    'zoho-org-id'?: string,
    'easypost-event-id'?: string,
    'easypost-user-id'?: string,
    'appConfigId'?: number,
    environment?: string,
    cuid? :string,
};

/**
 * Custom Typeguard to check if this request is a nextAPIRequest or not
 * @param arg
 */
function isNextRequest(arg: any): arg is IncomingData {
    return arg?.httpVersion !== undefined;
}

interface IncomingData {
    headers? :IncomingHttpHeaders
    body? :any
    query? :{
        [key: string]: string | string[]
    }
}
/**
 * Creates and Returns the Standard Winston logger with metadata
 * @param req The next.js Req Object
 */
const logAndMeasure = (req :IncomingData|MetaObject, commonConfig :CommonConfigData) => {
    const winstonLogger = loggingAndApmSetup(commonConfig);
    const defaultMeta :MetaObject = {
    };

    if (isNextRequest(req)) {
        const cuid = req?.query?.cuid as string;
        const easyPostTrackingCode = req?.body?.result?.tracking_code as string;
        const easypostUserId = req?.headers?.['X-Webhook-User-Id'] as string;
        const easypostTrackingStatus = req?.body?.result?.status as string;

        const invoiceId = req?.body?.invoice?.invoice_id as string;

        if (cuid) {
            defaultMeta.cuid = cuid;
        }
        if (req?.headers?.['x-saleor-domain']) {
            defaultMeta['saleor-domain'] = req?.headers?.['x-saleor-domain'] as string;
        }
        if (req?.headers?.['x-saleor-event']) {
            defaultMeta['saleor-event'] = req?.headers?.['x-saleor-event'] as string;
        }
        if (easypostUserId && req?.body?.id) defaultMeta['easypost-event-id'] = req?.body?.id as string;
        if (easypostUserId) defaultMeta['easypost-user-id'] = easypostUserId;
        if (easyPostTrackingCode) defaultMeta['easypost-tracking-code'] = easyPostTrackingCode;
        if (easypostTrackingStatus) defaultMeta['easypost-tracking-status'] = easypostTrackingStatus;

        // eslint-disable-next-line max-len
        const zohoOrgId = req?.headers?.['x-com-zoho-organizationid'] as string || req?.query?.['zoho-org-id'] as string || req?.headers?.['dre-scope-id'] as string;
        if (zohoOrgId) {
            defaultMeta['zoho-org-id'] = zohoOrgId;
        }

        if (invoiceId) {
            defaultMeta['zoho-invoice-id'] = invoiceId;
        }
    } else {
        // this request is coming from the worker for example.
        defaultMeta.appConfigId = req?.appConfigId;
        defaultMeta['saleor-domain'] = req?.['saleor-domain'];
    }

    defaultMeta.environment = env;

    // we always start a child logger with corresponding metadata used just for this request
    const childLogger = winstonLogger.child(defaultMeta);

    return childLogger;
};

type WinstonLoggerType = winston.Logger;

export { logAndMeasure, apmFlush, WinstonLoggerType };
