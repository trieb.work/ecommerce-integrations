import axios, { AxiosInstance } from 'axios';
import assert from 'assert';
import FormData from 'form-data';
import retry from 'async-retry';
import { LexofficeInvoiceObject, LexOfficeVoucher, VoucherStatus } from './types';

type VoucherType = 'invoice' | 'salesinvoice';

function createInstance(token :string) {
    if (!token) throw new Error('Missing lexoffice API token! Set it via env variable LEXOFFICE_KEY');
    const options = {
        baseURL: 'https://api.lexoffice.io/v1',
        timeout: 6000,
    };

    return axios.create({
        ...options,
        headers: {
            Authorization: `Bearer ${token}`,
        },
    });
}
export class LexofficeInstance {
    private apiKey :string;

    private instance :AxiosInstance;

    constructor(apiKey :string) {
        this.apiKey = apiKey;
        this.instance = createInstance(this.apiKey);
    }

    /**
     * Get a voucher list from Lexoffice
     * @param voucherType
     * @param voucherStatus
    */
    voucherlist = async (voucherType :VoucherType, voucherStatus :VoucherStatus) => {
        const vouchers = await this.instance({
            url: '/voucherlist',
            params: {
                voucherType,
                voucherStatus,
            },
        });
        assert.strictEqual(vouchers.status, 200);
        return vouchers.data.content as [];
    };

    /**
     * Search for a voucher with a specific number
     * @param voucherNumber
    */
    getVoucherByVoucherNumber = async (voucherNumber :string) => {
        const voucher = await this.instance({
            url: '/vouchers',
            params: {
                voucherNumber,
            },
        });
        assert.strictEqual(voucher.status, 200);
        return voucher.data?.content[0] as LexOfficeVoucher || null;
    };

    /**
     * Create a voucher in Lexoffice
     * @param voucherData
     * @returns lexOfficeInvoiceID
     */
    createVoucher = async (voucherData :LexofficeInvoiceObject) => {
        const result = await this.instance({
            url: '/vouchers',
            method: 'post',
            data: voucherData,
        });
        return result.data.id as string;
    };

    /**
     * Uploads a file as multipart form-data to a voucher
     * @param filebuffer
     * @param filename
    */
    addFiletoVoucher = async (filebuffer :Buffer, filename :string, voucherId :string) => {
        const form = new FormData();
        form.append('file', filebuffer, filename);
        await this.instance({
            url: `/vouchers/${voucherId}/files`,
            method: 'post',
            headers: form.getHeaders(),
            data: form,
        });

        return true;
    };

    /**
     * searches for the Email Adresse and returns the customer Id, if the customer does already exist.
    */
    createContactIfnotExisting = async (
        { email, firstName, lastName, street, countryCode }
        :{email:string, firstName :string, lastName:string, street :string, countryCode :string},
    ) :Promise<string> => {
        const customerLookup = await this.instance({
            url: '/contacts',
            method: 'GET',
            params: {
                email,
                customer: true,
            },
        });
        if (customerLookup.data?.content?.length > 0) {
            console.log('Found a contact. Returning ID');
            return customerLookup.data.content[0].id;
        }
        const data = {
            version: 0,
            roles: {
                customer: {},
            },
            person: {
                firstName,
                lastName,
            },
            addresses: {
                billing: [
                    {
                        street,
                        countryCode,
                    },
                ],
            },
            emailAddresses: {
                business: [
                    `${email}`,
                ],
            },
        };
        const retryLog = (e :Error, attempt :number) => {
            console.info(`Retry ${attempt} to create User in Lexoffice`);
        };
        await retry(async () => {
            const createResult = await this.instance({
                url: '/contacts',
                method: 'post',
                data,
            });
            assert.strictEqual(createResult.status, 200);
            if (!createResult.data?.id) throw new Error(`Could not create contact! ${createResult.data}`);
            return createResult.data.id;
        }, {
            retries: 5,
            onRetry: retryLog,
        });
    };
}
