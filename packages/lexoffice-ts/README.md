# Lexoffice-ts
This is a little helper library used to interact with the Lexoffice API. Not all functionality is included.

# Usage
```
import { LexofficeInvoiceObject, LexOfficeVoucherItem, LexofficeInstance } from '@trieb.work/lexoffice-ts';

const lexofficeClient = new LexofficeInstance(apiToken);

const createObject :LexofficeInvoiceObject = {
    ...invoice,
    voucherStatus: 'paid',
    totalTaxAmount: taxTotal,
    taxType: 'gross',
    useCollectiveContact: false,
    contactId: lexofficeCustomerId,
    voucherItems,
    version: 0,
};
console.info(`Creating now ${createObject.voucherNumber} with total gross amount ${createObject.totalGrossAmount} in Lexoffice`);
const lexofficeInvoiceID = await lexofficeClient.createVoucher(createObject);

```