import { PrismaClient } from '@prisma/client';

let prisma: PrismaClient;

const eciPrismaClient = () => {
    // Prisma - start the Postgres connection, or reuse the existing one
    if (process.env.NODE_ENV === 'production') {
        prisma = new PrismaClient();
    } else {
        // Ensure the prisma instance is re-used during hot-reloading
        // Otherwise, a new client will be created on every reload
        globalThis.prisma = globalThis.prisma || new PrismaClient();
        prisma = globalThis.prisma;
    }
    return prisma;
};
export default eciPrismaClient;
